# Stage 1: Build the app
FROM node:20 AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install npm@latest

RUN npm install

COPY . .

RUN npm run build

# Stage 2: Serve with Nginx
FROM nginx:alpine

# Copy build files
COPY --from=builder /app/dist /usr/share/nginx/html

# Copy Nginx configuration
COPY ./nginx/frontend.conf /etc/nginx/conf.d/default.conf

# Expose both 80
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]