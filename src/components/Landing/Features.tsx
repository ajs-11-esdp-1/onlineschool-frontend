import React from "react";
import { Typography } from "antd";

const { Title } = Typography;

interface Feature {
  icon: string;
  title: string;
  text: string;
}

interface FeaturesProps {
  data: Feature[] | undefined;
}

const Features: React.FC<FeaturesProps> = (props) => {
  return (
    <div id="features" style={{ textAlign: "center", padding: "40px" }}>
      <div className="container">
        <div className="col-md-10 col-md-offset-1 section-title">
          <Title level={2}>Features</Title>
        </div>
        <div className="row">
          {props.data
            ? props.data.map((d, i) => (
                <div key={`${d.title}-${i}`}>
                  <i className={d.icon}></i>
                  <h3>{d.title}</h3>
                  <p>{d.text}</p>
                </div>
              ))
            : "Loading..."}
        </div>
      </div>
    </div>
  );
};

export default Features;

