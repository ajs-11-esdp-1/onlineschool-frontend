import React from "react";
import { Typography, Row, Col } from "antd";

const { Title, Paragraph } = Typography;

interface AboutData {
  paragraph: string;
  Why: string[];
  Why2: string[];
}

interface AboutProps {
  data: AboutData | undefined;
}

const About: React.FC<AboutProps> = (props) => {
  return (
    <div id="about" style={{ padding: "40px", background: "#F2F2F2" }}>
      <div className="container">
        <Row gutter={[16, 16]}>
          <Col xs={24} md={12}>
            <img src="https://thumbs.dreamstime.com/b/kids-education-child-boy-study-school-thinking-bubble-dreaming-over-black-chalkboard-75214276.jpg" alt="" style={{ width: "100%" }} />
          </Col>
          <Col xs={24} md={12}>
            <div className="about-text">
              <Title level={2}>About Us</Title>
              <Paragraph>{props.data ? props.data.paragraph : "Loading..."}</Paragraph>
              <Title level={3}>Why Choose Us?</Title>
              <div className="list-style">
                <Row gutter={[16, 16]}>
                  <Col lg={12} sm={12} xs={24}>
                    <ul>
                      {props.data
                        ? props.data.Why.map((d, i) => (
                            <li key={`${d}-${i}`}>{d}</li>
                          ))
                        : "Loading..."}
                    </ul>
                  </Col>
                  <Col lg={12} sm={12} xs={24}>
                    <ul>
                      {props.data
                        ? props.data.Why2.map((d, i) => (
                            <li key={`${d}-${i}`}>{d}</li>
                          ))
                        : "Loading..."}
                    </ul>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default About;

