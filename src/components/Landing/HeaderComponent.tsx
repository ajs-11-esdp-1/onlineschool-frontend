import React from "react";
import { Typography, Button } from "antd";

const { Title, Paragraph } = Typography;

export interface HeaderProps {
    data: {
        title: string;
        paragraph: string;
    } | null;
}

const Header: React.FC<HeaderProps> = ({ data }: HeaderProps) => {
    return (
        <header id="header">
            <div className="intro">
                <div className="overlay" >
                    <div className="container">
                        <div className="row">
                            <div className="intro-text">
                                <Title level={1}>
                                    {data ? data.title : "Loading"}
                                </Title>
                                <Paragraph
                                    style={{
                                        color: "#fff",
                                        fontSize: "22px",
                                        fontWeight: 300,
                                        lineHeight: "30px",
                                        margin: "0 auto",
                                        marginBottom: "60px",
                                        width: '100%'
                                    }}
                                >
                                    {data ? data.paragraph : "Loading"}
                                </Paragraph>
                                <Button className="btn-custom" type="primary" size="large">
                                    Learn More
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;


