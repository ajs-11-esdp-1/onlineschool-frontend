import React from "react";
import { Typography, Button } from "antd";

const { Title, Paragraph } = Typography;


const VideoAndSignUp: React.FC = () => {

  return (
    <div style={{ backgroundColor: "#f6f6f6", padding: "40px" }}>
      <div style={{ textAlign: "center", color: "#333" }}>
        <Title style={{ color: "#f39c12" }}>Watch Our Video</Title>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/VIDEO_ID"
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
      </div>
      <div style={{ textAlign: "center", color: "#333", marginTop: "40px" }}>
        <Title style={{ color: "#e74c3c" }}>Join us now!</Title>
        <Paragraph>
          Ut vehicula, lectus nec ullamcorper accumsan, elit risus tincidunt odio, ac rhoncus ex velit sit amet odio.
          Phasellus dapibus lorem nec elit feugiat, eu egestas justo sollicitudin.
        </Paragraph>
        <Button
          type="primary"
          size="large"
          style={{ background: "#28a745", borderColor: "#28a745" }}
        >
          Sign Up
        </Button>
      </div>
    </div>
  );
};

export default VideoAndSignUp;

