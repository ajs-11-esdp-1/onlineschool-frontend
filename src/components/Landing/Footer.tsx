import React from "react";
import { Typography } from "antd";

const { Paragraph } = Typography;

const Footer: React.FC = () => {
  return (
    <div style={{ textAlign: "center", color: "#777", marginBottom: "25px" }}>
      <Paragraph>
        &copy; {new Date().getFullYear()} Class2Be | All rights reserved
      </Paragraph>
    </div>
  );
};

export default Footer;
