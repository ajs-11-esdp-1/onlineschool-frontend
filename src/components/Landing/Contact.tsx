/*
import { useState } from "react";
import { Typography, Form, Input, Button, Row, Col } from "antd";
// @ts-ignore
import emailjs, { EmailJSResponseStatus } from "emailjs-com";

const { Title, Paragraph } = Typography;

interface ContactProps {
  data: {
    address: string;
    phone: string;
    email: string;
    facebook: string;
    twitter: string;
    youtube: string;
  } | null;
}

const initialState = {
  name: "",
  email: "",
  message: "",
};

const Contact: React.FC<ContactProps> = ({ data }) => {
  const [form] = Form.useForm();
  const [{ name, email, message }, setState] = useState(initialState);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  const clearState = () => setState({ ...initialState });

  const handleSubmit = async (values: any) => {
    console.log(values.name, values.email, values.message);

    try {
      const response: EmailJSResponseStatus = await emailjs.sendForm(
        "YOUR_SERVICE_ID",
        "YOUR_TEMPLATE_ID",
        values,
        "YOUR_USER_ID"
      );

      console.log(response.text);
      clearState();
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div>
      <div id="contact">
        <div className="container">
          <Row gutter={[16, 16]}>
            <Col md={12}>
              <div className="section-title">
                <Title level={2}>Напишите нам</Title>
                <Paragraph>
                  Заполните форму и мы с Вами свяжемся!
                </Paragraph>
              </div>
              <Form
                name="sentMessage"
                onFinish={handleSubmit}
                form={form}
                validateTrigger="onSubmit"
              >
                <Row gutter={[16, 16]}>
                  <Col md={12}>
                    <Form.Item
                      name="name"
                      rules={[{ required: true, message: "Please enter your name" }]}
                    >
                      <Input
                        placeholder="Name"
                        onChange={handleChange}
                      />
                    </Form.Item>
                  </Col>
                  <Col md={12}>
                    <Form.Item
                      name="email"
                      rules={[
                        { required: true, message: "Please enter your email" },
                        { type: "email", message: "Please enter a valid email" }
                      ]}
                    >
                      <Input
                        placeholder="Email"
                        onChange={handleChange}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Form.Item
                  name="message"
                  rules={[{ required: true, message: "Please enter your message" }]}
                >
                  <Input.TextArea
                    rows={4}
                    placeholder="Message"
                    onChange={handleChange}
                  />
                </Form.Item>
                <Button className="btn-custom" type="primary" htmlType="submit">
                  Send Message
                </Button>
              </Form>
            </Col>
            <Col md={8} md-offset-4 className="contact-info">
              <div className="contact-item">
                <Title level={3}>Контактная информация</Title>
                <p>
                  <span>
                    <i className="fa fa-map-marker"></i> Адрес
                  </span>{" "}
                  {data ? data.address : "loading"}
                </p>
                <p>
                  <span>
                    <i className="fa fa-phone"></i> Телефон
                  </span>{" "}
                  {data ? data.phone : "loading"}
                </p>
                <p>
                  <span>
                    <i className="fa fa-envelope-o"></i> Email
                  </span>{" "}
                  {data ? data.email : "loading"}
                </p>
              </div>
              <div className="contact-item">
                <div className="social">
                  <ul>
                    <li>
                      <a href={data ? data.facebook : "/"}>
                        <i className="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a href={data ? data.twitter : "/"}>
                        <i className="fa fa-twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a href={data ? data.youtube : "/"}>
                        <i className="fa fa-youtube"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default Contact;
*/
