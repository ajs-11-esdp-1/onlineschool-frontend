import { Progress, message } from 'antd'
import { useEffect } from 'react';


interface ResultProps {
    resultHandler: () => number;
    finish: boolean;
}

const Grade = ({resultHandler , finish}:ResultProps) => {
    const [messageApi, contextHolder] = message.useMessage();

    const success = () => {
        messageApi.open({
          type: 'success',
          content: <Progress
                type='circle'
                strokeColor={{'3%': '#081782' , '36%' : '#420979' , '64%' : '#420979' , '100%' : '#00d4ff' }}
                percent={(100 * resultHandler()) / 10}
                format={(percent) => percent &&`${(10 * percent) / 100} / 10`}
            />,
          duration: 10,
          icon: true,
          style: {
            marginTop: '20vh'
          }
        });
    };

    useEffect(() => {
        if(finish) {
            success()
        }
    },[finish])

    return contextHolder
};

export default Grade;