import { Button, Col, Input, Popconfirm, Row } from 'antd'
import Title from 'antd/es/typography/Title'
import { GetGrade } from '../../interfaces/grade/IGrade'
import { usePostReveiwGradeByIdMutation } from '../../store/services/grade'
import { useState } from 'react'


const GradeToolbar = ({id }: Pick<GetGrade , 'id'>) => {

    const [value , setValue] = useState<string>('')

    const [postReview] = usePostReveiwGradeByIdMutation() 

    const changeReview = () => {
        
        if(value.trim() !== '') {
            postReview({grade_id:Number(id),review:value})

            // console.log(value);
            
        }        
        
    }

    return (
        <Row 
            gutter={[24 ,24]}
            style={{marginTop:'3vh'}}   
             
        >
            <Col>
            <Popconfirm
                placement="bottomLeft"
                title={<Title level={4} >Рецензия на работу ученика</Title>}
                description={
                   
                    <Input.TextArea
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                        size='large'
                    />
                }
               
                onConfirm={() => changeReview() }
                okText="Отправить"
                cancelText="Отменить"
                
            >
                <Button
                    type='primary'
                >Рецензия</Button>
            </Popconfirm>
            </Col>
            <Col>
                
            </Col>
            <Col>
                
            </Col>
            <Col>
                
            </Col>
        </Row>
    )
}

export default GradeToolbar