import { Badge,  Collapse, Row } from 'antd'
import Title from 'antd/es/typography/Title'
import { ReactNode} from 'react'
import { INotice } from '../../interfaces/notice/Notice'
import { usePutNoticeReadMutation } from '../../store/services/notice'
import { useGetTemplateQuery } from '../../store/services/template'
import SortWordReview from '../PreviewComponents/SortWordReview'
import GradeToolbar from './GradeToolbar'
import MahjongPreview from '../PreviewComponents/MahjongPreview'
import { gradePreview } from './GradeStyle/GradeStyles'
import { default as dayjsLibrary } from 'dayjs';
import SortPicturePreview from '../PreviewComponents/SortPictureReview'
import QuizPreview from '../PreviewComponents/QuizPreview'
import WrittenAnswerPreview from '../PreviewComponents/WrittenAnswerPreview'



interface ITemplateObj {
    [key : string]:ReactNode
}


const GradeNotice = ({ from_user ,id , isRead , grade }:Pick<INotice , 'from_user' | 'id' | 'isRead' | 'grade'  >) => {
    
    const [putNotice] = usePutNoticeReadMutation()

    const {data: arrTamplate} = useGetTemplateQuery()

    const containerHandler = () => {

        if(arrTamplate && grade){
            
            const templateObj:ITemplateObj = {
                'Сортировка по словам':  <SortWordReview lesson={grade.lesson_answer} />,
                'Маджонг': <MahjongPreview lesson={grade.lesson_answer} />,
                'Сортировка по картинкам': <SortPicturePreview lesson={grade.lesson_answer} />,
                'Викторина 1 из 4': <QuizPreview lesson={grade.lesson_answer} />,
                'Впиши ответ': <WrittenAnswerPreview  lesson={grade.lesson_answer} />
            }            
    
            const index:number = arrTamplate.findIndex((val) => {
                return val.id === grade.lesson.template_id  
            })

            if(index >= 0){
                return templateObj[arrTamplate[index].title]
            }
     
        }   
                
    }

    const changeReadStatus = async() => {
        !isRead && await putNotice(id.toString())
        
    } 



    

    return (
        
        <Collapse
                expandIconPosition='end'
                ghost
                items={[
                    {
                      key: 2,
                      label: <Row>
                            {
                                !isRead &&
                                <Badge
                                    dot
                                    color='orange'
                                />
                            } 
                            <Title level={4} >Задание на проверку от {from_user.firstName} {from_user.lastName}</Title>
                        </Row>,
                      children: <div>
                            <Title level={4}>
                                Название : {
                                    grade?.lesson.description
                                }
                            </Title>
                            <Title level={4}>
                                Описание : {
                                    grade?.lesson.title
                                }
                            </Title>
                            <Title level={4}>
                                Время прохождения : {
                                      grade &&  `${dayjsLibrary((Number(grade.transit_time)) * 1000).minute()} : ${dayjsLibrary((Number(grade.transit_time)) * 1000).second()}`
                                    }
                            </Title>
                            <Title level={4}>
                                Оценка : {grade?.grade}
                            </Title>
                            <div style={gradePreview}>
                                {
                                    containerHandler()
                                }
                            </div> 
                            {
                                grade && <GradeToolbar id={grade.id}/>
                            }
                            
                      </div>,

                      
                      
                    }
                ]}
                onChange={() => changeReadStatus()}
            />
    )
}

export default GradeNotice;
