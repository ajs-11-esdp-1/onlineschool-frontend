import { CSSProperties } from "react";


export const gradePreview:CSSProperties = {
    maxHeight:'400px' ,
    overflow:'scroll',
    padding: '30px '
}