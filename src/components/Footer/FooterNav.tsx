import { Divider, Typography } from "antd";
import { Footer } from "antd/es/layout/layout";
import { NavLink, Link } from "react-router-dom";
import './FooterNav.css'
import { WhatsAppOutlined } from "@ant-design/icons";

const { Text, Title } = Typography;
function FooterNav() {
    return (
        <Footer>
            <div className="footer_container">
                <div className="footer_logo_content">
                    <NavLink to="/">
                        <div className="footer_logo"></div>
                    </NavLink>
                    <p className="footer_logo_text">Учебный центр <Text style={{ fontSize: 16 }} strong>Class2be</Text></p>
                    <Text strong style={{ marginBottom: 10 }}>since 2011</Text>
                    <div className="logo_socials">
                        <a href="https://instagram.com/class2be_education?igshid=MzRlODBiNWFlZA==" className="logo_socials_links logo_socials_links-inst" target="_blank"></a>
                        <a href="#" className="logo_socials_links logo_socials_links-yt"></a>
                    </div>
                </div>
                <div className="footer_quicklinks">
                    <Title style={{ marginBottom: 15, fontWeight: 'bolder' }} level={5}>Меню</Title>
                    <Link to="/" className="footer_links">Главная</Link>
                    <Link to="/about" className="footer_links">О нас</Link>
                    <Link to="/generator" className="footer_links">Генератор пособий</Link>
                    <Link to="/worksheets" className="footer_links">Интерактивные пособия</Link>
                </div>
                <div className="contactus_row">
                    <Title style={{ marginBottom: 15, fontWeight: 'bolder' }} level={5} className="footer_title">Связаться с нами</Title>
                    <div className="contactus-text">
                        <a className="whatsapp-link" target="_blank" href="https://wa.me/77079985106"><WhatsAppOutlined className="whatsapp-icon" /></a>
                        <a className="whatsapp-link" target="_blank" href="https://wa.me/77079985106">+77079985106</a>
                        <p className="contact-mail-text"><a href="mailto:officeclass2be@gmail.com">officeclass2be@gmail.com</a></p>
                    </div>
                </div>
            </div>
            <Divider />
            <div className="all-rights-row-box">
                <div className="privacy-text">
                    <p>
                        <a href="#" className="privacy-link">Пользовательское соглашение</a>
                    </p>
                    <p>
                        <a href="#" className="privacy-link">Политика конфиденциальности</a>
                    </p>
                </div>
                <div>
                    <span className="all-rigts-span text-color">© Attractor School</span>
                </div>
            </div>
        </Footer>
    )
}

export default FooterNav;