import {Card, Col, Button, Tooltip} from 'antd';
import {EditTwoTone} from '@ant-design/icons';
import {Link, useNavigate} from 'react-router-dom';

interface Props {
    title: string;
    worksheetId: string | number;
}

const noImage = '/no_image.jpg';

function WrittenAnswerItem({title, worksheetId}: Props) {
    const navigate = useNavigate();
    return (
        <Col span={8}>
            <Card
                onClick={() => {
                    navigate(`/worksheets/${worksheetId}`)
                }}
                hoverable
                size='small'
                style={{width: 230}}
                cover={<img style={{height: 200}} src={noImage} alt='noImage'/>}
                extra={<Link to='#'>
                    <Tooltip title='Редактировать'>
                        <Button shape="circle" icon={<EditTwoTone/>}></Button>
                    </Tooltip>
                </Link>}>
                <p>{title}</p>
            </Card>
        </Col>
    )
}

export default WrittenAnswerItem;