import { ChangeEvent } from 'react'
import { Col, Input, Typography, Image, Space } from 'antd';
import { inputValidity } from '../../../containers/Tutorials/TutorialsByStudents/InteractiveWrittenAnswer/InteractiveWrittenAnswer';
import { incorrectInputStyle, correctInputStyle } from './Style/WrittenQInput';

const { Text } = Typography;

interface Props {
    index: number;
    question: string;
    inputChangeHandler: (event: ChangeEvent<HTMLInputElement>, i: number) => void;
    inputAnswer: string;
    inputValidity: inputValidity;
    disabled: boolean;
    picturePath: string | null;
}

function WrittenQuestionInput({
    index,
    question,
    inputChangeHandler,
    inputAnswer,
    inputValidity,
    disabled,
    picturePath
}: Props) {
    return (
        <Col span={24} style={{ padding: '30px 0px 0px 30px', width: '100%' }}>
            <Space direction="vertical" size="middle" style={{ display: 'flex', maxWidth: 750, width: '100%' }}>
                <Text strong style={{ fontSize: 16 }}>{`${index + 1}. `}{question}</Text>
                {picturePath !== null
                    ?
                    <Image preview={false} width={250} src={picturePath} />
                    :
                    null}
                <div style={{ paddingRight: 15 }}>
                    <Input
                        autoComplete='off'
                        disabled={disabled}
                        style={inputValidity[index] === false ? incorrectInputStyle : inputValidity[index] === undefined ? undefined : correctInputStyle}
                        name='inputAnswer'
                        value={inputAnswer}
                        onChange={(e) => {
                            inputChangeHandler(e, index)
                        }} />
                </div>
            </Space>
        </Col>
    )
}

export default WrittenQuestionInput;