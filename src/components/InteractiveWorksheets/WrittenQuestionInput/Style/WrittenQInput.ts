import { CSSProperties } from "react";

export const incorrectInputStyle: CSSProperties = {
    backgroundColor: 'rgba(255,0,0,.5)',
    color: 'black'
}

export const correctInputStyle: CSSProperties = {
    backgroundColor: 'rgba(0,128,0,.5)',
    color: 'black'
}