import { ChangeEvent } from "react";
import { labelStyle } from "./StyleMatchTutorial/StyleMahjongComponent";


interface IMahjongComponent {
    onChangeImage: (e: ChangeEvent<HTMLInputElement>) => void
}

const MahjongComponent = ({onChangeImage}: IMahjongComponent) => {

    return (
        <>
            
                    <label style={labelStyle}>
                        Добавить картинку
                        <input
                            name="arrPicture"
                            style={{ display: "none" }}
                            type="file"
                            onInput={onChangeImage}
                        />
                    </label>
        </>
    );
};

export default MahjongComponent;