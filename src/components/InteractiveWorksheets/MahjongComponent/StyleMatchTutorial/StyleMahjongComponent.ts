import { CSSProperties } from "react";


export const labelStyle: CSSProperties = {
    background: '#1677ff',
    cursor: 'pointer',
    color: 'white',
    width: '20vw',
    borderRadius: '4px',
    height: '2rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: '700'
}

export const deleteButtonPicture: CSSProperties = {
    position: 'relative',
    bottom: '5rem',
    right: '1.5rem',
    color: 'red',
    mixBlendMode: 'difference',
    fontSize: '1.4rem',
}

export const pictureStyle: CSSProperties = {
    width: '150px',
    objectFit:'cover',
    height:'100px',
    borderRadius:'4px',
    transition:'0.5s'
}

export const iconStyle: CSSProperties = {
    color: 'gray',
    fontSize: '1.5rem',
    position:'relative',
    bottom :'6rem',
    left:'11.8rem',
}

export const cardStyle: CSSProperties = {
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    width:'100%'
}