import { Card, Popover } from 'antd'
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK'
import Title from 'antd/es/typography/Title'
import { DeleteOutlined,FileAddOutlined, SendOutlined } from '@ant-design/icons'
import StudentList from '../../../StudentList/StudentList'
import { useDeleteTutorialMutation, usePutTutorialPublicMutation } from '../../../../store/services/lessons'

const TutorialItem = (tutorial: ILessonByRTK) => {

    const [deleteTutorial] = useDeleteTutorialMutation()

    const [publicedTutorial] = usePutTutorialPublicMutation()

    return (
        
            <Card
                hoverable={true}
                bordered={true}
                className='flexible-card'
                style={{ display: 'flex', flexDirection: 'column', width: '100%' }}
                actions={ tutorial.public ? 
                    [
                        <Popover
                            placement='bottom'
                            title='Удалить'
                            children={<DeleteOutlined 
                                onClick={() => deleteTutorial(tutorial.id as number)}
                            />}
                        />,
                        <Popover
                            placement='bottom'
                            trigger='hover'
                            title='Отправить ученику'
                            children={<Popover
                                placement='top'
                                trigger='click'
                                title='Кому отправить пособие ?'
                                content={
                                    <StudentList 
                                        id={tutorial.id as number} 
                                    />
                                }
                                children={<SendOutlined/>}
                            />}
                        />
                    
                    ]
                    :
                    [
                        <Popover
                            placement='bottom'
                            title='Удалить'
                            children={<DeleteOutlined 
                                onClick={() => deleteTutorial(tutorial.id as number)}
                            />}
                        />,
                        <Popover
                            placement='bottom'
                            title='Опубликовать'
                            children={<FileAddOutlined 
                                onClick={() => publicedTutorial(tutorial.id as number)}
                            /> }
                        />
                    ]
                }
                
            >
                <Title
                    level={4}
                >
                    Название: {tutorial.title}
                </Title>
                <Title
                    level={5}
                >
                    Описание: {tutorial.description}
                </Title>
            
                <Title
                    level={5}
                >
                    Предмет: {tutorial.direction.direction}
                </Title>

            </Card>
       
    )
}

export default TutorialItem