import { useEffect, useState } from 'react'
import { IStudent } from '../../../interfaces/teacher/Teacher'
import { Avatar, Card, Popover } from 'antd'
import Title from 'antd/es/typography/Title'
import { apiUrl } from '../../../common/constants'
import { GetGrade } from '../../../interfaces/grade/IGrade'
import { useGetAllGradeToTeacherQuery } from '../../../store/services/grade'
import { DeleteOutlined } from '@ant-design/icons'


const StudentItem = (props:IStudent) => {

    
    const [grades , setGrades] = useState<GetGrade[]>([])

    const {data: arrGrade} = useGetAllGradeToTeacherQuery()

    const MidleGradeHander = (id : number):number => {


        const totalGrade:number = grades.reduce((acc , item) => {

            return Number(item.student_id) === id ? acc += Number(item.grade)  : acc += 0
     
        } , 0) 

        const countGrade:number = grades.reduce((acc , item) => {            
            
            return Number(item.student_id) === id ? acc+=1 : acc += 0
            
        } , 0) 

        

        return !isNaN(Math.ceil(totalGrade  / countGrade)) 
            ? 
            Math.ceil(totalGrade  / countGrade) 
            : 
            0
    }

    useEffect(() => {

        arrGrade && setGrades([...arrGrade])
        
        
    }, [ arrGrade])

    return (
        <div>
            <Card.Grid
                    
                    key={props.id}
                    style={{width:'30%' , height:'50vh'}}
                >
                    {/* <Descriptions title="User Info">
                        <Descriptions.Item label="Ученик">{props.student.firstName} 
                            {props.student.lastName}
                        </Descriptions.Item>
                        <Descriptions.Item label="Telephone">
                            <Avatar size={40} style={{marginLeft:'3vw'}} src={`${apiUrl}/uploads/user/${props.student.avatar}`} />
                        </Descriptions.Item>
                        <Descriptions.Item label="Live">Hangzhou, Zhejiang</Descriptions.Item>
                        <Descriptions.Item label="Remark">empty</Descriptions.Item>
                        <Descriptions.Item label="Address">
                            No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
                        </Descriptions.Item>
                    </Descriptions>; */}
                    <Title
                        level={4}    
                    >
                        Ученик  {
                            props.student.firstName
                        } {
                            props.student.lastName
                        }
                        <Avatar size={40} style={{marginLeft:'3vw'}} src={`${apiUrl}/uploads/user/${props.student.avatar}`} />

                    </Title>
                    <Title
                        level={4}    
                    >
                        Средняя оценка  {
                            MidleGradeHander(props.student_id)
                        } 
                    </Title>

                    <Popover
                        placement='bottom'
                        title='Удалить'
                        children={<DeleteOutlined 
                            onClick={() => {}}
                        />}
                    /> 
            </Card.Grid>
            
        </div>
    )
}

export default StudentItem