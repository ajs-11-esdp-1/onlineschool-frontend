import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';

const PageNotFound = () => {

    const navigate = useNavigate();

    return (
        <Result
            status="404"
            title="404"
            subTitle="Извините, страница, которую вы посетили, не существует."
            extra={<Button onClick={() => navigate('/')} type="primary">Вернуться на главную страницу</Button>}
        />
    )
}

export default PageNotFound;