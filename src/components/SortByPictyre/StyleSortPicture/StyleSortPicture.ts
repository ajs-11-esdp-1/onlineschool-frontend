import { CSSProperties } from "react";

export const labelStyle : CSSProperties = {
    background: '#33658A',
    color: 'white',
    width: '20vw',
    borderRadius: '4px',
    height: '2rem',
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    fontWeight: '700',
    cursor: 'pointer'
}
