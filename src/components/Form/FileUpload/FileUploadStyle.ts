import { CSSProperties } from "react";

export const buttonStyle: CSSProperties = {
    display: 'inline-block',
    borderRadius: '32px',
    paddingInlineStart: '16px',
    paddingInlineEnd: '16px',
    color: '#fff',
    backgroundColor: '#1677ff',
    boxShadow: '0 2px 0 rgba(5, 145, 255, 0.1)',
    fontSize: '14px',
    height: '32px',
    padding: '4px 15px',
    textAlign: 'center',
    border: '1px solid transparent',
    cursor: 'pointer',
    transition: 'all 0.2s cubic-bezier(0.645, 0.045, 0.355, 1)'
}