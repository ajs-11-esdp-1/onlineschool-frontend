import {ChangeEventHandler, useState, ChangeEvent} from 'react';
import {Row, Col} from 'antd';
import {DeleteOutlined, UploadOutlined} from '@ant-design/icons';
import {buttonStyle} from './FileUploadStyle';

interface Props {
    onChange: ChangeEventHandler<HTMLInputElement>;
    name: string;
    deletePicture: () => void;
}

const FileUpload = ({onChange, name, deletePicture}: Props) => {
    const [fileName, setFileName] = useState('');
    const onFileChange = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target && e.target.files && e.target.files[0]) {
            if (e.target.files[0].type === 'image/jpeg' || e.target.files[0].type === 'image/png' || e.target.files[0].type === 'image/svg') {
                setFileName(e.target.files[0].name);
            }
        } else {
            setFileName('');
        }
        onChange(e);
    }

    const deleteFileName = () => {
        deletePicture();
        setFileName('')
    }

    return (
        <>
            <Row>
                <Col>
                    <label style={buttonStyle}>
                        <UploadOutlined/> Добавить картинку
                        <input name={name} hidden style={{display: "none"}} accept="image/*" type="file"
                               onInput={onFileChange}/>
                    </label>
                </Col>
                <Col style={{marginTop: 5, marginLeft: 10}}>
                    {fileName} {fileName ? <DeleteOutlined onClick={deleteFileName}/> : null}
                </Col>
            </Row>
        </>
    )
};

export default FileUpload;