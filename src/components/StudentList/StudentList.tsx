import { useGetStudentByTeacherQuery } from '../../store/services/teacher'
import { Button, Form, Select, SelectProps, message } from 'antd'
import TagRender from '../Tags/TagRender';
import {  usePostNoticeStudentMutation } from '../../store/services/notice';
import { useAppSelector } from '../../hooks/reduxHooks';
import { RootState } from '../../store/store';
import { NoticeType } from '../../enum/noticeType/NoticeType';

interface ISelect {
    select: number[]
}

type LessonId = {id:number}

const StudentList = ({id}:LessonId) => {

    const [form] = Form.useForm();

    const {data: arrStudent} = useGetStudentByTeacherQuery()

    const [messageApi, contextHolder] = message.useMessage();

    const [postNotice] = usePostNoticeStudentMutation()

    const {user} = useAppSelector<RootState>(state => state.auth);

    
    const options: SelectProps['options'] = arrStudent ? arrStudent.map((val) => {
        return {
            label:`${val.student.firstName} ${val.student.lastName} `,
            value: val.student.id
        }
    })
    : []
    ;
    const success = () => {
        messageApi.open({
            type: 'success',
            content: 'Пособия успешно отправлены',
            duration:4
        });
    };

    const onFinish = ({select}: ISelect) => {

        postNotice({
            sender: user.id,
            recipient:select ,
            model_id: id,
            notice_type:NoticeType.LESSON
        })

        success()
        
        form.resetFields()
        

    }

    return (
        <div>
            {
                contextHolder
            }
            <Form onFinish={onFinish}>
                <Form.Item
                    name="select"
                >
                    <Select
                        mode="tags"
                        style={{ width: '100%' }}
                        placeholder="Выберите учеников"
                        options={options}
                        tagRender={TagRender}
                        
                    />
                </Form.Item>
                <Button
                    htmlType="submit"
                    type='primary'
                >
                    Отправить
                </Button>
            </Form>
        </div>
    )
}

export default StudentList