import { Button, Modal } from "antd"
import Paragraph from "antd/es/typography/Paragraph";

interface ModalProps {
    open: boolean;
    onOk: ((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
    taskDescription: string;
}

const StartTestModal = ({ open, onOk, taskDescription }: ModalProps) => {
    return (
        <Modal
            closeIcon={null}
            title="Задание"
            centered
            open={open}
            width={600}
            footer={[
                <Button key='closeBtn' type="primary" onClick={onOk}>
                    Старт
                </Button>]}>
            <Paragraph>
                {taskDescription}
            </Paragraph>
        </Modal>
    )
}

export default StartTestModal;