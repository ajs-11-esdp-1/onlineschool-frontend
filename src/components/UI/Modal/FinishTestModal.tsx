import { Button, Modal } from "antd"

interface ModalProps {
    open: boolean;
    onCancel: ((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
    closeModal: React.MouseEventHandler<HTMLElement> | undefined;
    finishFunc: React.MouseEventHandler<HTMLElement> | undefined;
}

const FinishTestModal = ({ open, onCancel, closeModal, finishFunc }: ModalProps) => {
    return (
            <Modal
                centered
                title='Завершить выполнение задания?'
                open={open}
                onCancel={onCancel}
                footer={[
                    <Button key='continue' onClick={closeModal}>
                        Продолжить отвечать
                    </Button>,
                    <Button key='finish' type="primary" onClick={finishFunc}>
                        Завершить
                    </Button>]}
            >
                <p>Вы уверены, что хотите закончить выполнение этого задания?</p>
            </Modal>
    )
}

export default FinishTestModal;