import { Button, Modal } from "antd";
import { CloseCircleFilled } from '@ant-design/icons';

interface ModalProps {
    open: boolean;
    onOk: ((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
}

const EmptyAnswersModal = ({ open, onOk }: ModalProps) => {
    return (
        <Modal
            centered
            title={<span> <CloseCircleFilled style={{ color: "#f82e30", marginLeft: 15 }} /> Ошибка</span>}
            open={open}
            onOk={onOk}
            onCancel={onOk}
            footer={[
                <Button key='closeButton' type="primary" onClick={onOk}>
                    Закрыть
                </Button>]}
        >
            <p>Вы должны ответить хотя бы на один вопрос, чтобы иметь возможность отправить свои ответы.</p>
        </Modal>
    )
}

export default EmptyAnswersModal;