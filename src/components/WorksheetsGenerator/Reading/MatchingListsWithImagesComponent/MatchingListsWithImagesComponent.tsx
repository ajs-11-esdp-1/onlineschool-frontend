import {IWordObject} from "../../../../interfaces/generatedTutorials/IMatchingListsWithImages/IMatchingListsWithImages";
import React from "react";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import {Button} from "antd";


interface IMatchingListsWithImagesComponentProps {
    val: IWordObject[];
}

const shuffleArray = (array: string[]) => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
};

const MatchingListsWithImagesComponent: React.FC<IMatchingListsWithImagesComponentProps> = ({ val }) => {

    const images: string[] = val.map((wordObject) => wordObject.image);

    const shuffledImages = [...images];
    shuffleArray(shuffledImages);

    const shuffledSelectedWords = val.map((wordObject, index) => ({
        ...wordObject,
        image: shuffledImages[index],
    }));

    const saveAsPDF = async () => {
        const elementToCapture = document.getElementById('worksheet-container');

        if (elementToCapture) {
            try {
                const canvas = await html2canvas(elementToCapture);
                const imgData = canvas.toDataURL('image/png');

                const pdf = new jsPDF('p', 'mm', 'a4');
                await  pdf.addImage(imgData, 'PNG', 10, 10, 190, 0);
                await  pdf.save('matchingTutorial.pdf');
            } catch (error) {
                console.error('Error generating PDF:', error);
            }
        }
    };


    return (
        <div style={{width:'300px', height:'400px'}}>
            <div  id='worksheet-container' style={{border: '2px solid green', textAlign: 'center', width: '100%', height:'100%'}}>
                <p>Сопоставление слов с изображениями</p>
                {shuffledSelectedWords.map((val, index) => (
                    <div
                        key={val.id}
                        style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', margin: '0 30px'}}
                    >
                        <p style={{fontSize: '10px'}}>
                            {val.word}
                        </p>
                        <div>
                            <img
                                style={{ maxWidth: '30px', maxHeight: '30px' }}
                                alt={`Image ${index + 1}`}
                                src={val.image}
                            />
                        </div>
                    </div>
                ))}
            </div>
            <Button
                style={{marginTop: '5px'}}
                type="dashed"
                onClick={saveAsPDF}
            >
                Сохранить как PDF
            </Button>
        </div>
    );
};

export default MatchingListsWithImagesComponent;
