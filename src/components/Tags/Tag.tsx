import React, {CSSProperties, ReactNode} from 'react'
import {Tag} from 'antd'

interface TagProps {
    label: ReactNode
    onClose: () => void
    closable: boolean
    style: CSSProperties
}

const Tags = ({label, onClose, closable, style}: TagProps) => {
    const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.preventDefault();
        event.stopPropagation();
    };

    return (
        <Tag
            color='cyan'
            onMouseDown={onPreventMouseDown}
            onClose={onClose}
            closable={closable}
            style={style}
        >
            {label}
        </Tag>
    )
};

export default Tags;