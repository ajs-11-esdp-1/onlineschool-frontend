import axios from 'axios'
import {useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'

const Confirmation = () => {
    const params = useParams()
    const [load, setLoad] = useState<boolean>(false);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const postCodeAuth = async () => {
        try {
            await axios.get(`http://localhost:8000/confirm?confirmationCode=${params.confirmationCode}`);

            setLoad(true);
        } catch (error) {
            console.log('FAK');
        }
    };

    useEffect(() => {
        postCodeAuth().catch(e => console.log(e));
    }, [postCodeAuth]);

    return (
        <>
            <h2>LOAD</h2>
            {
                !load ?
                    <div>
                        <h2>{params.email}</h2>
                        <h2>{params.confirmationCode}</h2>
                    </div>
                    :
                    null
            }
        </>
    )
};

export default Confirmation;