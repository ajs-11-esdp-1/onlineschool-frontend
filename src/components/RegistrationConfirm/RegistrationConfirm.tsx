import { Card, Space } from 'antd';
import { MailTwoTone } from "@ant-design/icons";

const RegistrationConfirm = () => {
    return (
        <>
            <Space style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '70vh'}}>
                <Card title={
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <p>Проверьте свою электронную почту</p><MailTwoTone style={{ fontSize: '24px', marginLeft: '10px' }} />
                    </div>}
                    bordered={false} style={{ width: 650, textAlign: 'center' }}>
                    <p>На Ваш почтовый ящик отправлено письмо, содержащее ссылку для подтверждения e-mail адреса.
                        Пожалуйста, перейдите по ссылке для завершения регистрации. Если вы не получили письмо, пожалуйста, проверьте папку "Спам".
                    </p>
                    <p>
                        <a href="#">Отправить письмо еще раз</a>
                    </p>
                </Card>
            </Space>
        </>
    )
}

export default RegistrationConfirm;