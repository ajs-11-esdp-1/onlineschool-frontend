import { List } from "antd";
import Paragraph from "antd/es/typography/Paragraph";
import Title from "antd/es/typography/Title";
import { ISF } from "../../interfaces/tutorial/Lesson";
import { QuizTutorial } from "../../interfaces/tutorial/quizTutorial/quizTutorial";

const QuizPreview = ({ lesson }: ISF) => {

    const data: Pick<QuizTutorial, 'lesson'> = { lesson: JSON.parse(lesson) }

    return (
        <div>
            <Title level={2}>Ответы студента</Title>
            <List
                itemLayout="vertical"
                dataSource={data.lesson.quiz}
                renderItem={(question, index) => (
                    <List.Item key={index}>
                        <Title level={3}>Вопрос: {question.question}</Title>
                        <Paragraph>Правильный ответ: {question.correctAnswer}</Paragraph>
                        <Paragraph>Ответ студента: {data.lesson.student_answers[index]}</Paragraph>
                    </List.Item>
                )}
            />
        </div>
    )
}

export default QuizPreview;