import { Card } from "antd";
import { ISF } from "../../interfaces/tutorial/Lesson";
import { IMahjong } from "../../interfaces/tutorial/mahjongInterface/IMahjong";
import { apiUrl } from "../../common/constants";
import { style } from "./PreviewStyle/PreviewStyle";

const MahjongPreview = ({lesson}:ISF) => {

    const data: Pick<IMahjong , 'lesson'> = {lesson : JSON.parse(lesson)}

    return (
        <div
        >
            <Card>
                {data.lesson.images.map((tile) => (
                    <Card.Grid
                        key={tile.id}
                        style={{
                            ...style,
                            transform: `rotateY(${tile.isVisible ? '0' : '180'}deg)`,
                        }}
                        className={tile.id}
                    >
                        {tile.isVisible && (
                            <img
                                src={`${apiUrl}/uploads/tutorials/${tile.value}`}
                                alt={`Image ${tile.id}`}
                                style={{ maxWidth: '100%', maxHeight: '100%' }}
                            />
                        )}
                    </Card.Grid>
                ))}
            </Card>
        </div>
    )
}

export default MahjongPreview;