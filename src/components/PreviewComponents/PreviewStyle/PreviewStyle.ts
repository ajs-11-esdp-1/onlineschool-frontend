import { CSSProperties } from "react";

export const container : CSSProperties = {
    maxHeight: '50vh',
    overflow: 'scroll'
}

export const style: CSSProperties = {
    background: 'cadetblue',
    width: '250px',
    height: '130px',
    transform: 'rotateY(180deg)',
    display: 'flex',
    alignItems:'center',
    justifyContent:'center',
}

export const pictureStyle : CSSProperties = {
    width: '14rem',
    height: '12rem',
    objectFit:'cover',
    marginLeft: '1.3vh',
}
export const pictureStyleDrag : CSSProperties = {
    height: '12rem',
    marginLeft: '1.3vh',
    background: 'rgba(0,0,0,0.1)',
    borderRadius:'4px',
    marginTop:'2vh',
    
}