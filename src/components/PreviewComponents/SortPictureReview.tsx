import { Card, Col, Row } from 'antd'
import { apiUrl } from '../../common/constants'
import { SortByPicture } from '../../interfaces/tutorial/sortByPicture/SortByPicture'
import { pictureStyle, pictureStyleDrag } from './PreviewStyle/PreviewStyle'
import { ISF } from '../../interfaces/tutorial/Lesson'

const SortPicturePreview = ({lesson}:ISF) => {

    const data: Pick<SortByPicture , 'lesson'> = {lesson : JSON.parse(lesson)}

    console.log(data);
    

    return (
        <Row gutter={[64 , 64]} >
                    {
                        data.lesson.theme.map((obj) => {
                            return <Col span={12}
                                key={obj.id}
                            >
                                <Card
                                    title={obj.theme}
                                    hoverable={true}
                                >
                                    <Row 
                                        gutter={[2,32]} 
                                        justify={'center'}
                                    >
                                        {
                                            data.lesson.arrPicture.map((val ) => {
                                                               
                                                return val.theme_id === obj.id &&  

                                                    (
                                                        !val.dragbel ? 
                                              
                                                    <img 
                                                        key={val.id + Math.random()}
                                                        style={pictureStyle} 
                                                        src={`${apiUrl}/uploads/tutorials/${val.picture}`}
                                                                                              
                                                    />
                                                    :
                                                    <Col
                                                        span={24}
                                                        style={pictureStyleDrag}
                                                        
                                                    />
                                                    )
                                                
                                            })
                                        }
                                    </Row>
                                </Card>
                            </Col>
                        })
                    }
                </Row>
    )
}

export default SortPicturePreview