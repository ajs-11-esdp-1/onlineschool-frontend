import { Tag, Typography } from 'antd'
import { ISortObject } from '../../interfaces/tutorial/sortByWords/SortByWords'
import { dragBlur, dragStyle, dragbleStyle, gridStyle } from '../../containers/Tutorials/TutorialsByStudents/SortWordsTutorial/StyleSortWords/Style.style.words'
import { container } from './PreviewStyle/PreviewStyle'
import { ISF } from '../../interfaces/tutorial/Lesson'




const SortWordReview = ({lesson}:ISF) => {
    
    const data: Pick<ISortObject , 'lesson'> = {lesson : JSON.parse(lesson)}

    return (
        <div 
            style={container}
        >
            <Typography style={{margin : '4vh  0'}} >
                {
                    data.lesson.arrWords.map((val ) => {
                        return <span 
                                key={val.id} 
                                
                                style={
                                    val.styleHandler
                                    ?  dragBlur : (val.isDrable ? dragStyle : gridStyle ) }
                            >{val.value}</span>
                    })
                }
            </Typography>
            {                    
                data.lesson.checkedWord.map((val ) => {
                    return <Tag
                            style={val.isDrable ? gridStyle : dragbleStyle} 
                            color="#5BC0EB" 
                            key={val.id} 
                        >{val.value}</Tag>
                    
                })
            }
            
        </div>
    )
}

export default SortWordReview