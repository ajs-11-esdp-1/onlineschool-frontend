import { Typography, Image, Space } from 'antd';
import { IQAGrade } from '../../interfaces/tutorial/writtenAnswer/IWrittenAnswerForm';
import { apiUrl } from '../../common/constants';
import { incorrectInputStyle, correctInputStyle } from '../../components/InteractiveWorksheets/WrittenQuestionInput/Style/WrittenQInput';
import { ISF } from '../../interfaces/tutorial/Lesson';


const { Title, Text } = Typography;
function WrittenAnswerPreview({ lesson }: ISF) {

    const data: Pick<IQAGrade, 'lesson_answer'> = { lesson_answer: JSON.parse(lesson) }
    
    return (
        <Space direction='vertical'>
            <Space style={{ paddingBottom: 30, overflow: 'hidden', maxWidth: 800, borderRadius: '4px', border: '1px solid #ccc', background: '#fff', flexDirection: 'column', alignItems: 'flex-start' }}>
                {data.lesson_answer.studentAnswers.map((item, index) => {
                    return (
                        <Space direction="vertical" size="middle" style={{ display: 'flex', maxWidth: 750 }} key={index + Math.random()}>
                            <Text strong style={{ fontSize: 16 }}>{`${index + 1}. `}{item.question}</Text>
                            {item.picture !== null
                                ?
                                <Image style={{maxWidth: 150}} preview={false} src={`${apiUrl}/uploads/tutorials/${item.picture}`} />
                                :
                                null}
                            <Title
                                
                                level={5}
                                children={item.student_answer}
                                style={item.isCorrect ? correctInputStyle : incorrectInputStyle} />
                            <Text>
                                <Text strong>Правильные ответы: </Text>{item.correct_answers.map((item, index) => {
                                    return (
                                        <p key={index}> {item.answer}  </p>
                                    )
                                })}
                            </Text>
                        </Space>
                    )
                })}
            </Space>
        </Space>
    )
}

export default WrittenAnswerPreview;