import { Avatar, Badge } from 'antd';
import { useEffect, useState } from 'react';
import { useGetReadNoticeQuery } from '../../store/services/notice';
import { apiUrl } from '../../common/constants';
import { INotice } from '../../interfaces/notice/Notice';
import { useAppSelector } from '../../hooks/reduxHooks';
import { RootState } from '../../store/store';


const NoticeComponentStudent = () => {

    const [notice , setNotice] = useState<INotice[]>([])

    const {data:arrNotice , refetch} = useGetReadNoticeQuery()
    
    const {user} = useAppSelector<RootState>(state => state.auth);

    const noticeCount = (): number => {
        if(arrNotice) {            
            return arrNotice.length
        }
        return 0
    }

    const changeHandler = async() => {
        await refetch()

        arrNotice && await setNotice(() => {
            return arrNotice
        })
    }
    useEffect(() =>{

        changeHandler()

    },[user])



    return (
        <>
            {
                notice.length !== 0 ?
                <Badge count={noticeCount()}>
                    <Avatar.Group 
                        maxCount={1} 
                        maxStyle={{ color: '#f56a00', backgroundColor: '#fde3cf' }}
                        shape='circle'
                        size='small'
                    >
                        {   
                            notice.map((val) => {
                                return <Avatar 
                                        shape="circle"
                                        src={`${apiUrl}/uploads/user/${val.from_user.avatar}`}
                                    />
                            })
                        }
                        
                    </Avatar.Group>
                    

                </Badge>
                : 'Уведомления'
            }   
              
        </>
        
    )
}

export default NoticeComponentStudent