import { Badge, Collapse, Row } from 'antd'
import { INotice } from '../../../interfaces/notice/Notice'
import Title from 'antd/es/typography/Title'
import { usePutNoticeReadMutation } from '../../../store/services/notice'


const NoticeMessage = ({message , from_user ,id , isRead }:Pick<INotice , 'message' | 'from_user' | 'id' | 'isRead' >) => {

    const [putNotice] = usePutNoticeReadMutation()

    const changeReadStatus = async() => {
        !isRead && await putNotice(id.toString())

        console.log(await isRead);
        
        
    } 
    
    return (
            
                <Collapse
                    expandIconPosition='end'
                    ghost
                    items={[
                        {
                        key: id,
                        label: <Row>
                                {
                                    !isRead &&
                                    <Badge
                                        dot
                                        color='orange'
                                    />
                                } 
                                <Title level={5} >{from_user.firstName} {from_user.lastName} </Title>
                            </Row>,
                        children: <p>{message}</p>,
                        
                        }
                    ]}
                    onChange={() => changeReadStatus()}
                />
    )
}

export default NoticeMessage