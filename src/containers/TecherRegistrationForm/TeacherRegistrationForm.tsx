import {
    Button,
    Checkbox,
    Form,
    Input,
    Alert
} from 'antd';
import { ChangeEvent, useState, useMemo, useEffect } from 'react';
import { RegisterForm } from '../../interfaces/auth/RegisterForm';
import { useNavigate } from 'react-router-dom';
import { useSignUpMutation } from '../../store/services/auth';
import { MaskedInput } from 'antd-mask-input';
import '../Registration/RegistrationStyle.css';

const formItemLayout = {
    labelCol: {
        xs: { span: 10 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const TeacherRegistrationForm = () => {
    const phoneMask = '(000)000-00-00';

    const mask = useMemo(
        () => [
            {
                mask: phoneMask,
                lazy: false,
            },
        ],
        []
    );

    const [form] = Form.useForm();
    const navigate = useNavigate();

    const [registerForm, setRegisterForm] = useState<RegisterForm>({
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        phone: '',
        role_id: '2'
    });

    const [signUp, { isError }] = useSignUpMutation();
    const [showAlert, setShowAlert] = useState(false);

    const passwordRegExp = new RegExp('^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9@$!%*#?&^_-]{8,}$');

    const handleClose = () => {
        setShowAlert(false);
    };

    const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;

        setRegisterForm(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const onFinish = async () => {

        const data = await signUp(registerForm);

        if (!(data as { error: object }).error) {
            setRegisterForm({
                firstName: '',
                lastName: '',
                email: '',
                password: '',
                phone: '',
                role_id: '2'
            })
            form.resetFields();
            navigate('/verification');
        }
    };

    useEffect(() => {
        setShowAlert(isError);
    }, [isError]);

    return (
        <div className='sub-register'>
            {showAlert && (
                <Alert onClose={handleClose} message={'Что-то пошло не так. Попробуйте еще раз.'} type="error" closable
                    showIcon />
            )}
                <Form
                    {...formItemLayout}
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    style={{ paddingTop: 15 }}
                    scrollToFirstError
                >
                    <Form.Item
                        name="firstName"
                        label="Имя"
                        rules={[{ required: true, message: 'Укажите имя', whitespace: true }]}
                    >
                        <Input value={registerForm.firstName} name='firstName' onChange={inputChangeHandler} />
                    </Form.Item>
                    <Form.Item
                        name="lastName"
                        label="Фамилия"
                        rules={[{ required: true, message: 'Укажите фамилию', whitespace: true }]}
                    >
                        <Input value={registerForm.lastName} name='lastName' onChange={inputChangeHandler} />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                type: 'email',
                                message: 'Пожалуйста, введите корректный адрес электронной почты.',
                            },
                            {
                                required: true,
                                message: 'Укажите адрес электронной почты',
                            },
                        ]}
                    >
                        <Input value={registerForm.email} name='email' onChange={inputChangeHandler} />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        label="Пароль"
                        rules={[
                            {
                                required: true,
                                message: 'Укажите пароль'
                            },
                            {
                                message: 'Используйте не менее 8 символов',
                                min: 8
                            },
                            {
                                message: 'Пароль должен содержать цифры, буквы верхнего и нижнего регистров латинского алфавита',
                                pattern: passwordRegExp
                            }
                        ]}
                        hasFeedback
                    >
                        <Input.Password maxLength={20} value={registerForm.password} name='password'
                            onChange={inputChangeHandler} />
                    </Form.Item>
                    <Form.Item
                        name="confirm"
                        label="Подтвердите пароль"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Необходимо подтвердить пароль',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('Введенный пароль не совпадает'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        label="Номер телефона"
                        rules={[{ required: true, message: 'Укажите корректный номер телефона' }]}
                    >
                        <MaskedInput
                            value={registerForm.phone}
                            name='phone'
                            onChange={(e) => {
                                setRegisterForm(prevState => ({ ...prevState, phone: '7' + e.unmaskedValue }))
                            }}
                            addonBefore='+7'
                            mask={mask}
                        />
                    </Form.Item>
                    <Form.Item
                        name="agreement"
                        valuePropName="checked"
                        rules={[
                            {
                                validator: (_, value) =>
                                    value ? Promise.resolve() : Promise.reject(new Error('Необходимо принять соглашение')),
                            },
                        ]}
                        {...tailFormItemLayout}
                    >
                        <Checkbox>
                            {`Я ознакомлен(а) с`} <a href="#">пользовательским соглашением</a>
                        </Checkbox>
                    </Form.Item>
                    <Form.Item style={{ display: 'flex', justifyContent: 'center' }} {...tailFormItemLayout}>
                        <Button style={{ marginLeft: 10 }} type="primary" htmlType="submit">
                            Зарегистрироваться
                        </Button>
                    </Form.Item>
                </Form>
        </div>
    )
};

export default TeacherRegistrationForm;