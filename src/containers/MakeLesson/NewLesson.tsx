import { Form, Input, Select, Row, Col, Divider } from 'antd';
import { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useGetDirectionQuery } from '../../store/services/direction';
import {
    changeDescription,
    changeDirection,
    changeTeacher,
    changeTemplate,
    changeTitle,
} from '../../store/features/lesson';
import { useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../store/store';
import { useGetTemplateQuery } from '../../store/services/template';
import { useAppSelector } from '../../hooks/reduxHooks';
import { TutorialTypeEnum } from '../../enum/tutorialType/TutorialTypeEnum';
import Title from 'antd/es/typography/Title';

const NewLesson = () => {
    const dispatch = useDispatch<AppDispatch>();
    const navigate = useNavigate();

    const { user } = useAppSelector<RootState>(state => state.auth);
    const lesson = useAppSelector<RootState>(state => state.lesson);

    const { data: arrDirection } = useGetDirectionQuery();
    const { data: arrTemplate } = useGetTemplateQuery();

    const changeTemplateHandler = (e: number) => {
        if (arrTemplate) {

            const index: number = arrTemplate?.findIndex((val) => {
                return val.id === e
            })
            if (index >= 0) {
                dispatch(changeTemplate(e));

                if (arrTemplate[index].title === TutorialTypeEnum.wordsSort) {
                    navigate(`/new/lesson/sort/word/${arrTemplate[index].title}`)
                } else if (arrTemplate[index].title === TutorialTypeEnum.pictureSort) {
                    navigate(`/new/lesson/sort/picture/${arrTemplate[index].title}`)
                } else if (arrTemplate[index].title === TutorialTypeEnum.writtenAnswer) {
                    navigate(`/new/lesson/written/answer/${arrTemplate[index].title}`)
                } else if (arrTemplate[index].title === TutorialTypeEnum.mahjong) {
                    navigate(`/new/lesson/mahjong/${arrTemplate[index].title}`)
                } else if (arrTemplate[index].title === TutorialTypeEnum.matchWordPicture) {
                    navigate(`/new/lesson/match/word-picture/${arrTemplate[index].title}`)
                } else if (arrTemplate[index].title === TutorialTypeEnum.quizTutorial) {
                    navigate(`/new/lesson/quiz/${arrTemplate[index].title}`)
                }
            }
        }
    };

    useEffect(() => {
        dispatch(changeTeacher(user.id))
    }, [dispatch, user.id]);

    return (
        <div className='container'>
            <Row gutter={[8, 16]}>
                <Col span={24}>
                    <Title style={{ paddingTop: '60px' }} level={2}>Создайте новое интерактивное пособие</Title>
                </Col>
                <Col span={24}>
                    <Form layout='vertical' size='large' style={{ maxWidth: 600 }}>

                        <Col span={24}>
                            <Form.Item
                                label={<span style={{ fontSize: 17, fontWeight: 'bolder' }}>Название</span>}
                                rules={[{ required: true, message: 'Это поле является обязательным' }]}
                            >
                                <Input

                                    value={lesson.title}
                                    onChange={(e) => dispatch(changeTitle(e.target.value))}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                label={<span style={{ fontSize: 17, fontWeight: 'bolder' }}>Описание</span>}
                                rules={[{ required: true, message: 'Это поле является обязательным' }]}
                            >
                                <Input.TextArea
                                    onChange={(e) => dispatch(changeDescription(e.target.value))}
                                    value={lesson.description}

                                />
                            </Form.Item>
                        </Col>
                    </Form>
                </Col>
                <Col span={24}>
                    <div style={{ maxWidth: 200 }}>
                        <Select
                            style={{ width: '100%' }}
                            defaultValue="Выберите предмет"
                            onChange={(e) => dispatch(changeDirection(Number(e)))}
                            options={
                                arrDirection?.map((val) => {
                                    return { value: val.id, label: val.direction }
                                })
                            }
                        />
                    </div>
                </Col>
                <Col span={24}>
                    <div style={{ maxWidth: 330 }}>
                        <Select
                            style={{ width: '100%' }}
                            defaultValue="Выберите пособие"
                            onChange={(e) => changeTemplateHandler(Number(e))}
                            options={
                                arrTemplate?.map((val) => {
                                    return { value: val.id, label: val.title }
                                })
                            }
                        />
                    </div>
                </Col>
                <Divider />
                <Col span={24}>
                    <Outlet />
                </Col>
            </Row>

        </div>
    )
};

export default NewLesson;