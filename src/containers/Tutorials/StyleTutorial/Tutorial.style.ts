import { CSSProperties } from "react";

export const buttonStyle : CSSProperties = {
    color:'white',
    background: '#33658A',
    marginTop:'3vh'
}

export const buttonClose : CSSProperties = {
    color:'white',
    background: '#33658A',
    marginTop:'3vh',
    position: 'fixed',
    top: '7vh',
    left: '0',
    zIndex: 100   

}