import { useEffect, useState } from 'react';
import './StyleQuizStudent/QuizStudent.css';
import { useParams } from 'react-router-dom';
import { useGetPublicLessonQuery } from '../../../../store/services/lessons';
import { QuizTutorial } from '../../../../interfaces/tutorial/quizTutorial/quizTutorial';
import { Button, Card, Radio, RadioChangeEvent, Space } from 'antd';
import Title from 'antd/es/typography/Title';
import Countdown from 'antd/es/statistic/Countdown';
import Paragraph from 'antd/es/typography/Paragraph';
import Grade from '../../../../components/Grade/Grade';
import { RootState } from '../../../../store/store';
import { useAppSelector } from '../../../../hooks/reduxHooks';
import { useSaveGradeMutation } from '../../../../store/services/grade';
import StartTestModal from '../../../../components/UI/Modal/StartTestModal';


const Quiz = () => {

    const [quizArr, setQuiz] = useState<QuizTutorial>()

    const [answer, setAnswer] = useState<string[]>([])

    const [start, setStart] = useState<boolean>(false)

    const [finish, setFinish] = useState<boolean>(false)

    const [postAnswer] = useSaveGradeMutation()

    const params = useParams()

    const { data: arrLessons } = useGetPublicLessonQuery()

    const { user } = useAppSelector<RootState>(state => state.auth)

    const changeOptionHandler = (e: RadioChangeEvent, index: number) => {

        if (answer) {

            const copy: string[] = [...answer]

            copy.splice(index, 1, e.target.value)

            setAnswer(copy)
        }

    }

    const resultHandler = (): number => {
        setFinish(false)
        let result: number = 0
        if (quizArr) {
            answer.map((val, index) => {
                if (val === quizArr.lesson.quiz[index].correctAnswer) {
                    result += 10 / quizArr.lesson.quiz.length
                }
            })

            if (quizArr && user.role.role === 'student') {
                const lessonData = {
                    ...quizArr.lesson,
                    student_answers: answer,
                };

                postAnswer({
                    grade: Math.ceil(result).toString(),
                    lesson_id: quizArr.id as number,
                    transit_time: quizArr.transit_time,
                    student_id: user.id,
                    teacher_id: quizArr.teacher_id,
                    lesson_answer: JSON.parse(JSON.stringify(lessonData))
                })
            }
        }

        return Math.ceil(result)
    }


    const changeTime = (e: number) => {

        const time: number = e / 1000

        quizArr && setQuiz({ ...quizArr, transit_time: time.toString() })

    }

    useEffect(() => {

        if (arrLessons && params.id) {

            const index = arrLessons.findIndex((val) => {
                return val.id === Number(params.id)
            })
            if (index >= 0) {

                const copy: QuizTutorial =
                {
                    ...arrLessons[index],
                    lesson: JSON.parse(arrLessons[index].lesson)
                }

                setAnswer(() => {
                    return copy.lesson.quiz.map(() => {
                        return ''
                    })
                })

                setQuiz({ ...copy })


            }
        }

    }, [arrLessons])

    return (

        <Space className='container' direction='vertical' style={{ width: '100%', padding: 30, paddingLeft: 70 }} >
            {
                quizArr && quizArr.lesson.quiz.map((val, index) => {
                    return <Card
                        style={{ width: '30%' }}
                        key={index}
                    >
                        <Title level={3} >
                            {val.question}
                        </Title>
                        <Radio.Group
                            onChange={(e) => changeOptionHandler(e, index)}
                            value={answer[index]}
                            disabled={finish}
                        >
                            <Space direction="vertical">
                                {
                                    val.options.map((opt, optionIndex) => {
                                        return <Radio
                                            value={opt.option}
                                            key={`${index}-${optionIndex}`}
                                        >{opt.option}
                                        </Radio>
                                    })
                                }
                            </Space>
                        </Radio.Group>
                        {finish && (
                            <Paragraph style={{ marginTop: '10px' }}>
                                Правильный ответ: {val.correctAnswer}
                            </Paragraph>
                        )}
                    </Card>


                })
            }
            <Countdown
                style={{ display: 'none' }}
                title="Время для прохождения"
                value={Date.now() + (start && quizArr && !finish ? Number(quizArr.transit_time) : 0) * 1000}
                onChange={(e) => changeTime(Number(e))}
                onFinish={() => setFinish(true)}
            />
            <Button
                type='primary'
                size='large'
                onClick={() => setFinish(true)}
            >
                Проверить
            </Button>
            <StartTestModal
                open={!start}
                onOk={() => setStart(!start)}
                taskDescription='Выбери правильный ответ'
            />
            {
                Grade({ resultHandler: resultHandler, finish: finish })
            }
        </Space>

    );
};

export default Quiz;