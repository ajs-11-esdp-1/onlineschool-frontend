import React, { useState, useEffect, useRef } from 'react';

interface TimerControl {
    startTimer: () => void;
    stopTimer: () => void;
    elapsedTime: React.MutableRefObject<number>;
    getTime: () => number;
    resetTime: () => void;
}

function useTimer(): TimerControl {
    const elapsedTime = useRef(0);
    const [intervalId, setIntervalId] = useState<number | null>(null);

    const startTimer = () => {
        const id = setInterval(() => {
            elapsedTime.current += 1
        }, 1000)
        setIntervalId(id);
    }

    const stopTimer = () => {
        if (intervalId !== null) {
            clearInterval(intervalId);
            setIntervalId(null);
        }
    }

    const resetTime = () => {
        elapsedTime.current = 0;
    }

    const getTime = () => elapsedTime.current;

    useEffect(() => {
        return () => {
            if (intervalId !== null) {
                clearInterval(intervalId);
            }
        };
    }, [intervalId]);

    return {startTimer, stopTimer, elapsedTime, resetTime, getTime}
}

export default useTimer;