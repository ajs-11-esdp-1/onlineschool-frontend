import { ChangeEvent, useState, useEffect } from 'react'
import { Space, Typography, Button, Row, message } from 'antd';
import Grade from '../../../../components/Grade/Grade';
import WrittenQuestionInput from '../../../../components/InteractiveWorksheets/WrittenQuestionInput/WrittenQuestionInput';
import { useGetPublicLessonQuery } from '../../../../store/services/lessons';
import { useSaveGradeMutation } from '../../../../store/services/grade';
import { useParams } from 'react-router-dom';
import { IWrittenAnswerForm } from '../../../../interfaces/tutorial/writtenAnswer/IWrittenAnswerForm';
import { apiUrl } from '../../../../common/constants';
import { useAppSelector } from '../../../../hooks/reduxHooks';
import { RoleEnum } from '../../../../enum/roleEnum/RoleEnum';
import { IQAGrade } from '../../../../interfaces/tutorial/writtenAnswer/IWrittenAnswerForm';
import MessageEnum from '../../../../enum/messageEnum/MessageEnum';
import useTimer from './useTimer';
import FinishTestModal from '../../../../components/UI/Modal/FinishTestModal';
import EmptyAnswersModal from '../../../../components/UI/Modal/EmptyAnswersModal';
import StartTestModal from '../../../../components/UI/Modal/StartTestModal';


const { Title, Text } = Typography;

interface InputAnswer {
    inputAnswer: string;
    id: number;
}

export interface inputValidity {
    [key: number]: boolean | undefined;
}
export type GradeObject = Pick<IQAGrade, 'lesson_answer'>;

function InteractiveWrittenAnswer() {

    const { user } = useAppSelector(state => state.auth);
    const { id } = useParams();
    const { data: arrLessons } = useGetPublicLessonQuery();

    const { startTimer, stopTimer, resetTime, getTime } = useTimer()

    const [messageApi, contextHolder] = message.useMessage();
    const [saveGrade] = useSaveGradeMutation();

    const [worksheet, setWorksheet] = useState<IWrittenAnswerForm>();
    const [inputsArr, setInputsArr] = useState<InputAnswer[]>([]);
    const [inputValidity, setInputValidity] = useState({});
    const [studentAnswerArr, setStudentAnswerArr] = useState<GradeObject>({
        lesson_answer: {
            studentAnswers: [],
            title: '',
            description: ''
        }
    })

    const [booleanState, setBooleanState] = useState({
        inputsDisabled: false,
        finishButtonVisible: true,
        refreshButtonVisible: false,
        modalOpen: false,
        start: false,
        finish: false,
        checkGarde: false
    });

    const messageBy = (message: string, type: MessageEnum) => {
        messageApi.open({
            type: type,
            content: message,
        });
    };

    useEffect(() => {
        if (arrLessons && id) {

            const index = arrLessons.findIndex((val) => {
                return val.id === Number(id)
            });

            if (index >= 0) {

                const copy: IWrittenAnswerForm =
                {
                    ...arrLessons[index],
                    lesson: JSON.parse(arrLessons[index].lesson)
                }
                setWorksheet(copy);
            }
            if (worksheet?.lesson.questions.length) {
                setInputsArr(
                    Array(worksheet.lesson.questions.length).fill(null).map((_, index) => {
                        return (
                            { inputAnswer: '', id: index }
                        )
                    })
                );
                setStudentAnswerArr(prevState => ({
                    ...prevState, lesson_answer: {
                        title: worksheet.title,
                        description: worksheet.description,
                        studentAnswers: Array(worksheet.lesson.questions.length).fill(null).map((_, index) => {
                            return (
                                { student_answer: '', question: worksheet.lesson.questions[index].question, correct_answers: worksheet.lesson.questions[index].answers, isCorrect: false, picture: worksheet.lesson.questions[index].picture }
                            )
                        })
                    }
                }));
            }
        }
    }, [arrLessons, id, worksheet?.lesson.questions?.length]) //не добавлять worksheet.lesson.questions, вызывает много перерендеров

    const handleStart = () => {
        setBooleanState(prevState => ({ ...prevState, start: !booleanState.start }));
        startTimer();
    }

    const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>, i: number) => {
        const index = inputsArr.findIndex(input => input.id === i);
        const inputArrCopy = [...inputsArr];
        const copyInput = { ...inputArrCopy[index] };
        copyInput.inputAnswer = event.target.value;
        inputArrCopy[index] = copyInput;
        const answersCopy = [...studentAnswerArr.lesson_answer.studentAnswers];
        const copyAnsObj = { ...answersCopy[index] };
        copyAnsObj.student_answer = event.target.value;
        answersCopy[index] = copyAnsObj;
        const updatedAnswersArr = answersCopy.map((item) => {
            return {
                ...item,
                student_answer: item.student_answer.toLowerCase()
            }
        })

        setStudentAnswerArr(prevState => ({
            ...prevState, lesson_answer: {
                ...prevState.lesson_answer,
                studentAnswers: updatedAnswersArr
            }
        }))
        setInputsArr(inputArrCopy);
    }

    const checkAnswers = (): number => {
        setBooleanState(prevState => ({
            ...prevState, finish: false
        }))
        stopTimer();
        let check = 0;
        const newInputValidity: inputValidity = {};
        const studentAnswersCopy = [...studentAnswerArr.lesson_answer.studentAnswers]
        if (worksheet) {
            const allInputsEmpty = inputsArr.every((input) => input.inputAnswer.trim() === '');
            if (allInputsEmpty) {
                setBooleanState(prevState => ({ ...prevState, modalOpen: allInputsEmpty }));
                return -1
            }

            for (let i = 0; i < worksheet.lesson.questions.length; i++) {
                const correctAnswers = worksheet.lesson.questions[i].answers.map((ans) => ans.answer);
                const isCorrect = correctAnswers.some((ans) => ans === inputsArr[i].inputAnswer.toLowerCase());
                newInputValidity[i] = isCorrect;
                studentAnswersCopy[i].isCorrect = isCorrect
                if (isCorrect) {
                    check += 10 / worksheet.lesson.questions.length
                }
            }
            setStudentAnswerArr(prevState => ({
                ...prevState, lesson_answer: {
                    ...prevState.lesson_answer,
                    studentAnswers: studentAnswersCopy
                }
            }))
            setInputValidity(newInputValidity);
            window.scrollTo({ top: 0, behavior: 'smooth' });
            setBooleanState(prevState => ({
                ...prevState,
                finishButtonVisible: false,
                inputsDisabled: true,
                modalOpen: false,
                checkGarde: true,
                refreshButtonVisible: true
            }));
        }
        return Math.ceil(check);
    }

    const sendResult = async () => {
        if (user && user.role.role === RoleEnum.STUDENT) {
            if (worksheet) {
                const checkedGrade = checkAnswers();
                if (checkedGrade === -1) {
                    return
                }
                const resultData = {
                    lesson_id: Number(worksheet.id),
                    grade: checkedGrade.toString(),
                    transit_time: getTime().toString(),
                    student_id: user.id,
                    teacher_id: worksheet.teacher_id,
                    lesson_answer: JSON.parse(JSON.stringify(studentAnswerArr.lesson_answer))
                }

                const data = await saveGrade(resultData);

                if (!(data as { error: object }).error) {
                    messageBy('Ваш результат был сохранен', MessageEnum.success);
                } else {
                    messageBy('Ошибка сохранения. Попробуйте еще раз.', MessageEnum.error)
                }

            }
        } else {
            checkAnswers()
        }
        resetTime();
    }

    const refreshForm = () => {
        stopTimer();
        setInputValidity({});
        window.scrollTo({ top: 0, behavior: 'smooth' });
        setBooleanState(prevState => ({
            ...prevState,
            inputsDisabled: false,
            finishButtonVisible: true,
            refreshButtonVisible: false,
            checkGarde: false
        }));
        const arrCopy = [...inputsArr];
        for (let i = 0; i < arrCopy.length; i++) {
            arrCopy[i].inputAnswer = '';
        }
        setInputsArr(arrCopy);
        startTimer();
    }

    return (
        <div className='space-container'>
            {worksheet &&
                <Space direction='vertical' style={{ maxWidth: 800, width: '100%', marginBottom: 40 }}>
                    <Title level={1}>{worksheet.title}</Title>
                    <Text style={{ fontSize: '19px', marginLeft: 'auto' }}>{worksheet.description}</Text>
                    <div style={{ display: 'flex', paddingBottom: 30, overflow: 'hidden', maxWidth: 800, width: '100%', borderRadius: '4px', border: '1px solid #ccc', background: '#fff', flexDirection: 'column', alignItems: 'flex-start' }}>
                        <Row style={{ maxWidth: 800, width: '100%' }} gutter={16}>
                            {worksheet && worksheet.lesson.questions.map((item, index) => {
                                return (
                                    <WrittenQuestionInput
                                        disabled={booleanState.inputsDisabled}
                                        inputValidity={inputValidity}
                                        inputAnswer={inputsArr.length ? inputsArr[index].inputAnswer : ''}
                                        key={index} question={item.question}
                                        index={index}
                                        inputChangeHandler={(e) => inputChangeHandler(e, index)}
                                        picturePath={worksheet.lesson.questions[index].picture !== null
                                            ?
                                            `${apiUrl}/uploads/tutorials/${worksheet.lesson.questions[index].picture}`
                                            :
                                            null} />
                                )
                            })}
                        </Row>
                    </div>
                    {booleanState.finishButtonVisible &&
                        <div style={{ display: 'flex', justifyContent: 'center', marginTop: 20 }}>
                            <Button size='large' onClick={() => setBooleanState(prevState => ({ ...prevState, finish: true }))} type='primary'>Проверить</Button>
                        </div>
                    }
                    {booleanState.refreshButtonVisible &&
                        <div style={{ display: 'flex', justifyContent: 'center', marginTop: 20 }}>
                            <Button size='large' onClick={refreshForm} type='primary'>Ответить еще раз</Button>
                        </div>
                    }
                    <FinishTestModal
                        open={booleanState.finish}
                        onCancel={() => setBooleanState(prevState => ({ ...prevState, finish: false }))}
                        closeModal={() => setBooleanState(prevState => ({ ...prevState, finish: false }))}
                        finishFunc={sendResult}
                    />
                    <EmptyAnswersModal
                        open={booleanState.modalOpen}
                        onOk={() => setBooleanState(prevState => ({ ...prevState, modalOpen: false }))}
                    />
                    <StartTestModal
                        open={!booleanState.start}
                        onOk={handleStart}
                        taskDescription='Впишите правильные ответы'
                    />
                    <Grade finish={booleanState.checkGarde} resultHandler={checkAnswers} />
                    {contextHolder}
                </Space>
            }
        </div>
    )
}

export default InteractiveWrittenAnswer;