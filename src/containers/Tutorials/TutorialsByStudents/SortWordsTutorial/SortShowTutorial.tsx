import { Button, Card, Col, Tag, Typography, message } from 'antd'
import Countdown from 'antd/es/statistic/Countdown'
import Paragraph from 'antd/es/typography/Paragraph'
import Title from 'antd/es/typography/Title'
import { useEffect, useState, DragEvent } from 'react'
import { useParams } from 'react-router-dom'
import { ISortObject, IWordSortTutorial } from '../../../../interfaces/tutorial/sortByWords/SortByWords'
import { useGetPublicLessonQuery } from '../../../../store/services/lessons'
import { dragbleStyle, dragBlur, dragStyle, gridStyle } from './StyleSortWords/Style.style.words'
import Grade from '../../../../components/Grade/Grade'
import { useAppSelector } from '../../../../hooks/reduxHooks'
import { useSaveGradeMutation } from '../../../../store/services/grade'
import StartTestModal from '../../../../components/UI/Modal/StartTestModal'
import { RoleEnum } from '../../../../enum/roleEnum/RoleEnum'
import MessageEnum from '../../../../enum/messageEnum/MessageEnum'



const SortShowTutorial = () => {

    const [messageApi, contextHolder] = message.useMessage();

    const messageBy = (message: string, type: MessageEnum) => {
        messageApi.open({
            type: type,
            content: message,
        });
    };

    const [sortObject, setSortObject] = useState<ISortObject>()

    const [copyArrWords, setCopy] = useState<IWordSortTutorial[]>([])

    const [currentWord, setCurrentWord] = useState<{ value: IWordSortTutorial, index: number }>()

    const [start, setStart] = useState<boolean>(false)

    const [finish, setFinish] = useState<boolean>(false)

    const { user } = useAppSelector(state => state.auth);

    const [saveGrade] = useSaveGradeMutation();

    const params = useParams()

    const { data: arrLessons } = useGetPublicLessonQuery()

    const dropHandler = (e: DragEvent<HTMLSpanElement>, val: IWordSortTutorial, index: number) => {

        e.preventDefault()

        if (sortObject && currentWord) {

            const include: boolean = sortObject.lesson.arrWords.includes(val)

            const includeWord: boolean = sortObject.lesson.arrWords.includes(currentWord.value)

            const copy: ISortObject = { ...sortObject }

            if (include && includeWord && val.isDrable) {

                const copyCurrent: IWordSortTutorial = sortObject.lesson.arrWords[currentWord.index]

                const copyByRemove: IWordSortTutorial = sortObject.lesson.arrWords[index]

                copy.lesson.arrWords.splice(index, 1, copyCurrent)

                copy.lesson.arrWords.splice(currentWord.index, 1, copyByRemove)

                return setSortObject({ ...sortObject, lesson: copy.lesson })

            }
            const includeChecked: boolean = sortObject.lesson.checkedWord.includes(val)

            if (val.isDrable && !includeChecked) {

                copy.lesson.arrWords[index] = {
                    ...copy.lesson.arrWords[index],
                    value: currentWord.value.value,
                    isDrable: true,
                    styleHandler: false
                }
                copy.lesson.checkedWord[currentWord.index] = {
                    ...copy.lesson.checkedWord[currentWord.index],
                    isDrable: false
                }

                return setSortObject({ ...sortObject, lesson: copy.lesson })

            }

        }

    }

    const changeTime = (e: number) => {

        const time: number = e / 1000

        sortObject && setSortObject({ ...sortObject, transit_time: time.toString() })

    }

    const resultHandler = (): number => {
        setFinish(false)

        let check: number = 0

        if (sortObject) {

            const copy: IWordSortTutorial[] = [...sortObject.lesson.arrWords.filter((val) => val.isDrable)]

            copy.map((val, index) => {

                if (val.value === copyArrWords[index].value && !val.styleHandler) {

                    check += 10 / sortObject.lesson.checkedWord.length

                }

            })

            if (user && user.role.role === RoleEnum.STUDENT) {
                const resultData = {
                    lesson_id: Number(sortObject.id),
                    grade: check.toString(),
                    transit_time: sortObject.transit_time,
                    student_id: user.id,
                    teacher_id: sortObject.teacher_id,
                    lesson_answer: JSON.parse(JSON.stringify(sortObject.lesson))
                }

                saveGrade(resultData);

                messageBy('Ваш результат был сохранен', MessageEnum.success);


            }
        }
        return Math.ceil(check)
    }


    useEffect(() => {

        if (arrLessons && params.id) {

            const index = arrLessons.findIndex((val) => {
                return val.id === Number(params.id)
            })
            if (index >= 0) {

                const copy: ISortObject =
                {
                    ...arrLessons[index],
                    lesson: JSON.parse(arrLessons[index].lesson)
                }

                copy.lesson.checkedWord.sort(() => Math.random() - 0.5)


                setSortObject(copy)

                setCopy(copy.lesson.arrWords.filter((val) => val.isDrable))

            }

        }

    }, [arrLessons])



    return (
        <>


            {
                sortObject &&


                (

                    <Card  >
                        <Title>
                            Поместите слова в нужном порядке
                        </Title>
                        <Typography>
                            <Title>{sortObject.title}</Title>
                            <Paragraph>
                                {sortObject.description}
                            </Paragraph>
                        </Typography>

                        <Typography style={{ margin: '4vh  0' }} >
                            {
                                sortObject.lesson.arrWords.map((val, index) => {
                                    return <span
                                        key={val.id}
                                        draggable={val.isDrable}
                                        onDragStart={() => setCurrentWord({ value: val, index: index })}
                                        onDragOver={(e) => e.preventDefault()}
                                        onDrop={(e) => dropHandler(e, val, index)}
                                        style={
                                            val.styleHandler
                                                ? dragBlur : (val.isDrable ? dragStyle : gridStyle)}
                                    >{val.value}</span>
                                })
                            }
                        </Typography>
                        {
                            sortObject.lesson.checkedWord.map((val, index) => {
                                return <Tag
                                    style={val.isDrable ? gridStyle : dragbleStyle}
                                    color="#5BC0EB"
                                    key={val.id}
                                    draggable={val.isDrable}
                                    onDragStart={() => setCurrentWord({ value: val, index: index })}
                                    onDragOver={(e) => e.preventDefault()}
                                    onDrop={(e) => dropHandler(e, val, index)}

                                >{val.value}</Tag>

                            })
                        }
                        <Col span={12} style={{ margin: '3vh  0' }}>
                            <Countdown
                                style={{ display: 'none' }}
                                title="Время для прохождения"
                                value={Date.now() + (start && !finish ? Number(sortObject.transit_time) : 0) * 1000}
                                onChange={(e) => changeTime(Number(e))}
                                onFinish={() => setFinish(true)}
                            />
                            <Button
                                type='primary'
                                size='large'
                                onClick={() => {
                                    setFinish(true)
                                }}
                            >
                                Завершить
                            </Button>
                            {contextHolder}
                        </Col>
                        <StartTestModal
                            open={!start}
                            onOk={() => setStart(!start)}
                            taskDescription='Переместите слова в нужном порядке'
                        />

                    </Card>

                )
            }

            {
                Grade({ resultHandler: resultHandler, finish: finish })
            }

        </>
    )
}

export default SortShowTutorial;