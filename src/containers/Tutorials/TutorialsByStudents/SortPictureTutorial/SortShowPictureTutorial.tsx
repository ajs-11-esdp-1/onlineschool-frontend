import { Button, Card, Col, Modal, Row, Space } from 'antd'
import { useEffect, useState, DragEvent } from 'react'
import { ItemPicture, SortByPicture } from '../../../../interfaces/tutorial/sortByPicture/SortByPicture'
import { useGetPublicLessonQuery } from '../../../../store/services/lessons'
import { useParams } from 'react-router-dom'
import { apiUrl } from '../../../../common/constants'
import { buttonStyle, pictureStyle, pictureStyleDrag } from './StyleSortPicture/SortShowPicture'
import Countdown from 'antd/es/statistic/Countdown'
import Grade from '../../../../components/Grade/Grade'
import Paragraph from 'antd/es/typography/Paragraph'
import { useAppSelector } from '../../../../hooks/reduxHooks'
import { useSaveGradeMutation } from '../../../../store/services/grade'
import StartTestModal from '../../../../components/UI/Modal/StartTestModal'



const SortShowPictureTutorial = () => {

    const [sortObject, setSortObject] = useState<SortByPicture | null>()

    const [copySort, setCopy] = useState<ItemPicture[]>([])

    const [currentPicture, setPicture] = useState<ItemPicture | null>()

    const [start, setStart] = useState<boolean>(false)

    const [finish, setFinish] = useState<boolean>(false)

    const { data: arrLessons } = useGetPublicLessonQuery()

    const { user } = useAppSelector(state => state.auth);

    const [saveGrade] = useSaveGradeMutation();

    const params = useParams()



    const dropImageHandler = (e: DragEvent<HTMLDivElement>, index: number) => {
        e.preventDefault();
        const copyArr: ItemPicture[] = [...copySort]
        if (currentPicture) {
            copyArr[index] = { ...copyArr[index], dragbel: false, picture: currentPicture.picture }

            setCopy([...copyArr])
        }
    }

    const changeTime = (e: number) => {

        const time: number = e / 1000

        sortObject && setSortObject({ ...sortObject, transit_time: time.toString() })

    }

    const resultHandler = (): number => {

        let check: number = 0

        if (sortObject) {


            sortObject.lesson.arrPicture.map((val, index) => {

                if (val.picture === copySort[index].picture && !copySort[index].dragbel) {

                    check += 10 / sortObject.lesson.arrPicture.length

                }

            })

            if (user) {

                const copy: SortByPicture = { ...sortObject }

                copy.lesson.arrPicture = copySort

                const resultData = {
                    lesson_id: Number(sortObject.id),
                    grade: check.toString(),
                    transit_time: sortObject.transit_time,
                    student_id: user.id,
                    teacher_id: sortObject.teacher_id,
                    lesson_answer: JSON.parse(JSON.stringify(copy.lesson))
                }

                saveGrade(resultData);
            }

        }



        return check
    }

    useEffect(() => {

        if (arrLessons && params.id) {

            const index = arrLessons.findIndex((val) => {
                return val.id === Number(params.id)
            })

            if (index >= 0) {

                const copy: SortByPicture =
                {
                    ...arrLessons[index],
                    lesson: JSON.parse(arrLessons[index].lesson)
                }

                copy.lesson.arrPicture.sort(() => Math.random() - 0.5)

                setSortObject({ ...copy })
                setCopy([...copy.lesson.arrPicture])
            }
        }
    }, [arrLessons])

    return (
        <div>
            <Space direction="vertical" size="middle"  >
                <Row gutter={[32, 32]}>
                    <Col>
                        {
                            sortObject &&
                            sortObject.lesson.arrPicture.map((val) => {
                                return <img
                                    key={val.id}
                                    style={pictureStyle}
                                    src={`${apiUrl}/uploads/tutorials/${val.picture}`}
                                    onDragStart={() => setPicture(val)}
                                    onDragOver={(e) => e.preventDefault()}
                                    onDrop={() => { }}
                                    draggable={true}

                                />
                            })
                        }
                    </Col>

                </Row>
                <Row gutter={[64, 64]} >
                    {
                        sortObject &&
                        sortObject.lesson.theme.map((obj) => {
                            return <Col span={12}
                                key={obj.id}
                            >
                                <Card
                                    title={obj.theme}
                                    hoverable={true}
                                >
                                    <Row
                                        gutter={[2, 32]}
                                        justify={'center'}
                                    >
                                        {
                                            copySort.map((val, i) => {

                                                return val.theme_id === obj.id &&

                                                    (
                                                        !val.dragbel ?

                                                            <img
                                                                key={val.id + Math.random()}
                                                                style={pictureStyle}
                                                                src={`${apiUrl}/uploads/tutorials/${val.picture}`}
                                                                onDragStart={() => setPicture(val)}
                                                                onDragOver={(e) => e.preventDefault()}
                                                                onDrop={(e) => dropImageHandler(e, i)}
                                                                draggable={!val.dragbel}
                                                            />
                                                            :
                                                            <Col
                                                                span={24}
                                                                style={pictureStyleDrag}
                                                                onDragStart={() => setPicture(val)}
                                                                onDragOver={(e) => e.preventDefault()}
                                                                onDrop={(e) => dropImageHandler(e, i)}
                                                                draggable={!val.dragbel}
                                                            />
                                                    )

                                            })
                                        }
                                    </Row>
                                </Card>
                            </Col>
                        })
                    }
                    <Countdown
                        title="Время для прохождения"
                        value={Date.now() + (start && !finish ? Number(sortObject?.transit_time) : 0) * 1000}
                        onChange={(e) => changeTime(Number(e))}
                        onFinish={() => setFinish(true)}
                    />
                </Row>
                <Button
                    onClick={() => {
                        setFinish(true)
                    }}
                >
                    Проверить
                </Button>
            </Space>
            {
                Grade({ resultHandler: resultHandler, finish: finish })
            }
            <Modal
                title="Задание"
                centered
                open={!start}
                width={600}
                footer={[<Button
                    style={buttonStyle}
                    onClick={() => setStart(!start)}
                >Старт</Button>]}
            >
                <Paragraph>
                    Переместите слова в нужном порядке
                </Paragraph>
            </Modal>
            <StartTestModal
                open={!start}
                onOk={() => setStart(!start)}
                taskDescription='Распределите картинки по категориям'
            />
        </div>
    )
}

export default SortShowPictureTutorial