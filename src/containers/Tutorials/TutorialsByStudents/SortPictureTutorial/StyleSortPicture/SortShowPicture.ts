import { CSSProperties } from "react";

export const pictureStyle : CSSProperties = {
    width: '14rem',
    height: '12rem',
    objectFit:'cover',
    marginLeft: '1.3vh',
}
export const pictureStyleDrag : CSSProperties = {
    height: '12rem',
    marginLeft: '1.3vh',
    background: 'rgba(0,0,0,0.1)',
    borderRadius:'4px',
    marginTop:'2vh',
    
}
export const buttonStyle : CSSProperties = {
    color:'white',
    background: '#33658A',
    
}
