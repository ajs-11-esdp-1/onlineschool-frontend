import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useGetPublicLessonQuery } from '../../../../store/services/lessons';
import { apiUrl } from '../../../../common/constants';
import Countdown from 'antd/es/statistic/Countdown';
import { Button } from 'antd';
import { useSaveGradeMutation } from '../../../../store/services/grade';
import {
    AnswerLesson,
    IMatchByWordPicture,
    PictureObject
} from '../../../../interfaces/tutorial/matchWordPicture/IMatchWordPicture';
import StartTestModal from '../../../../components/UI/Modal/StartTestModal';

const StudInterfaceWordPictureTutorial: React.FC = () => {
    const params = useParams();
    const { data: arrLessons } = useGetPublicLessonQuery();

    const [start, setStart] = useState<boolean>(false);
    const [finish, setFinish] = useState<boolean>(false);

    const [matchObject, setMatch] = useState<IMatchByWordPicture>();

    const [data, setData] = useState<AnswerLesson>({
        arrWords: [],
        arrPictures: [],
    });

    const [picturesInFields, setPicturesInFields] = useState<(PictureObject | null)[]>([]);
    const [gameFinished, setGameFinished] = useState<boolean>(false);

    const [answer, setAnswer] = useState<AnswerLesson>({
        arrWords: [],
        arrPictures: [],
    });

    const [postGrade] = useSaveGradeMutation();

    const shuffleArray = (array: PictureObject[]) => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    };

    const changeTime = (e: number) => {
        const time: number = e / 1000;
        matchObject && setMatch({ ...matchObject, transit_time: time.toString() });
    };

    const handleDrop = (e: React.DragEvent<HTMLDivElement>, index: number) => {
        e.preventDefault();
        const pictureId = e.dataTransfer.getData('pictureId');
        const picture = data.arrPictures.find((picture) => picture.id === pictureId);
        if (picture) {
            const newPicturesInFields: (PictureObject | null)[] = [...picturesInFields];
            newPicturesInFields[index] = picture;
            setPicturesInFields(newPicturesInFields);
            setData((prevData) => ({
                arrWords: prevData.arrWords,
                arrPictures: prevData.arrPictures.filter((pic) => pic.id !== pictureId),
            }));
            setAnswer((prevAnswer) => ({
                arrWords: data.arrWords,
                arrPictures: [...prevAnswer.arrPictures, picture],
            }));
        }
    };

    const handleRemovePicture = (index: number) => {
        const removedPicture = picturesInFields[index];
        if (removedPicture) {
            const newPicturesInFields: (PictureObject | null)[] = [...picturesInFields];
            newPicturesInFields[index] = null;
            setPicturesInFields(newPicturesInFields);

            setData((prevData) => ({
                arrWords: [...prevData.arrWords],
                arrPictures: [...prevData.arrPictures, removedPicture],
            }));

            setAnswer((prevAnswer) => ({
                arrWords: prevAnswer.arrWords,
                arrPictures: prevAnswer.arrPictures.filter((pic) => pic.id !== removedPicture.id),
            }));
        }

        const areAllPicturesRemoved = picturesInFields.every((picture) => picture === null);

        if (areAllPicturesRemoved) {
            setPicturesInFields(Array(data.arrWords.length).fill(null));
        }
    };

    const handleDragOver = (e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
    };

    const handleDragStart = (e: React.DragEvent<HTMLImageElement>, pictureId: string) => {
        e.dataTransfer.setData('pictureId', pictureId);
    };

    const handleSendResults = () => {
        if (matchObject && params.id) {
            postGrade({
                lesson_id: Number(params.id),
                lesson_answer: JSON.parse(JSON.stringify(answer)),
                teacher_id: matchObject.teacher_id,
                student_id: 3,
                transit_time: '300',
                grade: '5',
            });
        }
        setFinish(true);
        console.log('Результаты отправлены!', answer);
        setGameFinished(true);
    };

    useEffect(() => {
        if (arrLessons && params.id) {
            const index = arrLessons.findIndex((val) => val.id === Number(params.id));
            if (index >= 0) {
                const copy: IMatchByWordPicture = {
                    ...arrLessons[index],
                    lesson: JSON.parse(arrLessons[index].lesson),
                };
                setMatch(copy);
                setData({
                    arrWords: copy.lesson.arrWords,
                    arrPictures: shuffleArray(
                        copy.lesson.arrPictures.map((val) => {
                            return { ...val, picture: val.picture as string };
                        })
                    ),
                });
                setPicturesInFields(Array(copy.lesson.arrWords.length).fill(null));
            }
        }
    }, [arrLessons, params.id]);

    return (
        <div style={{ display: "flex", flexDirection: "column", marginTop: 50, marginBottom: 100, justifyContent: "space-evenly" }}>
            <h1>Игра "Сопоставление слова и картинки"</h1>
            <Countdown
                title="Время для прохождения"
                value={Date.now() + (start && !finish ? Number(matchObject?.transit_time) : 0) * 1000}
                onChange={(e) => changeTime(Number(e))}
                onFinish={() => setFinish(true)}
            />
            <div
                className="picture-place"
                style={{
                    border: "1px solid lightgray",
                    minHeight: "300px",
                    borderRadius: "5px",
                    display: "flex",
                    flexWrap: "wrap",
                    alignItems: "center",
                    justifyContent: "space-evenly"
                }}
            >
                {data.arrPictures.map((picture) => (
                    <img
                        style={{ display: 'inline-flex', width: 200, height: 200, borderRadius: "5px" }}
                        key={picture.id}
                        src={`${apiUrl}/uploads/tutorials/${picture.picture}`}
                        alt={picture.word_id}
                        draggable
                        onDragStart={(e) => handleDragStart(e, picture.id)}
                    />
                ))}
            </div>
            <div
                className="words-picture-field"
                style={{
                    display: "flex",
                    justifyContent: "space-evenly"
                }}
            >
                {data.arrWords.map((word, index) => (
                    <div
                        key={`picture-field_${index}`}
                        onDrop={(e) => handleDrop(e, index)}
                        onDragOver={handleDragOver}
                        style={{
                            width: 250,
                            height: 300,
                            border: '1px solid lightgray',
                            display: 'inline-flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            margin: 10,
                            position: 'relative',
                        }}
                    >
                        <div
                            className="picture-filed-wrapper"
                            style={{
                                display: 'flex',
                                flexDirection: 'column'
                            }}
                        >
                            <div>{word.word}</div>
                            {picturesInFields[index] ? (
                                <div>
                                    <img
                                        style={{ display: 'flex', width: 200, height: 200 }}
                                        src={`${apiUrl}/uploads/tutorials/${picturesInFields[index]?.picture}`}
                                        alt={picturesInFields[index]?.word_id}
                                    />
                                    <Button onClick={() => handleRemovePicture(index)}
                                        style={{ position: 'absolute', top: 0, right: 0 }}>
                                        Удалить
                                    </Button>
                                </div>
                            ) : (
                                <div>Пусто</div>
                            )}
                        </div>
                    </div>
                ))}
            </div>
            {!gameFinished && picturesInFields.length > 0 && (
                <div>
                    <Button style={{ width: 200 }} onClick={handleSendResults}>Проверить</Button>
                </div>
            )}
            <StartTestModal
                open={!start}
                onOk={() => setStart(!start)}
                taskDescription='Переместите картинки к словам'
            />
        </div>
    );
};

export default StudInterfaceWordPictureTutorial;
