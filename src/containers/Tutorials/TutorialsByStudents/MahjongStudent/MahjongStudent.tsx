import { useEffect, useState } from 'react';
import { useGetPublicLessonQuery } from "../../../../store/services/lessons";
import { IMahjong, ITile } from "../../../../interfaces/tutorial/mahjongInterface/IMahjong";
import { useParams } from "react-router-dom";
import { Button, Card } from "antd";
import gsap from 'gsap';
import { style } from './StyleMahjongStudent/StyleMahjongStudent';
import Grade from "../../../../components/Grade/Grade";
import Countdown from "antd/es/statistic/Countdown";
import { generateID } from "../../../../helpers/GenerateId/GenerateId";
import { RootState } from "../../../../store/store";
import { useAppSelector } from "../../../../hooks/reduxHooks";
import { useSaveGradeMutation } from "../../../../store/services/grade";
import { apiUrl } from "../../../../common/constants";
import StartTestModal from '../../../../components/UI/Modal/StartTestModal';


const MahjongStudent = () => {
    const params = useParams();
    const { data: arrLessons } = useGetPublicLessonQuery();
    const [currentTile, setCurrentTile] = useState<ITile | null>(null);
    const [mahjongObject, setMahjongObject] = useState<IMahjong>();
    const [rotate, setRotate] = useState<boolean>(false);
    const [start, setStart] = useState<boolean>(false);
    const [finish, setFinish] = useState<boolean>(false);
    const { user } = useAppSelector<RootState>(state => state.auth);
    const [postAnswer] = useSaveGradeMutation();


    const rotateHandler = async (tile: ITile) => {
        setRotate(true);
        if (!currentTile) {
            await gsap.to(`.${tile.id}`, {
                rotateY: 0,
                duration: 0.5,
            });
            setCurrentTile(tile);
            tile.isVisible = true;
        } else if (currentTile) {
            await gsap.to(`.${tile.id}`, {
                rotateY: 0,
                duration: 0.5,
            });
            tile.isVisible = true;
            if (currentTile.value === tile.value && mahjongObject) {
                setCurrentTile(null);
                const copy: ITile[] = [...mahjongObject.lesson.images];

                const currentIndex: number = mahjongObject.lesson.images.findIndex((val) => val.id === currentTile.id);
                const tileIndex: number = mahjongObject.lesson.images.findIndex((val) => val.id === tile.id);

                if (currentIndex >= 0 && tileIndex >= 0) {
                    copy[currentIndex].isMatched = true;
                    copy[tileIndex].isMatched = true;
                    setMahjongObject({ ...mahjongObject, lesson: { images: [...copy] } });
                }

                if (copy.every((val) => val.isMatched)) {
                    setFinish(true);
                }

            } else {
                await setTimeout(() => {
                    gsap.to(`.${currentTile.id}`, {
                        rotateY: 180,
                        duration: 0.5,
                    });
                    currentTile.isVisible = false;
                }, 500);

                await setTimeout(() => {
                    gsap.to(`.${tile.id}`, {
                        rotateY: 180,
                        duration: 0.5,
                    })
                    tile.isVisible = false;
                }, 500);
                setCurrentTile(null);
            }
        }
        await setRotate(false);
    };

    const changeTime = (e: number) => {
        const time: number = e / 1000;
        mahjongObject && setMahjongObject({ ...mahjongObject, transit_time: time.toString() });
    };

    const resultHandler = (): number => {
        if (mahjongObject) {
            const matchedImagesCount: number = mahjongObject.lesson.images.filter((val) => val.isMatched).length / 2;
            const totalImagesCount: number = mahjongObject.lesson.images.length / 2;

            const check: number = Math.floor((matchedImagesCount / totalImagesCount) * 10);

            if (user.role.role === 'student') {
                postAnswer({
                    grade: check.toString(),
                    lesson_id: Number(mahjongObject.id),
                    transit_time: mahjongObject.transit_time,
                    student_id: user.id,
                    teacher_id: mahjongObject.teacher_id,
                    lesson_answer: JSON.parse(JSON.stringify(mahjongObject.lesson))
                })
            }
            return check;
        }
        return 0;
    };

    useEffect(() => {
        if (arrLessons && params.id) {
            const index = arrLessons.findIndex((val) => {
                return val.id === Number(params.id);
            });
            if (index >= 0) {
                const copy: IMahjong = { ...arrLessons[index], lesson: JSON.parse(arrLessons[index].lesson) };
                const copyArray: ITile[] = [...copy.lesson.images, ...copy.lesson.images.map((val) => {
                    return { ...val, id: generateID(), isVisible: false };
                })]
                    .sort(() => Math.random() - 0.5);

                setMahjongObject({ ...copy, lesson: { images: copyArray } });
            }
        }
    }, [arrLessons]);


    return (
        <div>
            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', minHeight: '100vh' }}>
                <h1>Маджонг</h1>
                <div className="tiles-container" style={{ width: '1001px' }}>
                    <Card>
                        {mahjongObject?.lesson.images.map((tile) => (
                            <Card.Grid
                                key={tile.id}
                                style={{
                                    ...style,
                                    transform: `rotateY(${tile.isVisible ? '0' : '180'}deg)`,
                                }}
                                onClick={() => !rotate && !finish && !tile.isMatched && rotateHandler(tile)}
                                className={tile.id}
                            >
                                {tile.isVisible && (
                                    <img
                                        src={`${apiUrl}/uploads/tutorials/${tile.value}`}
                                        alt={`Image ${tile.id}`}
                                        style={{ maxWidth: '100%', maxHeight: '100%' }}
                                    />
                                )}
                            </Card.Grid>
                        ))}
                    </Card>
                    <Countdown
                        title="Время для прохождения"
                        value={Date.now() + (start && !finish ? Number(mahjongObject?.transit_time) : 0) * 1000}
                        onChange={(e) => changeTime(Number(e))}
                        onFinish={() => setFinish(true)}
                    />
                    <Button
                        onClick={() => {
                            setFinish(true);
                        }}
                    >
                        Завершить
                    </Button>
                </div>
            </div>
            <StartTestModal
                open={!start}
                onOk={() => setStart(!start)}
                taskDescription='Найдите одинаковые картинки'
            />
            {
                Grade({ resultHandler: resultHandler, finish: finish })
            }
        </div>
    );
};

export default MahjongStudent;