import {CSSProperties} from "react";

export const style: CSSProperties = {
    background: 'cadetblue',
    width: '250px',
    height: '130px',
    transform: 'rotateY(180deg)',
    display: 'flex',
    alignItems:'center',
    justifyContent:'center',
}
