import { Avatar, Button, Card, Col, Row, Select, Typography } from 'antd'
import { useGetTemplateQuery } from '../../store/services/template';
import { useEffect, useState } from 'react';
import { useGetPublicLessonQuery } from '../../store/services/lessons';
import { ILessonByRTK } from '../../interfaces/tutorial/LessonByRTK';
import { HeartTwoTone, PlayCircleTwoTone } from '@ant-design/icons';
import Meta from 'antd/es/card/Meta';
import { apiUrl } from '../../common/constants';
import { TutorialTypeEnum } from '../../enum/tutorialType/TutorialTypeEnum';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '../../hooks/reduxHooks';
import { RootState } from '../../store/store';
import { RoleEnum } from '../../enum/roleEnum/RoleEnum';
import './Tutorials.css'



const Tutorials = () => {

    const { data: templateArr } = useGetTemplateQuery();

    const { data: lesson } = useGetPublicLessonQuery();

    const [arrLessons, setLessons] = useState<ILessonByRTK[]>([]);

    const { user } = useAppSelector<RootState>(state => state.auth);

    const navigate = useNavigate();

    const changeTutorial = (e: string) => {
        if (lesson) {
            if (e !== 'all') {
                const copyArrLessons: ILessonByRTK[] = lesson.filter((val) => val.template.title === e)

                setLessons(copyArrLessons)
            } else {
                setLessons(lesson)
            }
        }
    };

    const navigateTutorialHandler = (e: string, id: number) => {

        if (e === TutorialTypeEnum.wordsSort) {
            navigate(`/sort/words/${id}`);
        } else if (e === TutorialTypeEnum.pictureSort) {
            navigate(`/sort/picture/${id}`);
        } else if (e === TutorialTypeEnum.writtenAnswer) {
            navigate(`/written/answer/${id}`);
        } else if (e === TutorialTypeEnum.mahjong) {
            navigate(`/mahjong/${id}`);
        } else if (e === TutorialTypeEnum.matchWordPicture) {
            navigate(`/match/word-picture/${id}`);
        } else if (e === TutorialTypeEnum.quizTutorial) {
            navigate(`/quiz/${id}`)
        }
    };

    useEffect(() => {
        if (lesson) {
            setLessons(lesson)
        }

    }, [user, lesson])


    return (
        <div className='tutorials-container'>
            <h2 style={{ paddingLeft: 12 }}>Интерактивные пособия</h2>
            <div className='categories-box'>
                <div style={{ width: 300, marginRight: 10 }}>
                    <Select
                        style={{ width: '100%' }}
                        options={templateArr ?
                            [{ value: 'all', label: 'Все' }, ...templateArr.map((val) => {
                                return { value: val.title, label: val.title }
                            })]
                            :
                            [{
                                value: 'all', label: 'Все'
                            }]}
                        onChange={(e) => changeTutorial(e)}
                        defaultValue='Категория'
                    />
                </div>
                {user && user.role.role !== RoleEnum.STUDENT && (
                    <Button
                        style={{ marginTop: 12 }}
                        type='primary'
                        onClick={() => navigate('/new/lesson')}
                    >
                        Создать пособие
                    </Button>
                )}
                {!user &&
                    <div>
                        <Button
                            style={{ marginTop: 12 }} type='primary'
                            onClick={() => navigate('/register')}>
                            Создать пособие
                        </Button>
                    </div>
                }
            </div>
            <Row gutter={[24, 24]} style={{ marginTop: '4vh' }} >
                {
                    arrLessons.map((val) => {
                        return <Col
                            style={{ display: 'flex' }}
                            key={val.id}
                            xs={24} sm={12} md={12} lg={8} xl={8}
                        >
                            <Card
                                className='flexible-card'
                                style={{ display: 'flex', flexDirection: 'column', width: '100%' }}
                                hoverable
                                bordered={false}
                                title={val.title}
                                actions={[
                                    <HeartTwoTone
                                        twoToneColor="#eb2f96"
                                        style={{ fontSize: '1.3rem', height: '100%' }}
                                    />,
                                    <PlayCircleTwoTone
                                        onClick={() => {
                                            navigateTutorialHandler(val.template.title, Number(val.id))
                                        }}
                                        style={{ fontSize: '1.3rem' }} />
                                ]}
                            >

                                <Typography.Paragraph>
                                    Описание: {val.description}
                                </Typography.Paragraph>

                                <Card
                                    type='inner'
                                >
                                    <Meta
                                        avatar={<Avatar src={`${apiUrl}/uploads/user/${val.user.avatar}`} />}
                                        title={`Автор ${val.user.lastName} ${val.user.firstName}`}
                                    />
                                </Card>
                            </Card>
                        </Col>
                    })
                }
            </Row>

        </div>
    )
};

export default Tutorials;
