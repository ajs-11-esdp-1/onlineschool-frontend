import React, { CSSProperties, useState } from 'react';
import { Button, Input, message } from 'antd';
import { AppstoreAddOutlined, DeleteOutlined, SendOutlined } from '@ant-design/icons';
import { nanoid } from 'nanoid';
import { RootState } from "../../../../store/store";
import { useAppSelector, useAppDispatch } from "../../../../hooks/reduxHooks";
import { usePostLessonMutation, usePostPictureLessonMutation } from "../../../../store/services/lessons";
import { IMatchByWordPicture } from "../../../../interfaces/tutorial/matchWordPicture/IMatchWordPicture";
import { useNavigate } from "react-router-dom";
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK';
import MessageEnum from '../../../../enum/messageEnum/MessageEnum';
import { initLesson } from '../../../../store/features/lesson';

const buttonStyle: CSSProperties = {
    display: 'inline-block',
    borderRadius: '32px',
    paddingInlineStart: '16px',
    paddingInlineEnd: '16px',
    color: '#fff',
    backgroundColor: '#1677ff',
    boxShadow: '0 2px 0 rgba(5, 145, 255, 0.1)',
    fontSize: '14px',
    padding: '8px 15px',
    textAlign: 'center',
    border: '1px solid transparent',
    cursor: 'pointer',
    transition: 'all 0.2s cubic-bezier(0.645, 0.045, 0.355, 1)'
}


const MatchByWordPicture: React.FC = () => {
    const dispatch = useAppDispatch();
    const [messageApi, contextHolder] = message.useMessage();
    const messageBy = (message: string, type: MessageEnum) => {
        messageApi.open({
            type: type,
            content: message,
        });
    };
    const newWordId = nanoid();
    const newPictureId = nanoid();

    const [postLesson] = usePostLessonMutation();
    const [postPicture] = usePostPictureLessonMutation();

    const lesson = useAppSelector<RootState>(state => state.lesson);

    const navigate = useNavigate();

    const [matchObject, setMatchObject] = useState<Pick<IMatchByWordPicture, 'lesson'>>({
        lesson: {
            arrWords: [
                {
                    id: newWordId,
                    word: '',
                },
            ],
            arrPictures: [
                {
                    id: newPictureId,
                    word_id: newWordId,
                    picture: '',
                },
            ]
        },
    });

    const [errorMessage, setErrorMessage] = useState<string>('');

    const handleAddInput = () => {
        if (matchObject.lesson.arrWords.length < 10) {
            const newTId = nanoid();
            const newPId = nanoid();
            setMatchObject((prev) => ({
                lesson: {
                    arrWords: [
                        ...prev.lesson.arrWords,
                        {
                            id: newTId,
                            word: '',
                        },
                    ],
                    arrPictures: [
                        ...prev.lesson.arrPictures,
                        {
                            id: newPId,
                            word_id: newTId,
                            picture: '',
                        },
                    ],
                }
            }))
        }
    };

    const handleRemoveInput = (index: number) => {
        setMatchObject((prev) => ({
            lesson: {
                arrWords: prev.lesson.arrWords.filter((_, i) => i !== index),
                arrPictures: prev.lesson.arrPictures.filter((_, i) => i !== index)
            }
        }));
    };

    const handleWordChange = (event: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const newWord = event.target.value;
        setMatchObject((prev) => {
            const updatedInputs = { ...prev };
            updatedInputs.lesson.arrWords[index].word = newWord;
            return updatedInputs;
        });
    };

    const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>, index: number, id: string) => {
        const selectedFile = event.target.files?.[0];
        if (selectedFile) {

            const copy: Pick<IMatchByWordPicture, 'lesson'> = { ...matchObject }

            copy.lesson.arrPictures[index] = { id: nanoid(), word_id: id, picture: selectedFile }
            return setMatchObject({ ...matchObject, lesson: copy.lesson })
        }
    };

    const handleSendData = async () => {
        const formData: FormData = new FormData();
        const copyMatchObject: Pick<IMatchByWordPicture, 'lesson'> = { ...matchObject }

        try {
            setErrorMessage('');
            copyMatchObject.lesson.arrPictures.map((val) => {
                if (typeof val.picture !== 'string') {
                    const file_name = nanoid() + '.' + val.picture.name.split('.').slice(-1)[0];
                    const new_file = new File([val.picture],
                        file_name,
                        { type: val.picture.type });

                    const dataTransfer = new DataTransfer()
                    dataTransfer.items.add(new_file);
                    val.picture = dataTransfer.files[0].name
                    formData.append(`picture`, dataTransfer.files[0])

                    return val
                }
                return
            })
            const copy: string[] = [
                lesson.title,
                lesson.description
            ];

            const validated: boolean = copy.map((val: string) => {

                return val.trim() !== '' ? false : true

            }).includes(true)

            if (validated) {
                messageBy('Необходимо заполнить все поля и выбрать категорию', MessageEnum.error);
                return
            }


            const tutorial = await postLesson({ ...lesson, lesson: JSON.stringify({ ...copyMatchObject.lesson }) })


            if (tutorial as { data: ILessonByRTK }) {

                const lessonId: { data: ILessonByRTK } = tutorial as { data: ILessonByRTK }

                postPicture({
                    picture: formData,
                    lesson_id: lessonId.data.id as number
                })
                navigate('/teacher/personal/area/tutorial');
                dispatch(initLesson());
            }

        } catch (err) {
            
            setErrorMessage('Ошибка отправки данных ')
        }
    };

    return (
        <div className="MatchByWordPicture">
            {matchObject.lesson.arrWords.map((word, index) => (
                <div key={index} className="MatchByWordPicture-inputs"
                    style={{ display: 'flex', flexDirection: 'column' }}>
                    <h3>Задание {index + 1}</h3>
                    <div style={{ marginBottom: 20 }}>
                        <label htmlFor={`wordInput-${index}`} style={{ marginRight: 10 }}>
                            Слово:{' '}
                        </label>
                        <Input
                            id={`wordInput-${index}`}
                            type="text"
                            value={word.word}
                            onChange={(e) => handleWordChange(e, index)}
                            style={{ width: '20em' }}
                        />
                        <label htmlFor={`pictureInput-${index}`} style={{ margin: '10px' }}>
                            Картинка:{' '}
                        </label>
                        <label style={buttonStyle}>
                            Добавить картинку
                            <Input
                                type="file"
                                accept="image/*"
                                onChange={(e) => handlePictureChange(e, index, word.id)}
                                style={{
                                    display: 'none'
                                }}
                            />

                        </label>
                        <span>{matchObject.lesson.arrPictures[index].picture ?
                            (matchObject.lesson.arrPictures[index].picture as File).name :
                            ''}</span>
                        <Input
                            type="file"
                            accept="image/*"
                            onChange={(e) => handlePictureChange(e, index, word.id)}
                            style={{
                                display: 'none'
                            }}
                        />
                        <Button style={{ margin: '0 0 0 20px' }} onClick={() => handleRemoveInput(index)}>
                            Удалить <DeleteOutlined />
                        </Button>
                    </div>
                </div>
            ))}
            {errorMessage && <p>{errorMessage}</p>}
            <div className="MatchByWordPicture-buttons" style={{ margin: '20px 0 40px 10px' }}>
                {matchObject.lesson.arrWords.length < 10 && (
                    <Button style={{ margin: ' 0 20px 0 0' }} onClick={handleAddInput}>
                        Добавить слово и картинку
                        <AppstoreAddOutlined />
                    </Button>
                )}
                {matchObject.lesson.arrWords.length > 0 && (
                    <Button onClick={handleSendData}>
                        Отправить
                        <SendOutlined />
                    </Button>
                )}
            </div>
            {contextHolder}
        </div>
    );
};
export default MatchByWordPicture;