import { useAppSelector } from '../../../../hooks/reduxHooks';
import './StyleQuizTeacher/QuizTeacher.css';
import { RootState } from '../../../../store/store'
import React, { useState } from 'react';
import { Option, Question } from '../../../../interfaces/tutorial/quizTutorial/quizTutorial'
import { usePostLessonMutation } from '../../../../store/services/lessons';
import { useNavigate } from 'react-router-dom';
import MessageEnum from '../../../../enum/messageEnum/MessageEnum';
import { message } from 'antd';
import { useAppDispatch } from '../../../../hooks/reduxHooks';
import { initLesson } from '../../../../store/features/lesson';




const QuizCreator: React.FC = () => {
  const dispatch = useAppDispatch();
  const [messageApi, contextHolder] = message.useMessage();
  const [validationError, setValidationError] = useState<string>('');
  const [question, setQuestion] = useState<string>('');
  const [options, setOptions] = useState<Option[]>([{ option: '' }, { option: '' }, { option: '' }, { option: '' }]);
  const [correctAnswer, setCorrectAnswer] = useState<string>('');
  const [questionsArray, setQuestionsArray] = useState<Question[]>([]);
  const lesson = useAppSelector<RootState>(state => state.lesson);
  const [postLesson] = usePostLessonMutation();
  const navigate = useNavigate();


  const messageBy = (message: string, type: MessageEnum) => {
    messageApi.open({
      type: type,
      content: message,
    });
  };

  const handleQuestionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuestion(e.target.value);
  };

  const handleOptionChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const updatedOptions = [...options];
    updatedOptions[index] = { option: e.target.value };
    setOptions(updatedOptions);
  };

  const handleCorrectAnswerChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value !== '') {
      setCorrectAnswer(e.target.value);
    }
  };

  const handleAddQuestion = () => {
    if (!question || options.some(option => !option.option) || !correctAnswer) {
      setValidationError('Заполните все поля');
      return;
    }
    const newQuestion: Question = { question, options, correctAnswer };
    setQuestionsArray((prevQuestions) => [...prevQuestions, newQuestion]);
    setQuestion('');
    setOptions([{ option: '' }, { option: '' }, { option: '' }, { option: '' }]);
    setCorrectAnswer('');
    setValidationError('');
  };

  const handleReady = async () => {
    if (questionsArray.length === 0) {
      setValidationError('Добавьте хотя бы один вопрос');
      return;
    }

    if (questionsArray.some(questionObj => !questionObj.question || !questionObj.correctAnswer)) {
      setValidationError('Заполните все поля для всех вопросов');
      return;
    }
    const copy: string[] = [
      lesson.title,
      lesson.description
    ];

    const validated: boolean = copy.map((val: string) => {

      return val.trim() !== '' ? false : true

    }).includes(true)

    if (validated) {
      messageBy('Необходимо заполнить все поля и выбрать категорию', MessageEnum.error);
      return
    }

    const data = await postLesson({ ...lesson, lesson: JSON.stringify({ quiz: questionsArray }) });
    if (!(data as { error: object }).error) {
      dispatch(initLesson())
      navigate('/teacher/personal/area/tutorial');
    }
    
  };

  return (
    <div className="quiz-creator-container">
      <div>
        <div className="quiz-creator-inputs">
          {validationError && <p className="validation-error">{validationError}</p>}
          <label className="quiz-creator-label">Вопрос:</label>
          <input
            type="text"
            className="quiz-creator-input"
            value={question}
            onChange={handleQuestionChange}
          />
        </div>
        <div className="quiz-creator-inputs">
          <label className="quiz-creator-label">Варианты:</label>
          {options.map((option, index) => (
            <input
              key={index}
              type="text"
              className="quiz-creator-input"
              value={option.option}
              onChange={(e) => handleOptionChange(e, index)}
            />
          ))}
        </div>
        <div className="quiz-creator-inputs">
          <label className="quiz-creator-label">Правильный ответ:</label>
          <select
            className="quiz-creator-select"
            value={correctAnswer}
            onChange={handleCorrectAnswerChange}
          >
            <option value="">Выберите правильный ответ</option>
            {options.map((option, index) => (
              <option key={index} value={option.option}>
                {option.option}
              </option>
            ))}
          </select>
        </div>
        <button className="quiz-creator-button" onClick={handleAddQuestion}>
          Добавить вопрос
        </button>
        <button className="quiz-creator-button" onClick={handleReady}>
          Готово
        </button>
      </div>
      <div className="question-list">
        <h2>Список вопросов:</h2>
        <ul>
          {questionsArray.map((questionObj, index) => (
            <li key={index}>
              <h3>{questionObj.question}</h3>
              <div className="options">
                {questionObj.options.map((option, optionIndex) => (
                  <div key={optionIndex}>{option.option}</div>
                ))}
              </div>
              <div className="correct-answer">
                Correct Answer: {questionObj.correctAnswer}
              </div>
            </li>
          ))}
        </ul>
      </div>
      {contextHolder}
    </div>
  );
};

export default QuizCreator;
