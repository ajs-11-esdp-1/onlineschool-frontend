import { Button, Card, Col, message, Row } from 'antd'
import { nanoid } from 'nanoid'
import { ChangeEvent, useState } from 'react'
import { ItemPicture, ITheme, SortByPicture } from '../../../../interfaces/tutorial/sortByPicture/SortByPicture'
import SortByPictureComponent from '../../../../components/SortByPictyre/SortByPictureComponent'
import { CloseOutlined, DeleteOutlined } from '@ant-design/icons'
import MessageEnum from '../../../../enum/messageEnum/MessageEnum'
import { deleteButtonPicture, iconStyle, itemBlock, pictureItemBlock, pictureStyle } from './StyleSortPicture/StyleSortPicture'
import { usePostLessonMutation, usePostPictureLessonMutation } from '../../../../store/services/lessons'
import { RootState } from '../../../../store/store'
import { useAppSelector } from '../../../../hooks/reduxHooks'
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch } from '../../../../hooks/reduxHooks'
import { initLesson } from '../../../../store/features/lesson'

export type LessonPictureSort = Pick<SortByPicture, 'lesson'>

const SortPicture = () => {

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [messageApi, contextHolder] = message.useMessage();

    const [postLesson] = usePostLessonMutation()

    const [postPicture] = usePostPictureLessonMutation()

    const lesson = useAppSelector<RootState>(state => state.lesson)

    const [sortObject, setSortObject] = useState<LessonPictureSort>({

        lesson: {
            theme: [],
            arrPicture: []
        },

    })

    const messageBy = (message: string, type: MessageEnum) => {
        messageApi.open({
            type: type,
            content: message,
        });
    };


    const changeThemeHandler = (e: ChangeEvent<HTMLInputElement>, index: number) => {

        const { name, value } = e.target

        const copyArr: ITheme[] = [...sortObject.lesson.theme]

        copyArr[index] = { ...copyArr[index], [name]: value }

        setSortObject({ ...sortObject, lesson: { ...sortObject.lesson, theme: copyArr } })

    }

    const changePicture = (e: ChangeEvent<HTMLInputElement>, id: string) => {

        if (e.target.files) {

            const file: File = e.target.files[0]

            if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/svg') {


                return setSortObject({
                    ...sortObject, lesson: {
                        ...sortObject.lesson, arrPicture:
                            [...sortObject.lesson.arrPicture,
                            { id: nanoid(), theme_id: id, picture: file, dragbel: true }]
                    }
                })

            }

            messageBy('Данное фото не корректного формата', MessageEnum.error)
        }
    }

    const addAnswer = () => {
        setSortObject({
            ...sortObject, lesson: {
                ...sortObject.lesson, theme:
                    [...sortObject.lesson.theme, { id: nanoid(), theme: '' }]
            }
        })
    }

    const deletePicture = (index: number) => {

        const copyArr: ItemPicture[] = [...sortObject.lesson.arrPicture]

        copyArr.splice(index, 1)

        setSortObject({ ...sortObject, lesson: { ...sortObject.lesson, arrPicture: copyArr } })

    }

    const deleteBlock = (index: number) => {

        const copy: ITheme[] = [...sortObject.lesson.theme]

        const copyArrPicture: ItemPicture[] = [...sortObject.lesson.arrPicture.filter((val) => {
            return val.theme_id !== copy[index].id
        })]

        copy.splice(index, 1)

        setSortObject({
            ...sortObject, lesson:
                { ...sortObject.lesson, arrPicture: copyArrPicture, theme: copy }
        })

    }

    const saveTutorialHandler = async () => {

        const formData: FormData = new FormData();

        const copySortObject: LessonPictureSort = { ...sortObject }

        try {

            copySortObject.lesson.arrPicture.map((val) => {

                if (typeof val.picture !== 'string') {

                    const file_name = nanoid() + '.' + val.picture.name.split('.').slice(-1)[0];

                    const new_file = new File([val.picture],
                        file_name,
                        { type: val.picture.type });

                    const dataTransfer = new DataTransfer()

                    dataTransfer.items.add(new_file);

                    val.picture = dataTransfer.files[0].name

                    formData.append('picture', dataTransfer.files[0])

                    return val

                }

            })

            const copy: string[] = [
                lesson.title,
                lesson.description
            ];

            const validated: boolean = copy.map((val: string) => {

                return val.trim() !== '' ? false : true

            }).includes(true)

            if (validated) {
                messageBy('Необходимо заполнить все поля и выбрать категорию', MessageEnum.error);
                return
            }
            const tutorial = await postLesson({ ...lesson, lesson: JSON.stringify({ ...sortObject.lesson }) })

            // const lessonId: {data: ILessonByRTK } | {error : FetchBaseQueryError | SerializedError}  = tutorial 

            if (tutorial as { data: ILessonByRTK }) {

                const lessonId: { data: ILessonByRTK } = tutorial as { data: ILessonByRTK }

                postPicture({
                    picture: formData,
                    lesson_id: lessonId.data.id as number
                })
                dispatch(initLesson())
                navigate('/teacher/personal/area/tutorial')
            }
        } catch (error) {
            console.log(error);
        }
    }


    return (
        <>
            <h3>Выберите по какой тематике нужно распределить картинки</h3>
            <Button
            style={{marginBottom: 20}}
                onClick={addAnswer}
            >
                Добавить тематику
            </Button>
            <div>
                {
                    sortObject.lesson.theme.map((val, index) => {
                        return <Card
                            style={pictureItemBlock}
                            key={val.id}
                            hoverable
                        >
                            <SortByPictureComponent
                                changeThemeHandler={(e) => changeThemeHandler(e, index)}
                                props={val}
                                changePicture={(e) => changePicture(e, val.id)}
                            >
                                <DeleteOutlined
                                    style={iconStyle}
                                    onClick={() => deleteBlock(index)}
                                />

                                <Row>
                                    {
                                        sortObject.lesson.arrPicture.map((pic, index) => {

                                            return val.id === pic.theme_id ?
                                                <Col
                                                    span={3}
                                                    style={itemBlock}
                                                    key={pic.id}
                                                >
                                                    <img
                                                        style={pictureStyle}
                                                        src={
                                                            typeof pic.picture !== 'string' ?
                                                                URL.createObjectURL(pic.picture)
                                                                : 'https://t4.ftcdn.net/jpg/04/73/25/49/360_F_473254957_bxG9yf4ly7OBO5I0O5KABlN930GwaMQz.jpg'
                                                        }
                                                        alt='Картинка'
                                                    />

                                                    <CloseOutlined
                                                        style={deleteButtonPicture}
                                                        onClick={() => deletePicture(index)}
                                                    />
                                                </Col>
                                                : null
                                        })
                                    }
                                </Row>
                            </SortByPictureComponent>
                        </Card>
                    })
                }

            </div>

            {contextHolder}
            <Button
            type='primary'
            style={{marginBottom: '3vh'}}
                onClick={() => saveTutorialHandler()}
            >
                Создать пособие
            </Button>

        </>
    )
}

export default SortPicture;