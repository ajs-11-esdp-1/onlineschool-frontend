import {ChangeEvent, useState} from 'react';
import {Button, message, Image, Popconfirm} from 'antd';
import {ITile} from "../../../../interfaces/tutorial/mahjongInterface/IMahjong";
import {usePostLessonMutation, usePostPictureLessonMutation} from "../../../../store/services/lessons";
import {useAppSelector, useAppDispatch} from "../../../../hooks/reduxHooks";
import {RootState} from "../../../../store/store";
import {generateID} from "../../../../helpers/GenerateId/GenerateId";
import {nanoid} from "nanoid";
import MahjongComponent from "../../../../components/InteractiveWorksheets/MahjongComponent/MahjongComponent";
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK';
import { useNavigate } from 'react-router-dom';
import { initLesson } from '../../../../store/features/lesson';


const MahjongCreator = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const lesson        = useAppSelector<RootState>(state => state.lesson);
    const [postLesson]  = usePostLessonMutation();
    const [postPicture] = usePostPictureLessonMutation();
    const [messageApi, contextHolder]   = message.useMessage();
    const [imagesArray, setImagesArray] = useState<ITile[]>([]);

    const error = (message: string) => {
        messageApi.open({
            type: 'error',
            content: message,
        });
    };

    const saveTutorial = async() => {

        const formData: FormData = new FormData() ;

        const copyMahjongObject: ITile[] = [...imagesArray];

        try {
            copyMahjongObject.map((val) => {

                if (typeof val.value !== 'string') {

                    const  file_name = nanoid() + '.' + val.value.name.split('.').slice(-1)[0];

                    const  new_file = new File([val.value],
                        file_name,
                        {type: val.value.type});

                    const dataTransfer = new DataTransfer()

                    dataTransfer.items.add(new_file);

                    val.value = dataTransfer.files[0].name

                    formData.append(`picture` , dataTransfer.files[0])

                    return val
                }

            });
            const copy: string[] = [
                lesson.title,
                lesson.description
            ];

            const validated: boolean = copy.map((val: string) => {

                return val.trim() !== '' ? false : true

            }).includes(true)

            if (validated) {
                error('Необходимо заполнить все поля и выбрать категорию');
                return
            }

            const tutorial = await postLesson({...lesson , lesson:JSON.stringify({images: [...copyMahjongObject]})});


            if (tutorial as { data: ILessonByRTK }) {

                const lessonId : { data: ILessonByRTK } = tutorial as { data: ILessonByRTK }

                postPicture({
                    picture: formData,
                    lesson_id: lessonId.data.id as number
                })
                navigate('/teacher/personal/area/tutorial');
                dispatch(initLesson());
            }

            setImagesArray(() => {
                return []
            })

        } catch (error) {
            console.log('Error FAQ');
        }
    };

    const removeImage = (id: string) => {
        setImagesArray(imagesArray.filter((image) => image.id !== id));
    };

    const changePicture = (e: ChangeEvent<HTMLInputElement> ) => {
        if (e.type && e.target.files && e.target.files.length > 0) {
            const file: File = e.target.files[0];

            if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/svg') {
                setImagesArray([...imagesArray, {id: generateID(), value: file, isMatched: false, isVisible: false}]);
            } else {
                error('Данное фото не корректного формата');
            }
        }
    };

    return (
        <div style={{padding: '20px'}}>
            <h2>Поиск картинок по парам</h2>
            {contextHolder}
            <MahjongComponent onChangeImage={changePicture}/>
            <div style={{marginBottom: '20px'}}>
                <h4>Добавленные картинки:</h4>
                {imagesArray.map((val) => (
                    <div key={val.id} style={{ display: 'flex', alignItems: 'center', marginBottom: '8px' }}>
                        <Image
                            style={{ width: '250px', height: '150px', objectFit: 'cover', marginRight: '8px' }}
                            src={typeof val.value !== 'string' ?
                                URL.createObjectURL(val.value)
                                : 'https://t4.ftcdn.net/jpg/04/73/25/49/360_F_473254957_bxG9yf4ly7OBO5I0O5KABlN930GwaMQz.jpg'}
                            preview={false}
                        />
                        <Popconfirm
                            title="Вы уверены, что хотите удалить эту картинку?"
                            onConfirm={() => removeImage(val.id)}
                            okText="Да"
                            cancelText="Нет"
                        >
                            <Button danger>Удалить</Button>
                        </Popconfirm>
                    </div>
                ))}
            </div>
            <Button type="primary" onClick={saveTutorial}>Создать пособие</Button>
        </div>
    );
};

export default MahjongCreator;