import { useState } from 'react'
import TextArea from 'antd/es/input/TextArea'
import Button from 'antd/es/button/button'
import Tags from '../../../../components/Tags/Tag'
import { nanoid } from 'nanoid'
import { ISortObject, IWordSortTutorial } from '../../../../interfaces/tutorial/sortByWords/SortByWords'
import Typography from 'antd/es/typography/Typography'
import { Col, message, Row } from 'antd'
import { blurStyle, stockStyle, tagsStyle } from './StyleSortTutorial/Style.words'
import { RootState } from '../../../../store/store'
import { useAppSelector, useAppDispatch } from '../../../../hooks/reduxHooks'
import { usePostLessonMutation } from '../../../../store/services/lessons'
import { useNavigate } from 'react-router-dom'
import { initLesson } from '../../../../store/features/lesson'


export type LessonObject = Pick<ISortObject, 'lesson'>

const SortTutorial = () => {

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [valueArea, setArea] = useState<string>('')

    const [isShow, setShow] = useState<boolean>(false)

    const [messageApi, contextHolder] = message.useMessage();

    const [sortObject, setSortObject] = useState<LessonObject>({

        lesson: {
            arrWords: [],
            checkedWord: []
        },


    })

    const lesson = useAppSelector<RootState>(state => state.lesson)

    const [postLesson, { isError }] = usePostLessonMutation()

    const errorMessage = (message: string) => {
        messageApi.open({
            type: 'error',
            content: message,
        });
    };

    const sucssesMessage = (message: string) => {
        messageApi.open({
            type: 'success',
            content: message,
        });
    };

    const parseWords = () => {
        setShow(!isShow)

        const copyValue: IWordSortTutorial[] = valueArea.split(' ').map((val) => {

            return { value: val, id: nanoid(), isDrable: false, styleHandler: false }

        })

        setSortObject({
            ...sortObject, lesson: {
                ...sortObject.lesson,
                arrWords: copyValue
            }
        })

    }

    const addWord = (val: IWordSortTutorial) => {

        const index: number = sortObject.lesson.arrWords.findIndex((obj) => obj.id === val.id)

        if (index >= 0) {

            const copy: LessonObject = { ...sortObject }

            copy.lesson.arrWords[index] = {
                ...copy.lesson.arrWords[index],
                isDrable: true,
                styleHandler: true
            }

            copy.lesson.checkedWord = [...copy.lesson.checkedWord, {
                ...val, isDrable: true,
                styleHandler: true
            }]

            setSortObject(copy)
        }

    }

    const editWords = () => {
        const copy: LessonObject = { ...sortObject }

        copy.lesson.checkedWord = []

        setSortObject({ ...copy })

        setShow(!isShow)

    }

    const removeWord = (val: IWordSortTutorial) => {

        const index: number = sortObject.lesson.arrWords
            .findIndex((obj) => obj.id === val.id)

        const indexChecked: number = sortObject.lesson.checkedWord
            .findIndex((obj) => obj.id === val.id)

        if (index >= 0 && indexChecked >= 0) {

            const copy: LessonObject = { ...sortObject }

            copy.lesson.arrWords[index] = {
                ...val,
                isDrable: false,
                styleHandler: false
            }

            copy.lesson.checkedWord.splice(indexChecked, 1)

            setSortObject(copy)

        }
    }

    const onFinish = async () => {
        try {
            if (sortObject.lesson.checkedWord.length !== 0) {

                const copy: string[] = [
                    lesson.title,
                    lesson.description
                ];
        
                const validated: boolean = copy.map((val: string) => {
        
                    return val.trim() !== '' ? false : true
        
                }).includes(true)
        
                if (validated) {
                    errorMessage('Необходимо заполнить все поля и выбрать категорию');
                    return
                }

                const data = await postLesson({ ...lesson, lesson: JSON.stringify({ ...sortObject.lesson }) })

                const saveHandler: boolean = await isError
                if (!(data as { error: object }).error) {
                    if (!saveHandler) {
                        sucssesMessage('Туториал сохранен')
                        setSortObject({
                            ...sortObject,
    
                            lesson: {
                                arrWords: [],
                                checkedWord: []
                            },
    
                        })
                        dispatch(initLesson())
                        navigate('/teacher/personal/area/tutorial')
                    }
                }
            }
            return errorMessage('Заполните все поля')
        } catch (e) {
            errorMessage('Возникла ошибка при загрузке данных')
        }
    };

    return (
        <>
            <div style={{ marginBottom: 35 }}>
                {
                    !isShow ?
                        <>
                            <Row>
                                <Col span={24}>
                                    <TextArea
                                        style={{ height: "40vh", resize: 'none' }}
                                        maxLength={3000}
                                        value={valueArea}
                                        onChange={(e) => {
                                            setArea(e.target.value)
                                        }}
                                    />
                                </Col>
                            </Row>

                            <Button
                                style={{ marginTop: 20 }}
                                type='primary'
                                onClick={parseWords}
                            >
                                Выборка
                            </Button>
                        </>
                        :
                        <>
                            <Typography>
                                {
                                    sortObject.lesson.arrWords.map((val) => {
                                        return <span
                                            style={!val.isDrable ? stockStyle : blurStyle}
                                            key={val.id}
                                            onClick={() => addWord(val)}
                                        >{val.value}</span>
                                    })
                                }
                            </Typography>
                            <div style={{ marginTop: 15, marginBottom: 15 }}>
                                {
                                    sortObject.lesson.checkedWord.map((val) => {
                                        return <Tags
                                            style={tagsStyle}
                                            key={val.id}
                                            onClose={() => removeWord(val)}
                                            label={val.value}
                                            closable={true}
                                        />
                                    })
                                }
                            </div>
                            <Button
                                style={{ marginRight: 20, marginBottom: 10 }}
                                onClick={editWords}
                            >
                                Редактировать
                            </Button>
                            <Button
                                type='primary'
                                onClick={onFinish}
                            >
                                Сохранить
                            </Button>
                        </>
                }
            </div>
            {contextHolder}
        </>
    )
}

export default SortTutorial