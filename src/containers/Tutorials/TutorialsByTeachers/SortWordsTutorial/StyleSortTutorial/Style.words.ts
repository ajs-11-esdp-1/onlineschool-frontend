import { CSSProperties } from "react";


export const stockStyle:CSSProperties = {
    color:'gray',
    marginRight:'15px',
    fontSize: '16px',
    cursor: 'pointer'
}

export const blurStyle: CSSProperties = {
    marginRight: '15px',
    background:'#2db6f577',
    borderRadius: '4px',
    fontSize: '16px',
    padding: '4px',
    cursor: 'pointer'
}

export const tagsStyle: CSSProperties = {
    padding: '4px',
    fontSize: '16px'
}