import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form, Input, Tooltip, Card, Alert, message } from 'antd';
import { PlusOutlined, MinusCircleOutlined, DeleteOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { IWrittenAnswerForm } from '../../../../interfaces/tutorial/writtenAnswer/IWrittenAnswerForm';
import { IWrittenAnswerValues } from '../../../../interfaces/tutorial/writtenAnswer/IWrittenAnswerForm';
import { usePostLessonMutation, usePostPictureLessonMutation } from '../../../../store/services/lessons';
import { useNavigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../../../hooks/reduxHooks';
import FileUpload from '../../../../components/Form/FileUpload/FileUpload';
import { nanoid } from 'nanoid';
import MessageEnum from '../../../../enum/messageEnum/MessageEnum';
import { initLesson } from '../../../../store/features/lesson';
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK';

const { TextArea } = Input;
export type LessonObject = Pick<IWrittenAnswerForm, 'lesson'>

function WrittenAnswerForm() {
    const [messageApi, contextHolder] = message.useMessage();
    const [addWorksheet, { isError }] = usePostLessonMutation();
    const [sendPictures] = usePostPictureLessonMutation();

    const [form] = Form.useForm();
    const [showAlert, setShowAlert] = useState(false);
    const [picsArray, setPicsArray] = useState<LessonObject>({
        lesson: {
            questions: [],
            arrPictures: []
        }
    });

    const lesson = useAppSelector(state => state.lesson);

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const messageBy = (message: string, type: MessageEnum) => {
        messageApi.open({
            type: type,
            content: message,
        });
    };

    useEffect(() => {
        setShowAlert(isError);
    }, [isError]);

    const handleClose = () => {
        setShowAlert(false);
    }

    const fileChangeHandler = (e: ChangeEvent<HTMLInputElement>, index: number) => {

        if (e.target && e.target.files && e.target.files[0]) {
            const file = e.target.files[0];

            if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/svg') {

                return setPicsArray({
                    ...picsArray, lesson: {
                        ...lesson, arrPictures: [...picsArray.lesson.arrPictures,
                        { id: nanoid(), question_id: index, picture: file }
                        ]
                    }
                })
            }
            messageBy('Данное фото не корректного формата', MessageEnum.error);
        }
        e.target.value = '';
    }

    const deletePicture = (index: number) => {

        const updatedArrPictures = picsArray.lesson.arrPictures.filter(
            (_, i) => i !== index
        );

        setPicsArray((prevState) => ({
            ...prevState,
            lesson: {
                ...prevState.lesson,
                arrPictures: updatedArrPictures,
            },
        }));

    }

    const onFinish = async () => {

        const formData: FormData = new FormData();

        const copyPictureObject = { ...picsArray };

        copyPictureObject.lesson.arrPictures.map((val) => {

            if (typeof val.picture !== 'string') {

                const file_name = nanoid() + '.' + val.picture.name.split('.').slice(-1)[0];

                const new_file = new File([val.picture],
                    file_name,
                    { type: val.picture.type });

                const dataTransfer = new DataTransfer()

                dataTransfer.items.add(new_file);

                val.picture = dataTransfer.files[0].name

                formData.append(`picture`, dataTransfer.files[0])

                return val

            }

        });

        const QAForm: IWrittenAnswerValues = form.getFieldsValue(true);

        const updatedForm = QAForm.questions.map((question, index) => {
            const updatedAnswers = question.answers.map((answer) => {
                return (
                    { answer: answer.answer.toLowerCase(),  }
                )
            })
            const matchingImage =  copyPictureObject.lesson.arrPictures.find((image) => image.question_id === index)
            return { ...question, answers: updatedAnswers, picture: matchingImage ? matchingImage.picture : null }
        })

        const copy: string[] = [
            lesson.title,
            lesson.description
        ];

        const validated: boolean = copy.map((val: string) => {

            return val.trim() !== '' ? false : true

        }).includes(true)

        if (validated) {
            messageBy('Необходимо заполнить все поля и выбрать категорию', MessageEnum.error);
            return
        }
        if (!validated && QAForm?.questions?.length !== 0) {

            const data = await addWorksheet({ ...lesson, lesson: JSON.stringify({ questions: updatedForm, arrPictures: picsArray.lesson.arrPictures }) });

            if (!(data as { error: object }).error) {
                
                const lessonId : { data: ILessonByRTK } = data as { data: ILessonByRTK }

                sendPictures({
                    picture: formData,
                    lesson_id: lessonId.data.id as number
                })
                form.resetFields();
                dispatch(initLesson());
                navigate('/teacher/personal/area/tutorial');
            }
            
        }

    };

    return (
        <>
            {showAlert && (
                <Alert style={{ marginTop: 15 }} onClose={handleClose} message={'Ошибка сохранения. Поробуйте еще раз'} type="error" closable showIcon />
            )}
            <Form
                name="writtenAnswerForm"
                style={{ maxWidth: 700 }}
                onFinish={onFinish}
                autoComplete="off"
                layout='vertical'
                form={form}
            >
                <Form.List name="questions" initialValue={[{ question: '' }]}>
                    {(fields, { add, remove }) => {
                        return (
                            <>
                                {fields.map((field, index) => (
                                    <Card
                                        style={{marginBottom: 20}}
                                        key={field.key}
                                        title={
                                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <p style={{fontWeight: 700}}>Вопрос №{index + 1}</p>
                                                <div>
                                                    {fields.length > 1
                                                        ?
                                                        <Tooltip title="Удалить"><DeleteOutlined onClick={() => { remove(field.name) }} /></Tooltip>
                                                        :
                                                        null
                                                    }
                                                </div>
                                            </div>} bordered={false}>
                                        
                                            <Form.Item

                                                {...field}
                                                name={[field.name, 'question']}
                                                key={field.key + 'question'}
                                                rules={[{ required: true, message: 'Это поле является обязательным' }]}
                                                style={{ maxWidth: '550px' }}
                                            >
                                                <TextArea autoSize style={{ width: '100%' }} name='question' placeholder="Вопрос" />
                                            </Form.Item>

                                            <Form.Item

                                                {...field}
                                                name={[field.name, 'picture']}
                                                key={field.key + 'picture'}
                                            >
                                                <FileUpload deletePicture={() => deletePicture(index)} name='picture' onChange={(e) => fileChangeHandler(e, index)} />
                                            </Form.Item>

                                        <Form.List name={[field.name, 'answers']} initialValue={[{ answer: '' }]}>
                                            {(answers, { add, remove }) => {
                                                return (
                                                    <>
                                                        {answers.map((answer, index) => (
                                                            <div key={answer.key}>
                                                                <Form.Item
                                                                    label={index === 0 ? <span>
                                                                        <Tooltip
                                                                            title='Вы можете добавить столько ответов, сколько хотите (например, разные способы написания одного и того же ответа), но все они должны быть правильными ответами.'>
                                                                            <InfoCircleOutlined style={{ color: 'rgb(22, 119, 255)' }} />
                                                                        </Tooltip> Правильные ответы: </span> : ''}
                                                                    required={false}
                                                                    key={answer.key}
                                                                >
                                                                        <Form.Item
                                                                            {...answer}
                                                                            name={[answer.name, 'answer']}
                                                                            key={answer.key + 'answer'}
                                                                            rules={[{ required: true, message: 'Это поле является обязательным' }]}
                                                                            noStyle
                                                                        >
                                                                            <Input style={{ maxWidth: 250 }} name='answer' placeholder="Правильный ответ" />
                                                                        </Form.Item>
                                                                        {answers.length > 1 ? (
                                                                            <MinusCircleOutlined
                                                                                className="dynamic-delete-button"
                                                                                onClick={() => remove(answer.name)}
                                                                            />
                                                                        ) : null}
                                                                </Form.Item>
                                                            </div>
                                                        ))}

                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                block
                                                            >
                                                                <PlusOutlined /> Добавить ответ
                                                            </Button>
                                                        </Form.Item>
                                                    </>
                                                );
                                            }}
                                        </Form.List>
                                    </Card>
                                ))}
                                    <Form.Item>
                                        <Button block style={{ background: '#b9b6b6', color: '#fff', marginTop: 20, borderColor: '#b9b6b6' }} icon={<PlusOutlined />} onClick={() => { add() }}>Добавить вопрос</Button>
                                    </Form.Item>
                            </>
                        );
                    }}
                </Form.List>
                {contextHolder}
                <Form.Item style={{display: 'flex', justifyContent: 'center'}}>
                    <Button size='large' type="primary" htmlType="submit">
                        Сохранить
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default WrittenAnswerForm;