import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { TutorialTypeEnum } from '../../../enum/tutorialType/TutorialTypeEnum'
import { ILessonByRTK } from '../../../interfaces/tutorial/LessonByRTK'
import { useGetTutorialsQuery } from '../../../store/services/tutorial'

const TutorialById = () => {

    const params = useParams()

    

    const {data:arrLessons} = useGetTutorialsQuery()

    const [lesson , setLesson] = useState<ILessonByRTK>()


    useEffect(() => {
        if(arrLessons && params.id) {

            const index = arrLessons.findIndex((val) => val.id === params.id)

            setLesson(arrLessons[index])

            if(lesson?.template.title === TutorialTypeEnum.wordsSort){

                console.log(123)

            }

            if(index >= 0) {
                setLesson(arrLessons[index])
                console.log(arrLessons[index]);
                
            }
        }
    },[arrLessons])
    return (
        <div>
            
        </div>
    )
}

export default TutorialById