import { useEffect, useState } from 'react'
import { useGetTutorialByTeacherQuery } from '../../../../store/services/lessons'
import { Button, Card, Col, Result, Row, Select, Switch } from 'antd'
import { ILessonByRTK } from '../../../../interfaces/tutorial/LessonByRTK'
import TutorialItem from '../../../../components/PersonalArea/TeacherArea/TutorialItem/TutorialItem'
import { useGetTemplateQuery } from '../../../../store/services/template'
import { useGetDirectionQuery } from '../../../../store/services/direction'
import { SmileOutlined } from '@ant-design/icons'
import { clearTutorial } from '../TeacherAreaStyle/TeacherAreaStyle'
import Title from 'antd/es/typography/Title'
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'


export interface ISortTutorial {
    template: number | null
    direction: number | null
    public: boolean
}

const TutorialByTeacher = () => {

    const navigate = useNavigate()

    const { data: arrTutorial } = useGetTutorialByTeacherQuery()

    const [tutorial, setTutorial] = useState<ILessonByRTK[]>([])

    const { data: arrTemplate } = useGetTemplateQuery();

    const { data: arrDirection } = useGetDirectionQuery();

    const [currentTutorial, setCurent] = useState<ISortTutorial>({
        template: null,
        direction: null,
        public: true
    })

    const changeSwitch = () => {
        if (arrTutorial) {
            if (currentTutorial.direction) {
                return setTutorial(() => {
                    return !currentTutorial.template ?
                        [
                            ...arrTutorial.filter((val) => {
                                return val.direction_id === currentTutorial.direction &&
                                    val.public === currentTutorial.public
                            })]
                        :
                        [
                            ...arrTutorial.filter((val) => {
                                return val.direction_id === currentTutorial.direction &&
                                    val.template_id === currentTutorial.template &&
                                    val.public === currentTutorial.public
                            })
                        ]
                })
            }
            if (currentTutorial.template) {
                return setTutorial(() => {
                    return !currentTutorial.direction ? [...arrTutorial.filter((val) => {
                        return val.template_id === currentTutorial.template &&
                            val.public === currentTutorial.public
                    })]
                        :
                        [
                            ...arrTutorial.filter((val) => {
                                return val.template_id === currentTutorial.template &&
                                    val.direction_id === currentTutorial.direction &&
                                    val.public === currentTutorial.public
                            })
                        ]
                })
            }


            setTutorial(() => {
                return arrTutorial.filter((val) => {
                    return val.public === currentTutorial.public

                })
            })
        }

    }

    useEffect(() => {

        changeSwitch()

    }, [currentTutorial])

    useEffect(() => {

        arrTutorial && setTutorial(() => {
            return arrTutorial.filter((val) => {
                return val.public === true
            })
        })

    }, [arrTutorial])


    return (
        <div>
            <Row
                content='center'
                align='middle'
                justify='space-between'
                style={{ marginTop: '3vh', marginBottom: '3vh', marginRight: 0, marginLeft: 0 }}
                gutter={[{ md: 4, lg: 4 }, 22]}
            >
                <Col

                    md={24}
                    sm={24}
                    lg={3}
                >
                    <Switch
                        checkedChildren="Опубликованые"
                        unCheckedChildren="Закрытые"
                        onChange={(e) => {
                            setCurent((prev) => {
                                return { ...prev, public: e }
                            })
                        }}
                        defaultChecked={currentTutorial.public}
                    />
                </Col>
                <Col
                    md={24}
                    sm={24}
                    lg={4}
                >
                    <Select
                        defaultValue="Выберите пособие"
                        style={{ width: '220px' }}
                        onChange={(e) => {
                            setCurent((prev) => {
                                return { ...prev, template: Number(e) }
                            })
                        }}
                        options={arrTemplate ?
                            [...arrTemplate.map((val) => {
                                return { value: val.id, label: val.title }
                            }), { value: 'all', label: 'Все' }]
                            :
                            [{
                                value: 'all', label: 'Все'
                            }]

                        }
                    />
                </Col>
                <Col
                    md={24}
                    sm={24}
                    lg={4}
                >
                    <Select
                        defaultValue="Выберите предмет"
                        style={{ maxWidth: '220px' }}
                        onChange={(e) => {
                            setCurent((prev) => {
                                return { ...prev, direction: Number(e) }
                            })
                        }}
                        options={arrDirection ?
                            [...arrDirection.map((val) => {
                                return { value: val.id, label: val.direction }
                            }), { value: 'all', label: 'Все' }]
                            :
                            [{
                                value: 'all', label: 'Все'
                            }]
                        }
                    />
                </Col>
            </Row>
            <div>
                <Button type='primary' onClick={() => navigate('/new/lesson')}>
                    Создать пособие
                </Button>
            </div>
            <div
                style={{ marginTop: '3vh', marginBottom: '2vh' }}
            >
                <Card>
                    <Row
                        gutter={[18, 18]}
                    >
                        {
                            tutorial.length !== 0 ? tutorial.map((val) => {
                                return <Col
                                    style={{ display: 'flex' }}
                                    lg={8}
                                    md={12}
                                    sm={24}
                                    key={val.id}
                                >
                                    <TutorialItem
                                        {...val}
                                    />
                                </Col>
                            })
                                :
                                <Col
                                    span={24}
                                    content='center'
                                >
                                    <Result
                                        icon={<SmileOutlined />}
                                        style={clearTutorial}
                                        title="Пока нет пособий"
                                        extra={
                                            <Title level={5}>Можете создать в разделе <NavLink to='/new/lesson' children='Интерактивные пособия' />
                                            </Title>
                                        }
                                    />
                                </Col>
                        }
                    </Row>
                </Card>
            </div>
        </div>
    )
}

export default TutorialByTeacher