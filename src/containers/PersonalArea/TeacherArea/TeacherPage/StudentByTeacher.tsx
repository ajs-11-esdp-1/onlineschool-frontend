import { useEffect, useState } from 'react'
import { Card } from 'antd'
import { useGetArrStudentTeacherQuery } from '../../../../store/services/teacher.student'
import { IStudent } from '../../../../interfaces/teacher/Teacher'
import StudentItem from '../../../../components/PersonalArea/TeacherArea/StudentItem'

const StudentByTeacher = () => {

    const {data: arrStudent} = useGetArrStudentTeacherQuery()

    const [students , setStudent] = useState<IStudent[]>([])

    useEffect(() => {

        arrStudent && setStudent([...arrStudent])
        
        
    }, [arrStudent ])

    return (
        <div>
            <Card>
                {
                    students.map((val) => {
                        return <StudentItem {...val} />
                    })
                }
                
            </Card>
        </div>
    )
}

export default StudentByTeacher