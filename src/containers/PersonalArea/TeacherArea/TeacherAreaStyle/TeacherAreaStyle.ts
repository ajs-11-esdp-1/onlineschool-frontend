import { CSSProperties } from "react";

export const menuStyle: CSSProperties = {
    position:'fixed',
    bottom: '3vh',
    left: '0.4vw' ,
    
}

export const drawerStyle: CSSProperties = {
    background: '#235789',
    color: 'white'
}

export const activeStyle: CSSProperties = {
    color: '#FF784F',

}
export const stockStyle :CSSProperties = {
    color: 'white'
}
export const menuButton : CSSProperties = {
    color: '#52c41a',
    fontSize:'1.8rem'
}
export const clearTutorial : CSSProperties = {
    display:'flex',
    alignItems: 'center',
    justifyContent:'center',
    flexDirection:'column',
    width:'100%',
    margin: '0 auto'

}
export const mainBlock : CSSProperties = {
    width: '90%',
    margin: '3vh  auto'
}