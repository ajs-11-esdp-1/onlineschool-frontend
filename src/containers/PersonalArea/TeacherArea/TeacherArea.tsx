import { Button, Drawer} from 'antd'
import { useState } from 'react'
import { activeStyle, drawerStyle, menuButton, stockStyle } from './TeacherAreaStyle/TeacherAreaStyle';
import { LayoutFilled } from '@ant-design/icons';
import { NavLink, Outlet } from 'react-router-dom';
import { arrayLink } from '../../Menus/MenuLinks/TeacherAreaLink/TeacherLinks';
import Title from 'antd/es/typography/Title';

const TeacherArea = () => {
    const [open, setOpen] = useState(true);
  
    

    return (
        <div className='container'>
            <Button
                onClick={() => setOpen(!open)}
                shape="circle"
                size='large'
            >
                <LayoutFilled  
                    style={menuButton}                   
                />
            </Button>
            
                <Drawer
                    placement='left'
                    open={open}
                    width='20%'
                    closable={false}
                    onClose={() => setOpen(!open)}
                    style={drawerStyle}
                >
                    {
                        arrayLink.map((val , index) => {
                            return <Title 
                                    level={4} 
                                    key={index}
                                >
                                <NavLink
                                    onClick={() => setOpen(!open)}
                                    to={val.label}
                                    style={({isActive}) => {
                                        return   isActive ? activeStyle  : stockStyle 
                                    }}
                                >
                                    {val.children}
                                </NavLink>
                            </Title>
                        }) 
                    }
                    
                </Drawer>
            
            <div>
                <Outlet/>
            </div>
            
        </div>
    )
}

export default TeacherArea