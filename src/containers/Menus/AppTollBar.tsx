import { useState } from 'react';
import { ConfigProvider, Menu, theme } from 'antd';
import { MenuProps } from 'antd';
import { Header } from 'antd/es/layout/layout';
import MenuHandler from './MenuLinks/MenuLinks';
import './MenuLinks/MenuLinksStyle/headerLogo.css';
import { MenuOutlined } from '@ant-design/icons';
import { NavLink } from "react-router-dom";


const AppToolbar = () => {
    const [current, setCurrent] = useState('mail');

    const {
        token: { colorBgContainer },
    } = theme.useToken();

    const onClick: MenuProps['onClick'] = (e) => {
        setCurrent(e.key);
    };

    return (
        <Header style={{ position: 'sticky', top: 0, zIndex: 100, background: colorBgContainer, display: 'flex', alignItems: 'center', justifyContent: 'space-between', height: '75px', boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.03), 0 1px 6px -1px rgba(0, 0, 0, 0.02), 0 2px 4px 0 rgba(0, 0, 0, 0.02)' }}>
            <NavLink to='/'>
                <div className='headerLogo'></div>
            </NavLink>
            <ConfigProvider theme={{
                components: {
                    Menu: {
                        colorPrimary: '#52c41a',
                        itemSelectedBg: '#f6ffed',
                        horizontalItemHoverColor: '#61CE70',
                        horizontalItemSelectedColor: '#61CE70',
                        activeBarHeight: 3,
                    }
                },
            }}>
                <Menu
                    style={{ flex: "auto", minWidth: 0, lineHeight: '76px', justifyContent: 'flex-end' }}
                    overflowedIndicator={<MenuOutlined style={{ color: '#52c41a', fontSize: 18 }} />}
                    onClick={onClick}
                    selectedKeys={[current]}
                    mode="horizontal"
                    items={MenuHandler()} />
            </ConfigProvider>
        </Header>

    );
};

export default AppToolbar;