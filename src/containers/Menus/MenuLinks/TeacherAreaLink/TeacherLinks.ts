import { TeacherLinkArea } from "../../../../interfaces/PersonalArea/PersonalArea";

export const arrayLink : TeacherLinkArea[] = [
    {
        label: '/teacher/personal/area/tutorial',
        children: 'Мои пособия'
    },
    {
        label: '/teacher/personal/area/student',
        children: 'Мои ученики',
    },
    {
        label: '/teacher/personal/are',
        children: 'Моя галерея',
    },
    // {
    //     label: '/teacher/personal/area/student',
    //     children: 'Мои ученики'
    // }
]