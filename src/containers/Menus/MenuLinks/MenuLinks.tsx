import { Avatar, MenuProps } from "antd";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { RoleEnum } from "../../../enum/roleEnum/RoleEnum";
import { useAppSelector } from "../../../hooks/reduxHooks";
import { useLogoutMutation } from "../../../store/services/auth";
import { RootState } from "../../../store/store";
import { apiUrl } from "../../../common/constants";
import NoticeComponentStudent from "../../../components/Notice/NoticeComponent";


const MenuHandler = () => {
    const { user } = useAppSelector<RootState>(state => state.auth);
    const [logout] = useLogoutMutation();

    const TeacherLinks: MenuProps['items'] = [
        {
            label: <NavLink
                to='/'
            >Главная</NavLink>,
            key: 'home',
        },
        {
            label: <NavLink
                style={{ height: '40px' }}
                to='/worksheets'
            >Интерактивные пособия</NavLink>,
            key: 'worksheets',
        },
        {
            label: 'Генератор пособий',
            key: 'generator',
            children: [
                {
                    label: <NavLink to="/generator/words">Слова</NavLink>,
                    key: 'reading',
                },
                {
                    label: <NavLink to="/generator/math">Математика</NavLink>,
                    key: 'math',
                },
            ],
        },
        {
            label: <NavLink
                to='/shop'
            >Магазин</NavLink>,
            key: 'shop',
        },
        {
            label: <NavLink
                to='/subscription'
            >Подписка</NavLink>,
            key: 'subscription',
        },
        {
            label: <NavLink
                to='/notice/teacher'
            ><NoticeComponentStudent /></NavLink>,
            key: 'notice/teacher',
        },
        {
            label: <Avatar src={apiUrl + '/uploads/user/' + user?.avatar} />,
            key: 'teacher',
            children: [
                {
                    type: 'group',
                    label: <span style={{color: 'black', fontWeight: 'bolder'}}>{user?.firstName} {user?.lastName}</span>,
                    children: [
                        {
                            label: <NavLink to='/teacher/personal/area'>Личный кабинет</NavLink>,
                            key: 'profile',
                        },
                        {
                            label: 'Выйти',
                            key: 'logout',
                            onClick: () => logout()
                        },
                    ],
                }
            ]
        },
    ];

    const StudentLinks: MenuProps['items'] = [
        {
            label: <NavLink
                to='/'
            >Главная</NavLink>,
            key: 'home',
        },
        {
            label: <NavLink
                to='/worksheets'
            >Интерактивные пособия</NavLink>,
            key: 'worksheets',
        },
        {
            label: <NavLink
                to='/notice/student'
            ><NoticeComponentStudent /></NavLink>,
            key: 'notice/student',
        },
        {
            label: <Avatar src={apiUrl + '/uploads/user/' + user?.avatar} />,
            key: 'student',
            children: [
                {
                    type: 'group',
                    label: <span style={{color: 'black', fontWeight: 'bolder'}}>{user?.firstName} {user?.lastName}</span>,
                    children: [
                        {
                            label: <NavLink to='/room'>Личный кабинет</NavLink>,
                            key: 'profile',
                        },
                        {
                            label: 'Выйти',
                            key: 'logout',
                            onClick: () => logout()
                        },
                    ],
                }
            ]
        },
    ];

    const SchoolLinks: MenuProps['items'] = [
        {
            label: <NavLink
                to='/'
            >Главная</NavLink>,
            key: 'home',
        },
        {
            label: <NavLink
                to='/worksheets'
            >Интерактивные пособия</NavLink>,
            key: 'worksheets',
        },
        {
            label: 'Генератор пособий',
            key: 'generator',
            children: [
                {
                    label: <NavLink to="/generator/words">Слова</NavLink>,
                    key: 'reading',
                },
                {
                    label: <NavLink to="/generator/math">Математика</NavLink>,
                    key: 'math',
                },
            ],
        },
        {
            label: <NavLink
                to='/shop'
            >Магазин</NavLink>,
            key: 'shop',
        },
        {
            label: <Avatar src={apiUrl + '/uploads/user/' + user?.avatar} />,
            key: 'school',
            children: [
                {
                    type: 'group',
                    label: <span style={{color: 'black', fontWeight: 'bolder'}}>{user?.firstName} {user?.lastName}</span>,
                    children: [
                        {
                            label: <NavLink to='/room'>Личный кабинет</NavLink>,
                            key: 'profile',
                        },
                        {
                            label: 'Выйти',
                            key: 'logout',
                            onClick: () => logout()
                        },
                    ],
                }
            ]
        }
    ];

    const AnonymousLinks: MenuProps['items'] = [
        {
            label: <NavLink
                to='/'
            >Главная</NavLink>,
            key: 'home',
        },
        {
            label: <NavLink
                to='/worksheets'
            >Интерактивные пособия</NavLink>,
            key: 'worksheets',
        },
        {
            label: 'Генератор пособий',
            key: 'generator',
            children: [
                {
                    label: <NavLink to="/generator/words">Слова</NavLink>,
                    key: 'reading',
                },
                {
                    label: <NavLink to="/generator/math">Математика</NavLink>,
                    key: 'math',
                },
            ],
        },
        {
            label: <NavLink
                to='/shop'
            >Магазин</NavLink>,
            key: 'shop',
        },
        {
            label: <NavLink
                to='/subscription'
            >Подписка</NavLink>,
            key: 'subscription',
        },
        {
            label: <NavLink
                to='/login'
            >Вход</NavLink>,
            key: 'signin',
        },
        {
            label: <NavLink
                to='/register'
            >Регистрация</NavLink>,
            key: 'signup',
        },
    ];

    const [links, setLinks] = useState<MenuProps['items']>(AnonymousLinks);

    useEffect(() => {
        if (user) {
            if (user.role?.role === RoleEnum.SCHOOL) {
                return setLinks(SchoolLinks);
            } else if (user.role?.role === RoleEnum.TEACHER) {
                return setLinks(TeacherLinks);
            }
            return setLinks(StudentLinks);
        }
        setLinks(AnonymousLinks);

    }, [user])

    return links
};

export default MenuHandler;