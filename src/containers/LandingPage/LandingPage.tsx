import { Row, Col, Typography, Button, Card } from "antd";
import './Styles/LandingPageStyles.css';
import { NavLink } from "react-router-dom";

const { Title, Paragraph, Text } = Typography;

function LandingPage() {
    return (
        <div className="landing-bg landing-container">
            <Row>
                <Col span={24}>
                    {/* PROMO-CONTENT */}
                    <div className="promo-container">
                        <Title className="promo-title roboto-text">Сделайте свои уроки более увлекательными и интересными</Title>
                        <Paragraph className="promo-text">Создавайте свои собственные интерактивные пособия и используйте для своих уроков</Paragraph>
                        <i className="promo-bg-image"></i>
                        <NavLink to={"/register"}>
                            <Button className="promo-btn" type="primary">Начните создавать бесплатно</Button>
                        </NavLink>
                    </div>
                </Col>
                <Col span={24} className="feature-margin">
                    {/* FEATURES */}
                    <Title className="feature-title" level={2}>Обучение в Class2Be – это удобно и результативно</Title>
                    <div className="features-container">
                        <Card hoverable className="feature-card">
                            <i className="interactive-icon"></i>
                            <Paragraph className="card-text">Создавайте креативные задания онлайн</Paragraph>
                            <Text>Интерактивные пособия помогут вам воплотить в жизнь креативные идеи и сделать обучение увлекательным</Text>
                        </Card>
                        <Card hoverable className="feature-card">
                            <i className="generator-icon"></i>
                            <Paragraph className="card-text">Генерируйте задания в несколько кликов</Paragraph>
                            <Text>Автоматический генератор упражнений поможет легко и быстро создавать и сразу распечатывать задания для учеников</Text>
                        </Card>
                        <Card hoverable className="feature-card">
                            <i className="grade-icon"></i>
                            <Paragraph className="card-text">Отслеживайте прогресс своих учеников</Paragraph>
                            <Text>Будьте в курсе успеваемости учащихся благодаря автоматической оценке интерактивных заданий</Text>
                        </Card>
                    </div>
                </Col>
                <Col span={24}>
                    {/* HOW TO */}
                    <div className="how-to-container">
                        <div className="how-to-text">
                            <Title className="how-to-title" level={2}>Как создавать интерактивные пособия</Title>
                            <Paragraph className="how-to-p" style={{ fontSize: 16, fontWeight: 700 }}>
                                Легко и просто! Вы можете создать свое интерактивное пособие всего за несколько простых шагов.
                            </Paragraph>
                            <NavLink to={"/register"}>
                                <Button className="promo-btn" type="primary">Зарегистрироваться</Button>
                            </NavLink>
                        </div>
                        <div className="ratio ratio-16x9">
                            <iframe src="https://www.youtube.com/embed/kDdg2M1_EuE" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default LandingPage;