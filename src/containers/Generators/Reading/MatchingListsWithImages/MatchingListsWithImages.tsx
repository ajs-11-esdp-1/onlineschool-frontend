import { useEffect, useState } from "react";
import { predefinedWords } from "../../../../components/WorksheetsGenerator/Reading/MatchingListsWithImagesComponent/WordsArray/WordsArray";
import {
    IWordObject
} from "../../../../interfaces/generatedTutorials/IMatchingListsWithImages/IMatchingListsWithImages";
import { Button, message } from "antd";
import MatchingListsWithImagesComponent
    from "../../../../components/WorksheetsGenerator/Reading/MatchingListsWithImagesComponent/MatchingListsWithImagesComponent";
import { useAppSelector } from "../../../../hooks/reduxHooks";
import { RoleEnum } from "../../../../enum/roleEnum/RoleEnum";
import { useNavigate } from "react-router-dom";
import './StyleMatchingListsWithImages/StyleMatchingListsWithImages.css'


const MatchingListsWithImages = () => {
    const { user } = useAppSelector(state => state.auth);
    const [userCheck, setUser] = useState<boolean>(false);
    const navigate = useNavigate();

    const [selectedWords, setSelectedWords] = useState<IWordObject[]>([]);
    const [createdTutorial, setCreatedTutorial] = useState<IWordObject[]>([]);
    const [messageApi, contextHolder] = message.useMessage();
    const maxSelectedWords = 10;

    const error = (message: string) => {
        messageApi.open({
            type: 'error',
            content: message,
        });
    };

    const handleWordClick = (wordObject: IWordObject) => {
        if (selectedWords.length >= maxSelectedWords) {
            error(`Максимальное количество выбранных слов: ${maxSelectedWords}`);
            return;
        }

        const isWordAlreadySelected = selectedWords.findIndex(word => word.word === wordObject.word) !== -1;

        if (!isWordAlreadySelected) {
            setSelectedWords([...selectedWords, wordObject]);
        }
    };

    const handleWordRemove = (wordId: string) => {
        const updatedSelectedWords = selectedWords.filter((word) => word.id !== wordId);
        setSelectedWords(updatedSelectedWords);
    };

    const userHandler = () => {
        if (user) {

            if (user.role.role === RoleEnum.TEACHER || user.role.role === RoleEnum.SCHOOL) {

                return true;
            }
        }

        return false
    }

    useEffect(() => {

        setUser(userHandler());

    }, [user])

    return (
        <div className="container">
            {contextHolder}
            <div className="mainDiv">
                <div className="WordsDiv">
                    <h3>Список слов</h3>
                    <div className="contentDiv">
                        {predefinedWords.map((val, index) => (
                            <div className="wordCSS"
                                key={index}
                                onClick={() => handleWordClick(val)}
                            >
                                {val.word}
                            </div>
                        ))}
                    </div>
                </div>
                <div className="WordsDiv">
                    <h3>Выбранные слова</h3>
                    <div className="contentDiv">
                        {selectedWords.map((word, index) => (
                            <div className="wordCSS" key={index}>
                                {word.word}
                                <Button onClick={() => handleWordRemove(word.id)}>Удалить</Button>
                            </div>
                        ))}
                    </div>
                </div>

                {createdTutorial.length > 0 && (
                    <div className="WordsDiv">
                        <h5>Созданное пособие</h5>
                        <MatchingListsWithImagesComponent val={createdTutorial} />
                    </div>
                )}
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '5px', marginBottom: '25px' }}>
                <Button
                    key="submit"
                    type="primary"
                    onClick={
                        userCheck ?
                            () => { setCreatedTutorial(selectedWords) }
                            :
                            () => { navigate('/login') }}
                >
                    Создать пособие
                </Button>
            </div>
        </div>
    );
};

export default MatchingListsWithImages;