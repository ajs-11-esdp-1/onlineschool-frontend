import { useState, useEffect } from 'react';
import { Form, Input, Select, Button, Row, Col } from 'antd';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { useAppSelector } from '../../../hooks/reduxHooks';
import { RoleEnum } from '../../../enum/roleEnum/RoleEnum';
import { useNavigate } from "react-router-dom";
import './MathGeneratorStyle.css';

const { Option } = Select;

function MathGenerator() {
    const { user } = useAppSelector(state => state.auth);
    const [userCheck, setUser] = useState<boolean>(false);
    const navigate = useNavigate();

    const [numQuestions, setNumQuestions] = useState(10);
    const [numDigits, setNumDigits] = useState('1');
    const [operation, setOperation] = useState('addition');
    const [worksheet, setWorksheet] = useState<JSX.Element[]>([]);

    const userHandler = () => {
        if (user) {

            if (user.role.role === RoleEnum.TEACHER || user.role.role === RoleEnum.SCHOOL) {

                return true;
            }
        }

        return false
    }

    useEffect(() => {
        setUser(userHandler());
        setWorksheet([]);
    }, [numDigits, user]);

    const saveAsPDF = () => {
        const elementToCapture = document.getElementById('worksheet-container');

        if (elementToCapture) {
            html2canvas(elementToCapture).then((canvas) => {
                const imgData = canvas.toDataURL('image/png');

                const pdf = new jsPDF('p', 'mm', 'a4');
                pdf.addImage(imgData, 'PNG', 10, 10, 190, 0);
                pdf.save('math_problems.pdf');
            });
        }
    };

    const generateWorksheet = () => {
        const numDigitsInt = parseInt(numDigits);
        const newWorksheet: JSX.Element[] = [];

        for (let i = 0; i < numQuestions; i++) {
            let num1 = Math.ceil(Math.random() * Math.pow(10, numDigitsInt));
            let num2 = Math.ceil(Math.random() * Math.pow(10, numDigitsInt));
            let problemText = '';

            if (operation === 'subtraction') {
                if (num1 < num2) {
                    [num1, num2] = [num2, num1];
                }
            } else if (operation === 'division') {
                while (num2 === 0) {
                    num2 = Math.ceil(Math.random() * Math.pow(10, numDigitsInt));
                }
                num1 = num1 * num2;
            }

            switch (operation) {
                case 'addition':
                    problemText = `${num1} + ${num2} = ____`;
                    break;
                case 'subtraction':
                    problemText = `${num1} - ${num2} = ____`;
                    break;
                case 'multiplication':
                    problemText = `${num1} * ${num2} = ____`;
                    break;
                case 'division':
                    problemText = `${num1} / ${num2} = ____`;
                    break;
            }

            const problemCard = (
                <div key={i} className="problem-card">
                    <p>{problemText}</p>
                </div>
            );

            newWorksheet.push(problemCard);
        }

        setWorksheet(newWorksheet);
    };

    return (
        <div className='container'>
                <h1 style={{marginTop: 50}}>Генератор пособий по математике</h1>
                <div className="input-container">
                    <Form layout="vertical">
                        <Form.Item label="Количество примеров">
                            <Input
                                type="number"
                                min="1"
                                value={numQuestions}
                                onChange={(e) => setNumQuestions(parseInt(e.target.value))}
                            />
                        </Form.Item>
                        <Form.Item label="Уровень сложности">
                            <Select
                                value={numDigits}
                                onChange={(value) => setNumDigits(value.toString())}
                            >
                                <Option value="1">Легкий</Option>
                                <Option value="2">Средний</Option>
                                <Option value="3">Сложный</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Действие">
                            <Select
                                value={operation}
                                onChange={(value) => setOperation(value.toString())}
                            >
                                <Option value="addition">Сложение</Option>
                                <Option value="subtraction">Вычитание</Option>
                                <Option value="multiplication">Умножение</Option>
                                <Option value="division">Деление</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" onClick={userCheck ? generateWorksheet : () => navigate('/login')}>
                                Создать пособие
                            </Button>
                        </Form.Item>
                    </Form>
                </div>

                <div id='worksheet-container'>
                    <Row gutter={16}>
                        {worksheet.map((problem, index) => {
                            const numDigitsInt = parseInt(numDigits, 10);
                            const spanValue = numDigitsInt >= 3 ? 8 : 6;
                            return (
                                <Col xs={12} sm={spanValue}  md={spanValue} lg={spanValue} key={index}>
                                    {problem}
                                </Col>
                            );
                        })}
                    </Row>
                </div>
                <Button style={{marginBottom: 25, marginTop: 15}} type="dashed" onClick={saveAsPDF}>
                    Сохранить как PDF
                </Button>
        </div>
    );
}

export default MathGenerator;
