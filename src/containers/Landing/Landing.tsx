import React, { useState, useEffect } from 'react';
import JsonData from "../../components/Landing/Data/data.json";
import Header from '../../components/Landing/HeaderComponent';
import Features from '../../components/Landing/Features';
import About from '../../components/Landing/About';
import Footer from '../../components/Landing/Footer';
import './LandingStyle/Landing.css'
import VideoAndSignUp from '../../components/Landing/Video';

interface HeaderData {
  title: string;
  paragraph: string;
}

interface AboutData {
  paragraph: string;
  Why: string[];
  Why2: string[];
}

interface ContactData {
  address: string;
  phone: string;
  email: string;
  facebook: string;
  twitter: string;
  youtube: string;
}

interface Feature {
  icon: string;
  title: string;
  text: string;
}

interface LandingPageData {
  Header: HeaderData;
  About: AboutData;
  Contact: ContactData;
  Features: Feature[];
}

const Landing: React.FC = () => {
  const [landingPageData, setLandingPageData] = useState<LandingPageData>({ Header: {
    title: '',
    paragraph: ''
  }, About: {
    paragraph: '',
    Why: [],
    Why2: []
  }, Contact: {
    address: '',
    phone: '',
    email: '',
    facebook: '',
    twitter: '',
    youtube: ''
  }, Features: [] });

  useEffect(() => {
    setLandingPageData(JsonData);
  }, []);

  return (
    <>
      <Header data={landingPageData.Header} />
      <Features data={landingPageData.Features} />
      <About data={landingPageData.About} />
      <VideoAndSignUp />
      {/* <Contact data={landingPageData.Contact} /> */}
      <Footer />
    </>
  );
};

export default Landing;
