import { useEffect, useState } from 'react';
import { useGetNoticeQuery } from '../../store/services/notice';
import NoticeMessage from '../../components/Notice/TeacherNotice/NoticeMessage';
import GradeNotice from '../../components/Grade/GradeNotice';
import { INotice } from '../../interfaces/notice/Notice';
import { useAppSelector } from '../../hooks/reduxHooks';
import { RootState } from '../../store/store';
import { Card, Col, Row } from 'antd';

const NoticeTeacher = () => {

    const {data: arrNotice , refetch} = useGetNoticeQuery()

    const [notice , setNotice] = useState<INotice[]>([])

    const { user } = useAppSelector<RootState>(state => state.auth);

    const changeNotice = async() => {

        await refetch()

        arrNotice && await setNotice(() => {
            return arrNotice
        })        

    }


    useEffect(() => {
        
        changeNotice()

    },[user , arrNotice])
    
    return (
        <div>
            <Row gutter={[24 ,24]} >
                
            
            {
                notice.map((val) => {
                    return <Col 
                            span={24}
                            key={val.id}
                        >
                        {
                            val.grade ?
                                <Card
                                    hoverable
                                >
                                    <GradeNotice
                                        from_user={val.from_user}
                                        id={val.id}
                                        isRead={val.isRead}
                                        grade={val.grade}
                                    />
                                </Card>
                                
                                :
                                <Card
                                    hoverable
                                    
                                >
                                    <NoticeMessage
                                        message={val.message}
                                        from_user={val.from_user}
                                        id={val.id}
                                        isRead={val.isRead}
                                    />
                                </Card>
                        }
                    </Col>
                })
            }
            </Row>
        </div>
    )
}

export default NoticeTeacher