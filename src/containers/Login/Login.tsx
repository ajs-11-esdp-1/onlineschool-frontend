import { ChangeEvent, useEffect, useState } from "react";
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Typography, Alert } from 'antd';
import { Link } from "react-router-dom";
import { LoginForm } from "../../interfaces/auth/LoginForm";
import { useNavigate } from 'react-router-dom';
import { useSignInMutation } from "../../store/services/auth";
import './LoginStyle.css'

const { Title } = Typography;

const Login = () => {
    const [form] = Form.useForm();

    const [loginForm, setLoginForm] = useState<LoginForm>({
        email: '',
        password: ''
    })

    const [signIn, { isError }] = useSignInMutation();
    const [showAlert, setShowAlert] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        setShowAlert(isError);
    }, [isError]);

    const handleClose = () => {
        setShowAlert(false);
    }

    const onFinish = async () => {
        const data = await signIn(loginForm);
        if (!(data as { error: object }).error) {
            setLoginForm({
                email: '',
                password: ''
            })
            form.resetFields();
            navigate('/');
        }
    };

    const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;

        setLoginForm(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    return (
        <div className="container">
            {showAlert && (
                <Alert style={{ marginTop: 15 }} onClose={handleClose} message={'Что-то пошло не так. Попробуйте еще раз.'}
                    type="error" closable showIcon />
            )}
            <div className="login-container">
                <Title style={{ paddingTop: '100px' }} level={2}>Вход</Title>
                <Form
                    form={form}
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    style={{ maxWidth: 330 }}
                >
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Укажите адрес электронной почты' }]}
                    >
                        <Input
                            value={loginForm.email}
                            name="email"
                            prefix={<UserOutlined className="site-form-item-icon" />}
                            placeholder="E-mail"
                            onChange={inputChangeHandler}
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Укажите пароль' }]}
                    >
                        <Input.Password
                            value={loginForm.password}
                            prefix={<LockOutlined />}
                            type="password"
                            placeholder="Пароль"
                            name="password"
                            onChange={inputChangeHandler}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox className="forgot-text" style={{ float: 'left' }}>Запомнить меня</Checkbox>
                        </Form.Item>

                        <a style={{ float: 'right' }} href="#">
                            Забыли пароль?
                        </a>
                    </Form.Item>

                    <Form.Item style={{ display: 'flex', justifyContent: 'center' }}>
                        <Button type="primary" htmlType="submit">
                            Войти
                        </Button>
                    </Form.Item>
                    <Typography className="register-text">
                        Еще нет аккаунта?<Link to='/register'> Зарегистрироваться</Link>
                    </Typography>
                </Form>
            </div>
        </div>
    )
};

export default Login;