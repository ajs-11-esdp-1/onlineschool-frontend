import { useState } from "react";
import type { RadioChangeEvent } from 'antd';
import { Radio, Typography } from 'antd';
import { Link } from 'react-router-dom';
import TeacherRegistrationForm from "../TecherRegistrationForm/TeacherRegistrationForm";
import SchoolRegistrationForm from "../SchoolRegistrationForm/SchoolRegistrationForm";
import './RegistrationStyle.css'

type RegistrationType = '2' | '3'; // teacher - 2 school - 3

const { Title } = Typography;

const Registration = () => {
    const [registrationType, setRegistrationType] = useState<RegistrationType>('2');
    const onRegistrationTypeChange = (e: RadioChangeEvent) => {
        setRegistrationType(e.target.value);
    };

    return (
        <div className='container'>
            <div className="registration-box">
                <Title className="registration-title" style={{ paddingTop: '30px' }} level={2}>Регистрация</Title>
                <Radio.Group className="radio-box" onChange={onRegistrationTypeChange} defaultValue={'2'}>
                    <Radio.Button checked={registrationType === '2'} name="registrationType" value={'2'}>Учитель</Radio.Button>
                    <Radio.Button checked={registrationType === '3'} name="registrationType" value={'3'}>Школа</Radio.Button>
                </Radio.Group>
                {registrationType === '2'
                    ?
                    <TeacherRegistrationForm />
                    :
                    <SchoolRegistrationForm />
                }
                <Typography className="login-text">
                    Уже есть аккаунт?<Link to='/login'> Войти</Link>
                </Typography>
            </div>
        </div>
    )
};

export default Registration;