import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {LessonReducerByPost} from "../../interfaces/tutorial/LessonByRTK";

const initialState: Omit<LessonReducerByPost, 'lesson'> = {
    title: '',
    description: '',
    transit_time: '0',
    teacher_id: 2,
    template_id: 2,
    direction_id: 2,
    public:false
}

const lessonReducer = createSlice({
    name: 'lesson',
    initialState: initialState,
    reducers: {
        initLesson: () => {
            return initialState
        },
        changeTitle: (state, action: PayloadAction<string>) => {
            return state = {...state, title: action.payload}
        },
        changeDescription: (state, action: PayloadAction<string>) => {
            return state = {...state, description: action.payload}
        },
        changeTime: (state, action: PayloadAction<string>) => {
            return state = {...state, transit_time: action.payload}
        },
        changeTeacher: (state, action: PayloadAction<number>) => {
            return state = {...state, teacher_id: action.payload}
        },
        changeTemplate: (state, action: PayloadAction<number>) => {
            return state = {...state, template_id: action.payload}
        },
        changeDirection: (state, action: PayloadAction<number>) => {
            return state = {...state, direction_id: action.payload}
        },
    }
})


export const {
    initLesson,
    changeDescription,
    changeDirection,
    changeTeacher,
    changeTemplate,
    changeTime,
    changeTitle
} = lessonReducer.actions;

export default lessonReducer.reducer;