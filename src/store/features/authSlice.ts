import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IUser} from '../../interfaces/auth/IUser';
import authApi from '../services/auth';

interface State {
    user: IUser | null,
}

const initialState: State = {
    user: null
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder
            .addMatcher(authApi.endpoints.signIn.matchFulfilled, (state, action: PayloadAction<IUser>) => {
                state.user = action.payload;
            })
            .addMatcher(authApi.endpoints.logout.matchFulfilled, () => {
                return initialState;
            })
    }
})

export default authSlice.reducer;