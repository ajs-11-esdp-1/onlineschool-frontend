import {api} from "./index";
import {ILessonByRTK} from "../../interfaces/tutorial/LessonByRTK";

const tutorialApi = api.injectEndpoints({
    endpoints: (build) => ({
        postTutorial: build.mutation<ILessonByRTK, ILessonByRTK>({
            query: (body) => ({
                url: "/tutorials",
                method: "post",
                body,
            }),
            invalidatesTags: ['Tutorials']
        }),
        getTutorials: build.query<ILessonByRTK[], void>({
            query: () => '/tutorials',
            providesTags: ['Tutorials']
        }),
        deleteTutorial: build.mutation<ILessonByRTK[], string>({
            query: (id) => ({
                url: `/tutorials/${id}`,
                method: "delete",
            }),
            invalidatesTags: ['Tutorials']
        }),
    })
})

export const {
    useDeleteTutorialMutation,
    useGetTutorialsQuery,
    usePostTutorialMutation
} = tutorialApi;

