import IDirection from "../../interfaces/direction/Direction";
import {api} from "./index";

const directionApi = api.injectEndpoints({
    endpoints: (builder) => ({
        getDirection: builder.query<IDirection[], void>({
            query: () => '/direction',
            providesTags: ['Direction']
        })
    })
})

export const {useGetDirectionQuery} = directionApi