import { ISavePicture } from '../../interfaces/tutorial/Lesson'
import {ILessonByRTK, LessonReducerByPost} from '../../interfaces/tutorial/LessonByRTK'
import {api} from './index'

const lessonsApi = api.injectEndpoints({
    endpoints: (build) => ({
        getPublicLesson: build.query<ILessonByRTK [], void>({
            query: () => '/lesson/public',
            providesTags: ['Lesson']
        }),
        postLesson: build.mutation<ILessonByRTK, LessonReducerByPost>({
            query: (body) => ({
                url: '/lesson/save',
                method: 'post',
                body: body
            }),
            invalidatesTags: ['Lesson']
        }),
        getLesson: build.query<ILessonByRTK, void>({
            query: () => '/lesson',
            providesTags: ['Lesson']
        }),
        postPictureLesson: build.mutation<void, ISavePicture>({
            query: (body) => ({
                url: `/lesson/save/picture/${body.lesson_id}`,
                method: 'post',
                body: body.picture
            }),
            invalidatesTags: ['Lesson']
        }),
        getTutorialByTeacher: build.query<ILessonByRTK[] , void>({
            query:() => '/lesson/by/teacher',
            providesTags: ['Lesson']
        }),
        putTutorialPublic: build.mutation<ILessonByRTK, number>({
            query: (body) => ({
                url: `/lesson/published/${body}`,
                method: 'put',
            }),
            invalidatesTags: ['Lesson']
        }),
        deleteTutorial : build.mutation<string, number>({
            query: (body) => ({
                url: `/lesson/delete/${body}`,
                method: 'delete',
            }),
            invalidatesTags: ['Lesson']
        }),
    })
})

export const {
    useGetLessonQuery,
    useGetPublicLessonQuery,
    usePostLessonMutation,
    usePostPictureLessonMutation,
    useGetTutorialByTeacherQuery,
    useDeleteTutorialMutation,
    usePutTutorialPublicMutation
} = lessonsApi