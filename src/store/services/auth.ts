import {api} from "./index";
import {IUser} from "../../interfaces/auth/IUser";
import {RegisterForm} from "../../interfaces/auth/RegisterForm";
import {LoginForm} from "../../interfaces/auth/LoginForm";

export const authApi = api.injectEndpoints({
    endpoints: (build) => ({
        signUp: build.mutation<IUser, RegisterForm>({
            query: (body) => ({
                url: "/auth/register",
                method: "post",
                body,
            }),
        }),
        signIn: build.mutation<IUser, LoginForm>({
            query: (body) => ({
                url: "/auth/login",
                method: "post",
                body,
            }),
        }),
        logout: build.mutation<void, void>({
            query: () => ({
                url: "/auth/logout",
                method: "delete",
            }),
        }),
    }),
});

export const {useSignUpMutation, useSignInMutation, useLogoutMutation} = authApi;
export default authApi;