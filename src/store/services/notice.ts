import {INotice, IPostNoticeStudent} from '../../interfaces/notice/Notice';
import {api} from './index'

const noticeApi = api.injectEndpoints({
    endpoints:(build) => ({
        getNotice:build.query<INotice[] , void>({
            query:() => '/notice/all',
            providesTags:['Notice']
        }),
        postNoticeStudent:build.mutation<INotice[] , IPostNoticeStudent>({
            query : (body) => ({
                url: '/notice/student/add',
                method: 'post',
                body: body
            }),
            invalidatesTags:['Notice']
        }),
        getReadNotice : build.query<INotice[] , void>({
            query: () => '/notice/notread',
            providesTags:['Notice']
        }),
        postNoticeTeacher: build.mutation<INotice , Pick<INotice , 'recipient' | 'message' > >({
            query : (body) => ({
                url: '/notice/teacher/add',
                method: 'post',
                body: body
            }),
            invalidatesTags:['Notice']
        }),
        putNoticeRead : build.mutation<INotice , string>({
            query : (id) => ({
                url: `/notice/read/${id}`,
                method: 'put',
            }),
            invalidatesTags:['Notice']
        }),
        
    })
});

export const {
    useGetNoticeQuery,
    usePostNoticeStudentMutation,
    useGetReadNoticeQuery,
    usePutNoticeReadMutation
} = noticeApi