import {Template} from "../../interfaces/template/Template";
import {api} from "./index";

const templateApi = api.injectEndpoints({
    endpoints: (build) => ({
        getTemplate: build.query<Template[], void>({
            query: () => '/template',
            providesTags: ['Template']
        }),
        getByIdTemplate: build.query<Template, string>({
            query: (id) => `/template?id=${id}`,
            providesTags: ['Template']
        }),
        postTemplate: build.mutation<Template, Omit<Template, 'id'>>({
            query: (body) => ({
                url: '/template',
                method: 'post',
                body: body
            }),
            invalidatesTags: ['Template']
        })
    })
})

export const {useGetByIdTemplateQuery, useGetTemplateQuery, usePostTemplateMutation} = templateApi;