import { IStudent } from '../../interfaces/teacher/Teacher'
import {api} from './index'
import { IUser } from '../../interfaces/auth/IUser'

const teacher = api.injectEndpoints({
    endpoints: (build) => ({

        getStudentByTeacher : build.query<IStudent[] , void>({
            query: () => '/teacher/by/student',
            providesTags:['Student']
        }),
        postStudent: build.mutation<IUser , Omit<IUser , 'id'>>({
            query: (body) => ({
                url: '/by/student',
                method: 'post',
                body: body
            })
        })

    })
})

export const { 
    useGetStudentByTeacherQuery,
    usePostStudentMutation
} = teacher