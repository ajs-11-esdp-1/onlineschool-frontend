import { IStudent, ITeacherStudent } from '../../interfaces/teacher/Teacher'
import {api} from './index'

const teacherStudentApi = api.injectEndpoints({
    endpoints: (build) => ({
        getArrStudentTeacher : build.query<IStudent[] , void>({
            query: () => '/teacher/by/student',
            providesTags:['TeacherToStudent']
        }),
        postStudentToTeacher : build.mutation<IStudent , Pick<ITeacherStudent , 'student_id'>>({
            query: (body) => ({
                url: '/teacher/add/student',
                method: 'post',
                body:body
            }),
            invalidatesTags:['TeacherToStudent']
        })
    })
})



export const {
    useGetArrStudentTeacherQuery , 
    usePostStudentToTeacherMutation 
} = teacherStudentApi