import {GetGrade, IGrade, IReview} from '../../interfaces/grade/IGrade'
import {api} from './index'

const gradeApi = api.injectEndpoints({
    endpoints: (builder) => ({
        getAllGradeStudent: builder.query<GetGrade[], void>({
            query: () => '/grades/public/all/student',
            providesTags: ['Grade']
        }),
        saveGrade: builder.mutation<GetGrade, Omit<IGrade, 'review' | 'id' | 'readed'>>({
            query: (body) => ({
                url: '/grades/save',
                method: 'post',
                body: body
            }),
            invalidatesTags: ['Grade']
        }),
        getPublicAllGrade: builder.query<GetGrade[], void>({
            query: () => '/grades/public/all',
            providesTags: ['Grade']
        }),
        getPublicGradeById: builder.query<GetGrade, number>({
            query: (id) => `/grades/public/${id}`,
            providesTags: ['Grade']
        }),
        postReveiwGradeById: builder.mutation<GetGrade, IReview>({
            query: (body) => ({
                url: `/grades/review/${body.grade_id}`,
                method: 'post',
                body: {review : body.review},
                
            }),
            invalidatesTags: ['Grade']
        }),
        getAllGradeToTeacher: builder.query<GetGrade[], void>({
            query: () => '/grades/public/all/teacher',
            providesTags: ['Grade']
        }),


    })
})

export const {
    useGetAllGradeStudentQuery,
    useGetPublicAllGradeQuery,
    useGetPublicGradeByIdQuery,
    useSaveGradeMutation,
    usePostReveiwGradeByIdMutation,
    useGetAllGradeToTeacherQuery
} = gradeApi;