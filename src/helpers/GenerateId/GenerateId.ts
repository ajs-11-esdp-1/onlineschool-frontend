const alphabet: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

export const generateID = ():string => {
    const ID:string[] = [];

    for (let i = 0 ; i < 12 ; i++) {
        const number : number = Math.floor(Math.random() * alphabet.length);
        ID.push(alphabet[number]);
    }
    return ID.join('');
};