import { Routes, Route } from 'react-router-dom';
import Registration from "../containers/Registration/Registration";
import Login from "../containers/Login/Login";
import RegistrationConfirm from "../components/RegistrationConfirm/RegistrationConfirm";
import Confirmation from "../components/Confirmation/Confirmation";
import WrittenAnswerForm from "../containers/Tutorials/TutorialsByTeachers/WrittenAnswerForm/WrittenAnswerForm";
import InteractiveWrittenAnswer
    from '../containers/Tutorials/TutorialsByStudents/InteractiveWrittenAnswer/InteractiveWrittenAnswer';
import Tutorials from "../containers/Tutorials/Tutorials";
import SortShowTutorial from "../containers/Tutorials/TutorialsByStudents/SortWordsTutorial/SortShowTutorial";
import NewLesson from "../containers/MakeLesson/NewLesson";
import SortTutorial from '../containers/Tutorials/TutorialsByTeachers/SortWordsTutorial/SortTutorial';
import SortPicture from '../containers/Tutorials/TutorialsByTeachers/SortByPicture/SortByPicture'
import SortShowPictureTutorial
    from '../containers/Tutorials/TutorialsByStudents/SortPictureTutorial/SortShowPictureTutorial';
import QuizCreator from '../containers/Tutorials/TutorialsByTeachers/Quiz/QuizCreator';
import Quiz from '../containers/Tutorials/TutorialsByStudents/Quiz/QuizStudent';
import MatchShowWordPictureTutorial
    from '../containers/Tutorials/TutorialsByStudents/MatchShowWordPictureTutorial/MatchShowWordPictureTutorial';
import MatchByWordPicture from '../containers/Tutorials/TutorialsByTeachers/MatchByWordPicture/MatchByWordPicture';
import TeacherArea from '../containers/PersonalArea/TeacherArea/TeacherArea';
import TutorialByTeacher from '../containers/PersonalArea/TeacherArea/TeacherPage/TutorialByTeacher';
import PageNotFound from '../components/PageNotFound/PageNotFound';
import NoticeTeacher from '../containers/Notice/NoticeTeacher';
import NoticeStudent from '../containers/Notice/NoticeStudent';
import MahjongStudent from "../containers/Tutorials/TutorialsByStudents/MahjongStudent/MahjongStudent";
import MahjongCreator from "../containers/Tutorials/TutorialsByTeachers/Mahjong/MahjongCreator";
import LandingPage from '../containers/LandingPage/LandingPage';
import MathGenerator from '../containers/Generators/MathGenerator/MathGenerator';
import MatchingListsWithImages from "../containers/Generators/Reading/MatchingListsWithImages/MatchingListsWithImages";
import StudentByTeacher from '../containers/PersonalArea/TeacherArea/TeacherPage/StudentByTeacher';
import { useAppSelector } from '../hooks/reduxHooks';
import ProtectedRoute from '../components/ProtectedRoute/ProtectedRoute';
import { RoleEnum } from '../enum/roleEnum/RoleEnum';


const Router = () => {
    const { user } = useAppSelector(state => state.auth);
    return (
        <Routes>
            <Route path='/' element={<LandingPage />} />
            <Route path='*' element={<PageNotFound />} />
            <Route path='/confirm/:confirmationCode' element={<Confirmation />} />
            <Route path="/verification" element={<RegistrationConfirm />} />
            <Route path="/register" element={(
                <ProtectedRoute isAllowed={!user} redirectPath='/'>
                    <Registration />
                </ProtectedRoute>
            )} />
            <Route path="/login" element={(
                <ProtectedRoute isAllowed={!user} redirectPath='/'>
                    <Login />
                </ProtectedRoute>
            )} />
            <Route path='/worksheets' element={<Tutorials />} />
            <Route path='/shop' element={<h2>shop</h2>} />
            <Route path='/generator/words' element={<MatchingListsWithImages />} />
            <Route path='/generator/math' element={<MathGenerator />} />
            <Route path='/subscription' element={<h2>subscription</h2>} />
            <Route path='/sort/words/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <SortShowTutorial />
                </ProtectedRoute>
            )} />
            <Route path='/sort/picture/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <SortShowPictureTutorial />
                </ProtectedRoute>
            )} />
            <Route path='/quiz/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <Quiz />
                </ProtectedRoute>
            )} />
            <Route path='/written/answer/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <InteractiveWrittenAnswer />
                </ProtectedRoute>
            )} />
            <Route path='/match/word-picture/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <MatchShowWordPictureTutorial />
                </ProtectedRoute>
            )} />
            <Route path='/mahjong/:id' element={(
                <ProtectedRoute isAllowed={!!user} redirectPath='/login'>
                    <MahjongStudent />
                </ProtectedRoute>
            )} />
            <Route path='/new/lesson' element={(
                <ProtectedRoute isAllowed={user && user.role.role === RoleEnum.TEACHER}
                    redirectPath={user && user.role.role !== RoleEnum.TEACHER ? '/' : '/login'}>
                    <NewLesson />
                </ProtectedRoute>
            )}>
                <Route path='/new/lesson/sort/word/:template' element={<SortTutorial />} />
                <Route path='/new/lesson/quiz/:template' element={<QuizCreator />} />
                <Route path='/new/lesson/sort/picture/:template' element={<SortPicture />} />
                <Route path='/new/lesson/written/answer/:template' element={<WrittenAnswerForm />} />
                <Route path='/new/lesson/mahjong/:template' element={<MahjongCreator />} />
                <Route path='/new/lesson/match/word-picture/:template' element={<MatchByWordPicture />} />
            </Route>
            <Route path='/teacher/personal/area' element={(
                <ProtectedRoute isAllowed={user && user.role.role === RoleEnum.TEACHER}
                    redirectPath={user && user.role.role !== RoleEnum.TEACHER ? '/' : '/login'}>
                    <TeacherArea />
                </ProtectedRoute>
            )} >
                <Route index path='/teacher/personal/area/tutorial' element={<TutorialByTeacher />} />
                <Route path='/teacher/personal/area/student' element={<StudentByTeacher />} />
            </Route>
            <Route path='/notice/teacher' element={(
                <ProtectedRoute isAllowed={user && user.role.role === RoleEnum.TEACHER}
                    redirectPath={user && user.role.role !== RoleEnum.TEACHER ? '/' : '/login'}>
                    <NoticeTeacher />
                </ProtectedRoute>
            )} />
            <Route path='/notice/student' element={(
                <ProtectedRoute isAllowed={user && user.role.role === RoleEnum.STUDENT}
                    redirectPath={user && user.role.role !== RoleEnum.STUDENT ? '/' : '/login'}>
                    <NoticeStudent />
                </ProtectedRoute>
            )} />
        </Routes>
    )
}

export default Router;