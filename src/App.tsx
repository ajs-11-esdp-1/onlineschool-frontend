import { Layout, ConfigProvider } from 'antd';
import AppToolbar from './containers/Menus/AppTollBar';
import Router from './routes';
import FooterNav from './components/Footer/FooterNav';

const { Content } = Layout;

const contentStyle = {
    minHeight: 'calc(100vh - 75px)',
    width: '100%'
};

function App() {
    return (
        <div>
            <ConfigProvider
                theme={{
                    token: {
                        // Seed Token
                        colorPrimary: '#52c41a',
                        colorLink: '#52c41a'
                    },
                }}
            >
                <Layout>
                    <AppToolbar />
                    <Content style={contentStyle}>
                        <Router />
                    </Content>
                    <FooterNav />
                </Layout>
            </ConfigProvider>

        </div>
    );
}

export default App;