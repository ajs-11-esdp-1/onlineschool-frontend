import Lesson from "../Lesson"


export interface SortByPicture extends Omit<Lesson, 'user' | 'template' | 'direction'> {
    lesson: {
        theme: ITheme[];
        arrPicture: ItemPicture[];
    },

}

export interface ITheme {
    id: string;
    theme: string;
}

export interface ItemPicture {
    id: string;
    theme_id: string;
    picture: File | string;
    dragbel: boolean;
}