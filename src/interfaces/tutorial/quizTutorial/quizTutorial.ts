import Lesson from "../Lesson";

export interface Option {
    option: string;
}

export interface Question {
    question: string;
    options: Option[];
    correctAnswer: string;
    
}

export interface QuizTutorial extends Omit<Lesson, 'user' | 'template' | 'direction'> {
    lesson: {
        quiz: Question[];
        student_answers: string[]
    },

}