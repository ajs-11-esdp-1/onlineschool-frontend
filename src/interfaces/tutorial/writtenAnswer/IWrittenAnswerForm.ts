import { IGrade } from "../../grade/IGrade";
import Lesson from "../Lesson"

export interface IWrittenAnswerForm extends Omit<Lesson, 'user' | 'template' | 'direction'>{
    lesson: {
        questions: IQuestion[];
        arrPictures: IPictures[];
    }
}

export interface IWrittenAnswerValues {
    questions: IQuestion[];
}

export interface IQuestion {
    question: string;
    answers: IAnswer[];
    picture: string | null;
}

export interface IAnswer {
    answer: string;
}

export interface IPictures {
    id: string;
    question_id: number;
    picture: File | string;
}
export interface IQALessonAnswer {
    student_answer: string;
    question: string;
    isCorrect: boolean;
    correct_answers: IAnswer[];
    picture: string | null;
}

export interface IQAGrade extends Omit<IGrade, 'lesson_answer' | 'id' | 'readed' | 'review'>{
    lesson_answer: {
        title: string;
        description: string
        studentAnswers: IQALessonAnswer[];
    }
}

export interface IQALessonAnswer {
    student_answer: string;
    question: string;
    isCorrect: boolean;
    correct_answers: IAnswer[];
    picture: string | null;
}

