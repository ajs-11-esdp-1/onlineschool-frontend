import Lesson from "../Lesson"

export interface ISortObject extends Omit<Lesson, 'user' | 'template' | 'direction'> {
    lesson: {
        arrWords: IWordSortTutorial[],
        checkedWord: IWordSortTutorial[],
    }
}


export interface IWordSortTutorial {
    value: string;
    isDrable: boolean;
    id: string;
    styleHandler: boolean;
}
