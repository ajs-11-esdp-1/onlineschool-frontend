import Lesson from "../Lesson";

export interface ITile {
    id: string;
    value: File | string;
    isMatched: boolean;
    isVisible: boolean;
}

export  interface IMahjong extends Omit<Lesson , 'user' | 'template' | 'direction'>{
    lesson: {
        images: ITile[]
    }
}