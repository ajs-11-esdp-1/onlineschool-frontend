import Lesson from "./Lesson"

export interface ILessonByRTK extends Lesson {
    lesson: string;
}

export type LessonReducer = Omit<ILessonByRTK, 'lesson' | 'user' | 'id' | 'direction' | 'template'>;

export type LessonReducerByPost = Omit<ILessonByRTK, 'user' | 'id' | 'direction' | 'template'>;