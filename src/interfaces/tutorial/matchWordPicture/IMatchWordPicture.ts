import Lesson from "../Lesson";

export interface IWord {
    id: string;
    word: string;
}

export interface IPicture {
    id: string;
    word_id: string;
    picture: File | string;
}

export interface IMatchByWordPicture extends Omit<Lesson, 'user' | 'template' | 'direction'> {
    lesson: {
        arrWords: IWord[]
        arrPictures: IPicture[]
    }
}

export interface PictureObject {
    id: string;
    word_id: string;
    picture: string;
}

export interface AnswerLesson {
    arrWords: IWord[];
    arrPictures: PictureObject[];
}