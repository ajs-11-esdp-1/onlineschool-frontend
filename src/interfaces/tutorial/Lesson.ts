export default interface ILessons {
    id: string | number;
    title: string;
    description: string;
    transit_time: string;
    direction: {
        direction: string;
    }
    user: {
        id: number;
        firstName: string;
        lastName: string;
        avatar: string;
    }
    template: {
        title: string;
        description: string;
    }
    teacher_id: number;
    template_id: number;
    direction_id: number;
    public:boolean
}

export interface ISavePicture {
    lesson_id: number
    picture :FormData
}


export interface ISF{
    lesson: string
}
