export interface IWordObject {
    id: string;
    word: string;
    image: string;
}