import { IUser } from "../auth/IUser"

export interface ITeacherStudent{
    id:number
    teacher_id: number
    student_id : number
}

export interface IStudent extends ITeacherStudent{
    student: Omit<IUser , 'password'| 'token' | 'isConfirmed' |'phone' | 'email'>
}