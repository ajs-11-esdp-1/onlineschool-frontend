export interface TeacherLinkArea {
    label: string
    children:React.ReactNode
}