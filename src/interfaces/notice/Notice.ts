import { IUser } from "../auth/IUser"
import { ILessonByRTK } from "../tutorial/LessonByRTK"
import { GetGrade } from "../grade/IGrade"
import { NoticeType } from "../../enum/noticeType/NoticeType"

export  interface INotice {
    id:number
    sender : number
    recipient: number
    model_id: number 
    notice_type: NoticeType
    message: string
    isRead: boolean
    from_user: Omit<IUser ,'password'| 'token' |'isConfirmed' |'phone' | 'email'>
    lesson?:ILessonByRTK
    grade?:GetGrade
}

export interface IPostNoticeStudent {
    sender : number
    recipient: number[]
    model_id: number 
    notice_type: NoticeType

}