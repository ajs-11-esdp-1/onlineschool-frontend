import {IRole} from "../role/IRole";

export interface IUser {
    id: number;
    name?: string;
    userName?: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: number;
    avatar?: string;
    token: string;
    role_id: number;
    role?: IRole;
}
