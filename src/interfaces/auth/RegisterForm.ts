export interface RegisterForm {
    schoolName?: string;
    firstName?: string;
    lastName?: string;
    password: string;
    email: string;
    phone: string;
    role_id: string;
}
