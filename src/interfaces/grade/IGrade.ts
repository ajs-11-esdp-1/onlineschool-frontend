import {IUser} from "../auth/IUser"
import {ILessonByRTK} from "../tutorial/LessonByRTK"

export interface IGrade {
    id: string;
    lesson_id: number;
    grade: string;
    transit_time: string;
    readed: boolean;
    student_id: number;
    lesson_answer: JSON;
    teacher_id: number;
    review: string;
}

export interface GetGrade {
    id: string;
    lesson_id: number;
    grade: string;
    transit_time: string;
    readed: boolean;
    student_id: number;
    lesson_answer: string;
    teacher_id: number;
    review: string;
    teacher: Pick<IUser, 'avatar' | 'firstName' | 'lastName' | 'role_id' | 'id'>;
    student: Pick<IUser, 'avatar' | 'firstName' | 'lastName' | 'role_id' | 'id'>;
    lesson: ILessonByRTK;
}

export interface IReview {
    grade_id: number;
    review: string;
}