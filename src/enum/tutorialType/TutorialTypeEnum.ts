export enum TutorialTypeEnum {
    wordsSort = 'Распредели правильно слова',
    pictureSort = 'Распредели правильно картинки',
    writtenAnswer = 'Впиши ответ',
    mahjong = 'Маджонг',
    matchWordPicture = 'Сопоставление картинок по словам',
    quizTutorial = 'Викторина 1 из 4',
}