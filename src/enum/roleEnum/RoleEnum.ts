export enum RoleEnum {
    STUDENT = 'student',
    TEACHER = 'teacher',
    SCHOOL = 'school',
}